package com.netizen.eduman

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatDelegate
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION_RESULT
import com.netizen.eduman.model.AcademicYear
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import es.dmoral.toasty.Toasty
import org.json.JSONException
import java.util.*
import kotlin.collections.HashMap

class LoginActivity : Activity() {

    private val TAG = LoginActivity::class.java.simpleName
    internal var sectionsArrayList = ArrayList<Section>()
    internal var academicYearsArrayList = ArrayList<AcademicYear>()

    internal var input_userName: TextInputEditText? = null
    internal var input_password: TextInputEditText? = null
    internal var input_random_sum: TextInputEditText? = null

    internal var inputLayoutUserName: TextInputLayout? = null
    internal var inputLayoutUserPass: TextInputLayout? = null

    internal var sign_in_tead: TextView? = null
    internal var sign_in_sub_tead: TextView? = null
    internal var txt_version: TextView? = null
    internal var txt_network: TextView? = null
    private var btn_login: Button? = null

    internal var name: String? = null
    internal var pass: String? = null
    internal var sum: String? = null
    internal var grant_type = "password"

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_final)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        initializeView()

        val contentView: View = findViewById(R.id.contentView)
        contentView.setOnClickListener {
            it.hideKeyboard()
        }

    }

    override fun onStart() {
        super.onStart()
    }

    private fun initializeView() {

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        viewDialog = ViewDialog(this)

        sign_in_tead = findViewById<TextView>(R.id.sign_in_tead)
        sign_in_sub_tead = findViewById<TextView>(R.id.sign_in_text_sub_head)
        txt_version = findViewById<TextView>(R.id.txt_version)
        txt_network = findViewById<TextView>(R.id.network_disconnect)

        val versionName = BuildConfig.VERSION_NAME
        txt_version?.text = "Version $versionName"

        val typeface = Typeface.createFromAsset(applicationContext.assets, "fonts/Raleway-Bold.ttf")
        sign_in_tead?.setTypeface(typeface)
        val typeface1 =
            Typeface.createFromAsset(applicationContext.assets, "fonts/Raleway-Regular.ttf")
        sign_in_sub_tead?.setTypeface(typeface1)

        val typeface2 =
            Typeface.createFromAsset(applicationContext.assets, "fonts/Poppins-Regular.ttf")
        sign_in_sub_tead?.setTypeface(typeface2)
        txt_version?.setTypeface(typeface2)

        input_userName = findViewById(R.id.input_userName) as TextInputEditText
        input_password = findViewById(R.id.input_password) as TextInputEditText

        val userLogin = dao?.getUserDetails()
        input_userName?.setText(userLogin?.getuserName())
        input_password?.setText(userLogin?.getuserPassword())

        inputLayoutUserName = findViewById(R.id.input_layout_user)
        inputLayoutUserPass = findViewById(R.id.input_layout_pass)

        input_userName?.addTextChangedListener(MyTextWatcher(input_userName!!))
        input_password?.addTextChangedListener(MyTextWatcher(input_password!!))

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)

        btn_login = findViewById<Button>(R.id.btn_login)

        btn_login?.setOnClickListener {
            try {

                if (!AppController.instance?.isNetworkAvailable()!!) {

                    AppController.instance?.noInternetConnection()
                } else if (input_userName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
                    inputLayoutUserName?.setError(getString(R.string.err_msg_name))
                    requestFocus(input_userName!!)

                } else if (input_password?.getText()!!.toString().trim { it <= ' ' }.isEmpty()) {
                    inputLayoutUserPass?.setError(getString(R.string.err_msg_pass))
                    requestFocus(inputLayoutUserPass!!)
                } else {

                    setKeyboardVisibility(false)
                    sentLoginInformation()
                }

            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
        }
    }


    private fun sentLoginInformation() {
        if (!validateName()) {
            return
        }

        if (!validatePassword()) {
            return
        }

        name = input_userName?.text.toString()
        pass = input_password?.text.toString()
        sum = input_random_sum?.text.toString()

        try {
            login_check()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun afterTextChanged(editable: Editable) {

            when (view.id) {
                R.id.input_userName -> validateName()
                R.id.input_password -> validatePassword()
            }
        }
    }

    private fun validateName(): Boolean {
        if (input_userName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputLayoutUserName?.setError(getString(R.string.err_msg_name))
            requestFocus(input_userName!!)
            return false
        } else {
            inputLayoutUserName?.setErrorEnabled(false)
        }

        return true
    }

    private fun validatePassword(): Boolean {
        if (input_password?.getText()!!.toString().trim { it <= ' ' }.isEmpty()) {
            inputLayoutUserPass?.setError(getString(R.string.err_msg_pass))
            requestFocus(inputLayoutUserPass!!)
            return false
        } else {
            inputLayoutUserPass?.setErrorEnabled(false)
        }

        return true
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun setKeyboardVisibility(show: Boolean) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (show) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        } else {
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    private fun View.hideKeyboard() {
        val inputMethodManager =
            context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(this.windowToken, 0)
    }

    fun login_check() {

        val hitURL =
            AppController.BaseUrl + "institute/setting/user/check?userName=" + name + "&password=" + pass
        val params = HashMap<String?, String?>()

        params["userName"] = name //Items - Item 3 - phone
        params["password"] = pass //Items - Item 3 - catagory

        viewDialog?.showDialog()

        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Server Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")

                    if (messageType == 1) {
                        dataSendToServer()
                    } else {
                        Toasty.error(applicationContext, status, Toast.LENGTH_SHORT, true).show()
                        viewDialog?.hideDialog()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                var message: String? = null
                if (volleyError is NetworkError) {
                    Toast.makeText(
                        applicationContext,
                        "No Internet connection !",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        applicationContext,
                        "The server could not be found.!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        applicationContext,
                        "No Internet connection !",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is ParseError) {
                    message = "Parsing error !"
                } else if (volleyError is TimeoutError) {

                    Toasty.error(applicationContext, "Connection TimeOut", Toast.LENGTH_SHORT, true)
                        .show()
                } else {
                    Toasty.error(
                        applicationContext,
                        "An error occurred. Please try again",
                        Toast.LENGTH_SHORT,
                        true
                    )
                        .show()

                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                return headers
            }
        }

        val socketTimeout =
            100000//18 Minutes-change to what you want//100000 milliseconds = 1 minute
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy

        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }


    fun dataSendToServer() {

        val hitURL = AppController.BaseUrl + "oauth/token"

        val params = HashMap<String?, String?>()

        params["client_id"] = "eduman-web-read-write-client" //Items - Item 1 - name
        params["grant_type"] = grant_type //Items - passport
        params["username"] = name //Items - Item 3 - phone
        params["password"] = pass //Items - Item 3 - catagory

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Method.POST, hitURL, params,
            Response.Listener { response ->
                Log.d(TAG, "Server Response: $response")

                try {

                    val acesstoken = response.getString("access_token")
                    val tokentype = response.getString("token_type")
                    val refreshtoken = response.getString("refresh_token")

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_TOKEN + "(access_token, refresh_token, token_type) " +
                                "VALUES(?, ?, ?)", arrayOf(acesstoken, tokentype, refreshtoken)
                    )

                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
                    val editor = sharedpreferences.edit()
                    editor.putString("accessToken", acesstoken)
                    editor.putString("tokenType", tokentype)
                    editor.putString("refresh_token", refreshtoken)
                    editor.apply()

                    viewDialog?.hideDialog()

                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                catch (e: IllegalArgumentException){
                    viewDialog?.hideDialog()

                    e.printStackTrace()
                }

                authorization()

                name = input_userName?.text.toString()
                pass = input_password?.text.toString()
                DAO.executeSQL(
                    "INSERT OR REPLACE INTO " + DBHelper.TABLE_LOGIN + "(userName) " +
                            "VALUES(?)", arrayOf(name!!)
                )

                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                dao?.deleteSectionList()
                dao?.deleteAcademicList()
                dao?.close()

                loadServerSectionData()
                loadServerAcademicYearData()

              //  viewDialog?.hideDialog()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
               // d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                if (applicationContext!=null){

                when (volleyError) {
                    is NetworkError -> {
                        Toast.makeText(
                            applicationContext,
                            "No Internet connection !",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    is ServerError -> {
                        Toast.makeText(
                            applicationContext,
                            "The server could not be found.!",
                            Toast.LENGTH_SHORT
                        ).show()

                    }
                    is AuthFailureError -> {
                        Toast.makeText(
                            applicationContext,
                            "No Internet connection !",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    is TimeoutError -> {

                        Toasty.error(
                            applicationContext,
                            "Connection TimeOut !",
                            Toast.LENGTH_SHORT,
                            true
                        )
                            .show()
                    }
                    else -> {
                        Toasty.error(
                            applicationContext,
                            "Login credentials not matching ",
                            Toast.LENGTH_SHORT,
                            true
                        )

                    }
                }
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val client_Key =
                    "Basic ZWR1bWFuLXdlYi1yZWFkLXdyaXRlLWNsaWVudDpzcHJpbmctc2VjdXJpdHktb2F1dGgyLXJlYWQtd3JpdGUtY2xpZW50LXBhc3N3b3JkMTIzNA=="
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["Authorization"] = client_Key

                return headers
            }
        }

        val socketTimeout =
            100000//18 Minutes-change to what you want//100000 milliseconds = 1 minute
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    fun authorization() {
        val hitURL = AppController.BaseUrl + "institute/setting/user/info/details"
        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Server authorization: $response")
                try {
                    val c = response.getJSONObject("item")
                    val getData = c.getJSONArray("userRoles")

                    var user = ""

                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        user = getData.getString(i)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + DBHelper.TABLE_USER_ROLE + "(RoleAccount) " +
                                    "VALUES(?)", arrayOf(
                                user
                            )
                        )
                    }

                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_INSTITUTE_INFO + "(instituteId, instituteName, instituteAddress, academicYear, instituteLogo) " +
                                "VALUES(?, ?, ?, ?, ?)", arrayOf(
                            c.getString("instituteId"),
                            c.getString("instituteName"),
                            c.getString("instituteAddress"),
                            c.getString("academicYear"),
                            c.getString("instituteLogo")
                        )
                    )

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                when (volleyError) {
                    is NetworkError -> Toast.makeText(
                        applicationContext,
                        "No Internet connection !",
                        Toast.LENGTH_SHORT
                    ).show()
                    is ServerError -> Toast.makeText(
                        applicationContext,
                        "The server could not be found !",
                        Toast.LENGTH_SHORT
                    ).show()
                    is AuthFailureError -> Toast.makeText(
                        applicationContext,
                        "No Internet connection !",
                        Toast.LENGTH_SHORT
                    ).show()
                    is TimeoutError -> Toast.makeText(
                        applicationContext,
                        "Connection TimeOut !",
                        Toast.LENGTH_SHORT
                    ).show()
                    else -> {
                        Toasty.error(
                            applicationContext,
                            "Login credentials not matching ",
                            Toast.LENGTH_SHORT,
                            true
                        ).show()
                    }
                }
            }) {


            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                sharedpreferences = applicationContext.getSharedPreferences(MyPREFERENCES, 0)
                val h_acess_token = sharedpreferences.getString("accessToken", null)
                val h_token_type = sharedpreferences.getString("tokenType", null)

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["Authorization"] = "$h_token_type $h_acess_token"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            100000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    private fun loadServerSectionData() {

        val accessToken = sharedpreferences.getString("accessToken", null)

        val hitURL = AppController.BaseUrl + "core/setting/class-configuration/list?access_token=" + accessToken

        val jsonObjectRequest = object :
            JsonObjectRequest(
                Method.GET,
                hitURL,
                null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        val getData = response.getJSONArray("item")
                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("classConfigId"))
                            sectionlist.setSectionName(c.getString("classShiftSection"))

                            sectionsArrayList.add(sectionlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_SECTION + "(classConfigId, classShiftSection, instituteId) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(
                                    c.getString("classConfigId"),
                                    c.getString("classShiftSection"),
                                    c.getString("instituteId")
                                )
                            )

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    loadServerSectionResultData()

                },
                Response.ErrorListener { error ->
                    Log.d(
                        "loadServerSectionData",
                        error.toString()
                    )
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            100000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    private fun loadServerSectionResultData() {
        val accessToken = sharedpreferences.getString("accessToken", null)

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2102"

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        sectionsArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val sectionlist = Section()
                            sectionlist.setSectionId(c.getString("id"))
                            sectionlist.setSectionName(c.getString("name"))

                            sectionsArrayList.add(sectionlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_SECTION_RESULT + "(classId, className) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("id"), c.getString("name"))
                            )
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("Grand Final: Section", e.toString())

                    }
                }, Response.ErrorListener {

                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            500000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    private fun loadServerAcademicYearData() {
        val accessToken = sharedpreferences.getString("accessToken", null)

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2101"

        val jsonObjectRequest = object :
            JsonObjectRequest(Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        academicYearsArrayList = ArrayList()
                        val getData = response.getJSONArray("item")
                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val academicYearlist = AcademicYear()
                            academicYearlist.setYearId(c.getString("id"))
                            academicYearlist.setAcademicYear(c.getString("defaultId"))

                            academicYearsArrayList.add(academicYearlist)
                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_ACADEMIC_YEAR + "(academicYearID, academicYear) " +
                                        "VALUES(?, ?)",
                                arrayOf(
                                    c.getString("id"),
                                    c.getString("name")
                                )
                            )

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerCalendarData", e.toString())
                    }

                }, Response.ErrorListener { e -> Log.d("loadServerCalendarData", e.toString()) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = java.util.HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            100000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }
}
