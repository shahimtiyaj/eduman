package com.netizen.eduman

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.shimmer.ShimmerFrameLayout
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_EXAM
import com.netizen.eduman.model.CustomSearchableSpinner
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import es.dmoral.toasty.Toasty
import org.json.JSONException
import java.io.UnsupportedEncodingException
import java.util.*

class FragmenGrandFinal : Fragment(), AdapterView.OnItemSelectedListener {

    private var v: View? = null

    private var btn_grand_final_exam_process: Button? = null
    private var spinner_section: CustomSearchableSpinner? = null
    private var spinner_exam: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null
    private var localSectionID: String? = null
    private var localExamID: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    private val jObjPost: String? = null
    private var txt_back: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    internal var ok: TextView? = null
    internal var no: TextView? = null
    private var myDialog: Dialog? = null

    private var shimmerLayout: ShimmerFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.grand_final_exam_process, container, false)
        initializeViews()
        shimmerLayout = v?.findViewById(R.id.shimmer_layout_id)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "grand_final_exam_process")
        editor.apply()

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        dao?.deleteExamList()
        dao?.close()

        setsectionSpinnerData()

        return v

    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Result Process"
    }
    override fun onPause() {
        super.onPause()
        shimmerLayout?.stopShimmer()
    }

    private fun initializeViews() {
        viewDialog = activity?.let { ViewDialog(it) }
        myDialog = Dialog(requireActivity())

        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_grand_final_exam_process = v?.findViewById<View>(R.id.btn_grand_final_exam_process) as Button

        spinner_section = v?.findViewById<View>(R.id.spinner_grand_final_exam_class) as CustomSearchableSpinner

        spinner_section?.setTitle("Search Class")

        spinner_section?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false

                try {

                    val spinner = parent as CustomSearchableSpinner

                    if (spinner.id == R.id.spinner_grand_final_exam_class) {
                        selectSection = spinner_section?.selectedItem.toString()
                        if (selectSection == resources.getString(R.string.select_class)) {
                            selectSection = ""
                        } else {
                            selectSection = parent.getItemAtPosition(position).toString()

                            val da = DAO(activity!!)
                            da.open()
                            localSectionID = da.GetSectionResultID(selectSection!!)

                            Log.d("localSectionID ", localSectionID)

                            loadServerExamData()

                            da.close()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    Log.d("Student reg  ", "Spinner data")
                }
            }

        }

        spinner_exam = v?.findViewById<View>(R.id.spinner_grand_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        btn_grand_final_exam_process?.setOnClickListener {
            try {
                if (!AppController.instance?.isNetworkAvailable()!!) {
                    AppController.instance?.noInternetConnection()
                }
                else  if (selectSection == null || selectSection?.trim { it <= ' ' }!!.isEmpty() || selectSection == "Select Class") {
                    activity?.let { Toasty.error(it, "Select Class", Toast.LENGTH_SHORT, true).show() };
                }

                else if (selectExam == null || selectExam?.trim { it <= ' ' }!!.isEmpty() || selectExam == "Select Exam") {
                    activity?.let { Toasty.error(it, "Select Exam", Toast.LENGTH_SHORT, true).show() };
                }

                else {
                    GrandExamProcessDataSendToServer()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null

                val allstdListFragment = FragmentSemesterExamDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        try {

                val spinner = parent as Spinner

                if (spinner.id == R.id.spinner_grand_exam) {
                    selectExam = spinner_exam?.selectedItem.toString()
                    if (selectExam == resources.getString(R.string.select_exam)) {
                        selectExam = ""
                    } else {
                        selectExam = parent.getItemAtPosition(position).toString()
                        val da = DAO(activity!!)
                        da.open()
                        localExamID = da.GetExamConfigID(selectExam!!)
                        da.close()
                    }
            }
            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } finally {
                Log.d("Grand final ", "Spinner data")
            }
    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val grandFinal = dao?.allSectionResultData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_class))
            for (i in grandFinal?.indices!!) {
                grandFinal[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(
                context!!, android.R.layout.simple_spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("General exam ", "Section spinner data")
        }
    }

    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, exam)
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("General exam ", "Section spinner data")
        }
    }


    private fun loadServerExamData() {

        val hitURL = AppController.BaseUrl + "exam/grand-final/configuration/list/by/class-id?classId="+localSectionID+"&access_token=" + accessToken

        shimmerLayout?.visibility=View.VISIBLE
        shimmerLayout?.startShimmer()

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)

        object : JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)

                            val examList = Exam()
                            examList.setExamId(c.getString("examConfigId"))
                            examList.setExamName(c.getString("examName"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_EXAM + "(examConfigId, ExamId, ExamName) " +
                                        "VALUES(?, ?, ?)", arrayOf(c.getString("examConfigId"), " ", c.getString("examName")))

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("Grand Final: Exam", e.toString())

                        shimmerLayout?.visibility=View.GONE
                        shimmerLayout?.stopShimmer()

                    }
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                },
                Response.ErrorListener { error ->
                    Log.d("Grand Final: Exam", error.toString())
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    fun GrandExamProcessDataSendToServer() {

        val hitURL = AppController.BaseUrl + "/result/process/class-wise/grand-final?access_token=" + accessToken + "&examConfigurationId=" + localExamID

        viewDialog?.showDialog()

        val postRequest = object : CustomRequest(
            Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Response: $response")

                try {

                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        showPopup()
                        viewDialog?.hideDialog()

                    } else {
                        activity?.let { Toasty.error(it, status, Toast.LENGTH_LONG, true).show() }
                        viewDialog?.hideDialog()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)

                viewDialog?.hideDialog()

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                }

                else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }

                else {
                    activity?.let { Toasty.error(it, "Grand Final Result Already Processed", Toast.LENGTH_SHORT, true).show() };
                }

            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return jObjPost?.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    fun showPopup() {

        val ok: Button

        val txt: TextView

        myDialog?.setContentView(R.layout.sucessfull_alert_layout)
        ok = myDialog?.findViewById(R.id.back) as Button

        txt = myDialog?.findViewById(R.id.tvItemSelected1)!!
        txt.setText("Grand Final Result Successfully Processed !")

        ok.setOnClickListener {
            myDialog?.dismiss()
        }

        myDialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }

    companion object {
        private val TAG = "FragmenGrandFinal"
    }

}
