package com.netizen.eduman

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.netizen.eduman.model.AcademicYear
import com.netizen.eduman.model.Designation
import com.netizen.eduman.model.Group
import com.netizen.eduman.model.Section
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.ArrayList
import java.util.HashMap


class HrRegActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    private val Tag = javaClass.name
    private val context = this@HrRegActivity
    private var btn_reg_hr: Button? = null
    private var spinner_category_hr: Spinner? = null
    private var spinner_designation_hr: Spinner? = null
    private var spinner_hr_id: Spinner? = null
    private var spinner_gender_hr: Spinner? = null
    private var spinner_religion_hr: Spinner? = null

    private var inputHRCustomID: EditText? = null
    private var inputHRName: EditText? = null
    private var inputHRPhone: EditText? = null

    private var hrCustom: String? = null
    private var hrname: String? = null
    private var phone_hr: String? = null

    private var selectCategory_hr: String? = null
    private var selectIdType_hr: String? = null
    private var selectGender_hr: String? = null
    private var selectReligion_hr: String? = null
    private var selectDesignation_hr: String? = null
    private var id_type_hr: String? = null

    private var inputLayoutCustomID_hr: TextInputLayout? = null
    private var inputLayoutHRName: TextInputLayout? = null
    private var inputLayouHRMobile: TextInputLayout? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitle: TextView? = null

    internal var academicYearsArrayList = ArrayList<AcademicYear>()
    internal var designationArrayList = ArrayList<Designation>()
    internal var sectionsArrayList = ArrayList<Section>()
    internal var groupsArrayList = ArrayList<Group>()

    private var jsonObjectPost: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hr_enlistment_form)
        toolBarInit()
        initializeViews()
        loadServerHRCategoryData()
    }

    /**
     * SetUp toolbar method
     */
    fun toolBarInit() {
        // Lookup the toolbar in activity layout
        toolbar = findViewById(R.id.toolbar) as Toolbar
        // Lookup the toolbar title  in activity
        toolbarTitle = toolbar!!.findViewById(R.id.toolbar_title) as TextView
        // Lookup the toolbar in activity layout
        setSupportActionBar(toolbar)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        //Default home button enable false
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbarTitle!!.text = "HR Enlistment Form"

        toolbar!!.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun initializeViews() {
        inputHRCustomID = findViewById(R.id.et_hr_custom_id) as EditText
        inputHRName = findViewById(R.id.et_hr_name) as EditText
        inputHRPhone = findViewById(R.id.et_hr_mobile) as EditText

        inputLayoutHRName = findViewById(R.id.input_layout_hr_name) as TextInputLayout
        inputLayoutCustomID_hr = findViewById(R.id.input_layout_hr_custom_id) as TextInputLayout
        inputLayouHRMobile = findViewById(R.id.input_layout_mobile_hr) as TextInputLayout

        inputHRCustomID!!.addTextChangedListener(MyTextWatcher(inputHRCustomID!!))
        inputHRName!!.addTextChangedListener(MyTextWatcher(inputHRName!!))
        inputHRPhone!!.addTextChangedListener(MyTextWatcher(inputHRPhone!!))

        btn_reg_hr = findViewById(R.id.btn_hr_reg_submit) as Button

        spinner_gender_hr = findViewById(R.id.spinner_gender_hr) as Spinner
        spinner_gender_hr!!.onItemSelectedListener = this

        spinner_religion_hr = findViewById(R.id.spinner_religion_hr) as Spinner
        spinner_religion_hr!!.onItemSelectedListener = this

        spinner_category_hr = findViewById(R.id.spinner_category_hr) as Spinner
        spinner_category_hr!!.onItemSelectedListener = this

        spinner_designation_hr = findViewById(R.id.spinner_designation_hr) as Spinner
        spinner_designation_hr!!.onItemSelectedListener = this

        spinner_hr_id = findViewById(R.id.spinner_hr_id_type) as Spinner
        spinner_hr_id!!.onItemSelectedListener = this

        btn_reg_hr!!.setOnClickListener {
            try {
                submitRegistration()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun submitRegistration() {
        /*        if (!validateName()) {
            return;
        }
        if (!validatePhone()) {
            return;
        }*/
        sentRegistrationInformation()
    }

    private fun sentRegistrationInformation() {
        hrCustom = inputHRCustomID!!.text.toString()
        hrname = inputHRName!!.text.toString()
        phone_hr = inputHRPhone!!.text.toString()

        dataSendToServer()
    }

    fun dataSendToServer() {
        val a = "5d80f06c-b278-4122-8116-88db9b382662"
        val hitURL = "http://192.168.31.5:8080/staff/basic/save?registrationType=$selectIdType_hr&access_token=$a"

        //Json object for posting to server and getting object array
        val jsonArray = JSONArray()
        val arrayItem = JSONObject()
        try {

            arrayItem.put("category", selectCategory_hr)
            arrayItem.put("customId", hrCustom)
            arrayItem.put("designationId", selectDesignation_hr)
            arrayItem.put("gender", selectGender_hr)
            arrayItem.put("staffMobile1", phone_hr)
            arrayItem.put("staffName", hrname)
            arrayItem.put("staffReligion", selectReligion_hr)
            jsonArray.put(arrayItem)

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        //here convert json object to string for posting data to server
        jsonObjectPost = jsonArray.toString()

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        Toast.makeText(context, status, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(context, "Registration fail!!", Toast.LENGTH_LONG).show()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        applicationContext,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        applicationContext,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        applicationContext,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(
                        applicationContext,
                        "Parsing error! Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        applicationContext,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        applicationContext,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {

            /**
             * Passing some request in body. Basically here we have passed json object request
             */
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jsonObjectPost == null) null else jsonObjectPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jsonObjectPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    private fun loadServerHRCategoryData() {

        val hitURL = "https://api.netizendev.com:2083/emapi/core/setting/list/by-type-id?access_token=276da74b-fdb4-4a4b-8fdd-b2ef664735bc&typeId=2601"

        val jsonObjectRequest = object :
            JsonObjectRequest(Request.Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        designationArrayList = ArrayList()
                        //JSONObject jsonObj = response.getJSONObject("result");
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val designationlist = Designation()
                            designationlist.setDesignationId(c.getString("id"))
                            designationlist.setDesignationName(c.getString("name"))

                            designationArrayList.add(designationlist)

                            if (!designationArrayList.isEmpty()) {
                                setcategorySpinnerData(designationArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@HrRegActivity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(
                        this@HrRegActivity,
                        error.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["access_token"] = "276da74b-fdb4-4a4b-8fdd-b2ef664735bc"
                headers["typeId"] = "2601"

                return headers
            }

        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest)
    }

    private fun setcategorySpinnerData(designationArrayList: ArrayList<Designation>) {
        val hrdesignation = ArrayList<String>()
        hrdesignation.add(resources.getString(R.string.select_designation))
        for (i in designationArrayList.indices) {
            hrdesignation.add(designationArrayList[i].getDesignationId().toString())
        }

        val categoryAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, hrdesignation)
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_designation_hr!!.adapter = categoryAdapter
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val spinner = parent as Spinner

        if (spinner.id == R.id.spinner_category_hr) {
            selectCategory_hr = spinner_category_hr!!.selectedItem.toString()
            if (selectCategory_hr == resources.getString(R.string.select_category)) {
                selectCategory_hr = ""
            } else {
                selectCategory_hr = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_designation_hr) {
            selectDesignation_hr = spinner_designation_hr!!.selectedItem.toString()
            if (selectDesignation_hr == resources.getString(R.string.select_designation)) {
                selectDesignation_hr = ""
            } else {
                selectDesignation_hr = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_gender) {
            selectGender_hr = spinner_gender_hr!!.selectedItem.toString()
            if (selectGender_hr == resources.getString(R.string.select_gender)) {
                selectGender_hr = ""
            } else {
                selectGender_hr = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_religion) {
            selectReligion_hr = spinner_religion_hr!!.selectedItem.toString()
            if (selectReligion_hr == resources.getString(R.string.select_gender)) {
                selectReligion_hr = ""
            } else {
                selectReligion_hr = parent.getItemAtPosition(position).toString()
            }
        }

        if (spinner.id == R.id.spinner_hr_id_type) {
            selectIdType_hr = spinner_hr_id!!.selectedItem.toString()
            if (selectIdType_hr == resources.getString(R.string.select_student_id_type)) {
                selectIdType_hr = ""
            } else {
                selectIdType_hr = parent.getItemAtPosition(position).toString()
                if (selectIdType_hr == "Custom") {
                    id_type_hr = 1.toString()
                    inputHRCustomID!!.visibility = View.VISIBLE

                } else {
                    id_type_hr = 0.toString()
                    inputHRCustomID!!.visibility = View.GONE
                }
            }
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private inner class MyTextWatcher (private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.et_hr_name -> validateName()
                R.id.et_hr_mobile -> validatePhone()
            }
        }
    }


    private fun validatePhone(): Boolean {
        if (inputHRPhone!!.text.toString().trim { it <= ' ' }.isEmpty() || inputHRPhone!!.length() != 11) {
            inputLayouHRMobile!!.error = getString(R.string.err_msg_phone)
            requestFocus(inputHRPhone!!)
            return false
        } else {
            inputLayouHRMobile!!.isErrorEnabled = false
        }
        return true
    }


    private fun validateName(): Boolean {
        if (inputHRName!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputHRName!!.error = getString(R.string.err_msg_name)
            requestFocus(inputHRName!!)
            return false
        } else {
            inputLayoutHRName!!.isErrorEnabled = false
        }
        return true
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    companion object {
        private val TAG = "HrRegActivity"
    }
}

