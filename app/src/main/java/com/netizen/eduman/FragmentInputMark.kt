package com.netizen.eduman

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.InputMarkAdapter
import com.netizen.eduman.Adapter.MarkDistributionAdapter
import com.netizen.eduman.FragmentInputMarkSelection.Companion.localExamID
import com.netizen.eduman.FragmentInputMarkSelection.Companion.localGroupID
import com.netizen.eduman.FragmentInputMarkSelection.Companion.localSectionID
import com.netizen.eduman.FragmentInputMarkSelection.Companion.localSubjectID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_INPUT_STUDENT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_SCALE_DISTRIBUTION
import com.netizen.eduman.model.InputMark
import com.netizen.eduman.model.MarkDistribution
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.lang.NullPointerException
import java.util.*

class FragmentInputMark : Fragment() {

    private var btn_input_mark_save: Button? = null
    private var v: View? = null

    private var recyclerView: RecyclerView? = null
    private var recyclerView1: RecyclerView? = null
    private var adapter: InputMarkAdapter? = null
    private var adapter1: MarkDistributionAdapter? = null

    private var inputMarkArrayList: ArrayList<InputMark>? = null
    private var markDistributionArrayList: ArrayList<MarkDistribution>? = null

    private var mLayoutManager: LinearLayoutManager? = null
    private var mLayoutManager1: LinearLayoutManager? = null
    private var jObjPost: String? = null

    private var txt_back: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    private var linearLayout1: LinearLayout? = null
    private var flag = 0
    private var inputmark_title: TextView? = null

    internal var ok: TextView? = null
    internal var no: TextView? = null
    private var myDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.mark_input_recyler_list_layout, container, false)

        initializeViews()
        recyclerViewInit()

        val sharedPreferences = activity?.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putString("now", "input_mark_std_details")
        editor?.apply()

        if (!AppController.instance?.isNetworkAvailable()!!) {
            AppController.instance?.noInternetConnection()
        } else {
            GetMarkScaleDistrubution()
            GetMarkStudentList()
        }

        return v
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        activity?.title = "Mark Input"
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }
        myDialog = Dialog(requireActivity())

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_input_mark_save = v?.findViewById<View>(R.id.btn_input_mark_save) as Button

        btn_input_mark_save?.setOnClickListener {
            try {
                if (inputMarkArrayList?.isNotEmpty()!!) {
                    markInputDataSendToServer()
                } else {
                    activity?.let { it1 ->
                        Alert(
                            it1,
                            R.drawable.ic_not_found,
                            "Failure",
                            "Sorry ! No Data Found !"
                        )
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title = null
                val allstdListFragment = TabFragmentMIU()
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                fragmentTransaction?.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction?.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        inputmark_title = v?.findViewById<View>(R.id.inputmark_title) as TextView
        linearLayout1 = v?.findViewById(R.id.linear_mark_scale1) as LinearLayout

        inputmark_title?.setOnClickListener {
            try {

                if (flag == 0) {
                    linearLayout1?.visibility = View.VISIBLE
                    recyclerView1?.visibility = View.VISIBLE
                    flag = 1
                } else if (flag == 1) {
                    linearLayout1?.visibility = View.GONE
                    recyclerView1?.visibility = View.GONE
                    flag = 0
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun recyclerViewInit() {

        // Initialize item list
        inputMarkArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.mark_input_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = activity?.let { InputMarkAdapter(it, inputMarkArrayList!!) }
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter

        // Initialize item list
        markDistributionArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView1 = v?.findViewById<View>(R.id.mark_disbution_recyle_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter1 = activity?.let { MarkDistributionAdapter(it, markDistributionArrayList!!) }
        //GridLayoutManager shows items in a grid.
        mLayoutManager1 = LinearLayoutManager(context)
        // Set layout manager to position the items
        recyclerView1?.layoutManager = mLayoutManager1
        // Set the default animator
        recyclerView1?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView1?.adapter = adapter1
    }


    fun GetMarkScaleDistrubution() {
        val hitURL =
            AppController.BaseUrl + "exam/mark/config/list/by/subject-id?access_token=" + accessToken +
                    "&classConfigurationId=" + localSectionID + "&groupId=" + localGroupID + "&subjectId=" + localSubjectID + "&examConfigId=" + localExamID

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val markDistribution = MarkDistribution()

                        markDistribution.exameCode = c.getString("shortCodeName")
                        markDistribution.totalMark = c.getString("totalMark")
                        markDistribution.passMark = c.getString("passMark")
                        markDistribution.acceptance = c.getString("acceptance")

                        markDistributionArrayList?.add(markDistribution)

                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + TABLE_MARK_SCALE_DISTRIBUTION + "(shortCodeName, totalMark, " +
                                    "passMark, acceptance, defaultId) " +
                                    "VALUES(?, ?, ?, ?, ?)"),

                            arrayOf(
                                c.getString("shortCodeName"),
                                c.getString("totalMark"),
                                c.getString("passMark"),
                                c.getString("acceptance"),
                                c.getString("defaultId")
                            )
                        )
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                adapter1?.notifyDataSetChanged()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)

                if (activity != null) {

                    when (volleyError) {
                        is NetworkError -> {
                            Toast.makeText(
                                activity,
                                "No Internet connection !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is ServerError -> {
                            Toast.makeText(
                                activity,
                                "The server could not be found.!",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        is AuthFailureError -> {
                            Toast.makeText(
                                activity,
                                "No Internet connection !",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        is ParseError -> {
                            Toast.makeText(
                                activity,
                                "Parse Error !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is TimeoutError -> {
                            Toast.makeText(
                                activity,
                                "Connection TimeOut !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Toast.makeText(
                                activity,
                                "Sorry No Data Found !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }


    private fun GetMarkStudentList() {

        viewDialog?.showDialog()

        val hitURL =
            AppController.BaseUrl + "exam/mark/student/list?access_token=" + accessToken + "&classConfigurationId=" + localSectionID +
                    "&groupId=" + localGroupID + "&subjectId=" + localSubjectID + "&examConfigurationId=" + localExamID

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                val message = response.getString("message")

                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    //val message = response.getString("message")

                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val inputMark = InputMark()

                        inputMark.in_mark_st_identification_id = c.getString("identificationId")
                        inputMark.in_mark_st_id = c.getString("customStudentId")
                        inputMark.in_mark_st_roll = c.getString("studentRoll")
                        inputMark.in_mark_st_name = c.getString("studentName")
                        inputMark.shortCode1 = c.getString("shortCode1")
                        inputMark.shortCode2 = c.getString("shortCode2")
                        inputMark.shortCode3 = c.getString("shortCode3")
                        inputMark.shortCode4 = c.getString("shortCode4")
                        inputMark.shortCode5 = c.getString("shortCode5")
                        inputMark.shortCode6 = c.getString("shortCode6")

                        inputMarkArrayList?.add(inputMark)

                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + TABLE_MARK_INPUT_STUDENT + "(markinputId, identificationId, customStudentId, studentRoll, " +
                                    "studentName, shortCode1, shortCode2, shortCode3, shortCode4, shortCode5, shortCode6) " +
                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),

                            arrayOf(
                                "0",
                                c.getString("identificationId"),
                                c.getString("customStudentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("shortCode1"),
                                c.getString("shortCode2"),
                                c.getString("shortCode3"),
                                c.getString("shortCode4"),
                                c.getString("shortCode5"),
                                c.getString("shortCode6")

                            )
                        )
                    }


                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                if (inputMarkArrayList.isNullOrEmpty()) {
                    activity?.let {
                        Alert(
                            it,
                            R.drawable.ic_not_found,
                            "Failure",
                            message
                        )
                    }
                }
                //Notify adapter if data change
                adapter?.notifyDataSetChanged()

                viewDialog?.hideDialog()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                //d(TAG, "Error: " + volleyError.message)
                try {


                    viewDialog?.hideDialog()

                    if (context !== null) {

                        when (volleyError) {
                            is NetworkError -> {
                                Toast.makeText(
                                    context,
                                    "No Internet connection !",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            is ServerError -> {
                                Toast.makeText(
                                    context,
                                    "The server could not be found.!",
                                    Toast.LENGTH_SHORT
                                ).show()

                            }
                            is AuthFailureError -> {
                                Toast.makeText(
                                    context,
                                    "No Internet connection !",
                                    Toast.LENGTH_SHORT
                                ).show()

                            }
                            is ParseError -> {
                                Toast.makeText(
                                    context,
                                    "Parse Error !",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            is TimeoutError -> {
                                Toast.makeText(
                                    context,
                                    "Connection TimeOut !",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            else -> {
                                Toast.makeText(
                                    context,
                                    "Sorry No Data Found !",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }


                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }

            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }


    fun Alert(ctx: Context, icon: Int, title: String, message: String) {
        AlertDialog.Builder(ctx)
            .setIcon(icon)
            .setTitle(title)
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton("OK") { _, _ ->

                activity?.title = null

                val da = activity?.let { DAO(it) }
                da?.open()
                da?.deleteInputMarkSectionData()
                da?.close()

                val allstdListFragment = TabFragmentMIU()
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                fragmentTransaction?.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction?.commit()
            }
            .setNegativeButton("Cancel") { _, _ ->
                activity?.title = null

                val da = activity?.let { DAO(it) }
                da?.open()
                da?.deleteInputMarkSectionData()
                da?.close()

                val allstdListFragment = TabFragmentMIU()
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                fragmentTransaction?.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction?.commit()
            }.show()
    }

    fun markInputDataSendToServer() {
        val hitURL = AppController.BaseUrl + "exam/mark/input?access_token=" + accessToken
        viewDialog?.showDialog()

        InputMarkJsonData()

        val postRequest = object : CustomRequest(
            Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Input Mark Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {

                        viewDialog?.hideDialog()

                        showPopup()

                    } else {
                        activity?.let {
                            Alert(
                                it,
                                R.drawable.ic_error_black_24dp,
                                "Input mark",
                                status
                            )
                        }
                        viewDialog?.hideDialog()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }
                viewDialog?.hideDialog()

            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                if (activity != null) {

                    when (volleyError) {
                        is NetworkError -> {
                            Toast.makeText(
                                activity,
                                "Cannot connect to Internet...Please check your connection!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is ServerError -> {
                            Toast.makeText(
                                activity,
                                "The server could not be found. Please try again after some time!!",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        is AuthFailureError -> {
                            Toast.makeText(
                                activity,
                                "Cannot connect to Internet...Please check your connection!",
                                Toast.LENGTH_SHORT
                            ).show()


                        }
                        is ParseError -> {
                            Toast.makeText(
                                activity,
                                "Parsing error! Please try again after some time!!",
                                Toast.LENGTH_SHORT
                            )
                                .show()

                        }
                        is NoConnectionError -> {
                            Toast.makeText(
                                activity,
                                "Cannot connect to Internet...Please check your connection!",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        is TimeoutError -> {
                            Toast.makeText(
                                activity,
                                "Connection TimeOut! Please check your internet connection",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        else -> {

                            Toast.makeText(
                                activity,
                                "Opps! Imput Marks ail to Save",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                    }
                }
            }) {

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }
            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }
        }


        // Volley socket time its use for loading huge amount data
        val socketTimeout =
            1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }


    private fun InputMarkJsonData() {

        inputMarkArrayList = ArrayList()

        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val inputMarks = dao?.getALLInputMark()

            val jsonObj = JSONObject()
            val jsonArray = JSONArray()

            for (mark in inputMarks!!) {

                val arrayItem = JSONObject()

                arrayItem.put("identificationId", mark.in_mark_st_identification_id)

                if (mark.shortCode1 == "") {
                    arrayItem.put("shortCode1", "0.0")
                } else {
                    arrayItem.put("shortCode1", mark.shortCode1)
                }
                if (mark.shortCode2 == "") {
                    arrayItem.put("shortCode2", "0.0")
                } else {
                    arrayItem.put("shortCode2", mark.shortCode2)
                }

                if (mark.shortCode3 == "") {
                    arrayItem.put("shortCode3", "0.0")
                } else {
                    arrayItem.put("shortCode3", mark.shortCode3)
                }

                if (mark.shortCode4 == "") {
                    arrayItem.put("shortCode4", "0.0")
                } else {
                    arrayItem.put("shortCode4", mark.shortCode4)
                }


                if (mark.shortCode5 == "") {
                    arrayItem.put("shortCode5", "0.0")
                } else {
                    arrayItem.put("shortCode5", mark.shortCode5)
                }

                if (mark.shortCode6 == "") {
                    arrayItem.put("shortCode6", "0.0")
                } else {
                    arrayItem.put("shortCode6", mark.shortCode6)
                }

                jsonArray.put(arrayItem)
            }

            try {

                jsonObj.put("examMarkInputRqHelpers", jsonArray)
                jsonObj.put("examConfigurationId", localExamID)//76
                jsonObj.put("classConfigurationId", localSectionID) //143189
                jsonObj.put("groupId", localGroupID) //3000432105
                jsonObj.put("subjectId", localSubjectID) //1001492202
                jsonObj.put("type", "insert")

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            jObjPost = jsonObj.toString()
            Log.d("Input Mark Json", jObjPost)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showPopup() {
        val ok: Button

        val txt: TextView

        myDialog?.setContentView(R.layout.sucessfull_alert_layout)
        ok = myDialog?.findViewById(R.id.back) as Button

        txt = myDialog?.findViewById(R.id.tvItemSelected1)!!

        txt.text = "Inputted Mark Successfully Saved !"

        myDialog?.setCancelable(false)

        ok.setOnClickListener {
            activity?.title = null
            val da = activity?.let { it1 -> DAO(it1) }
            da?.open()
            da?.deleteInputMarkSectionData()
            da?.close()

            myDialog?.dismiss()
            val allstdListFragment = TabFragmentMIU()
            val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.fragment_container, allstdListFragment)
            fragmentTransaction?.commit()
        }

        myDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }

    companion object {

        private val TAG = "FragmentInputMark"
    }
}
