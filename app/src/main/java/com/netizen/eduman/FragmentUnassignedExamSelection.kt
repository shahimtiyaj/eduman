package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.shimmer.ShimmerFrameLayout
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import es.dmoral.toasty.Toasty
import org.json.JSONException
import java.util.*

class FragmentUnassignedExamSelection : Fragment(), AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    private var btn_unassigned_mark_search: Button? = null

    private var spinner_exam: Spinner? = null

    private var selectExam: String? = null

    internal var examArrayList = ArrayList<Exam>()

    private var txt_back: TextView? = null

    private var v: View? = null
    var viewDialog: ViewDialog? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    private var shimmerLayout: ShimmerFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.unassigned_mark_exam_selection, container, false)

        shimmerLayout = v?.findViewById(R.id.shimmer_layout_id)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "unassigned_mark_exam_selection")
        editor.apply()

        initializeViews()
        loadServerExamData()

        val da = DAO(activity!!)
        da.open()
        da.deleteGroupList()
        da.deleteExamList()
        da.deleteSubjectList()
        da.clearInputMark()
        da.clearMarkDistrubution()

        da.close()

        return v
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        shimmerLayout?.stopShimmer()
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        spinner_exam = v?.findViewById<View>(R.id.spinner_mark_in_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        btn_unassigned_mark_search = v?.findViewById<View>(R.id.btn_unassigned_mark_in_search) as Button

        btn_unassigned_mark_search?.setOnClickListener {
            try {

                if (selectExam == null || selectExam?.trim { it <= ' ' }!!.isEmpty() || selectExam == "Select Exam") {
                    activity?.let {
                        Toasty.error(it, "Select Exam", Toast.LENGTH_SHORT, true).show()
                    }
                } else if (!AppController.instance?.isNetworkAvailable()!!) {
                    AppController.instance?.noInternetConnection()
                } else {

                    val unAssignedResultragment = FragmentUnassignedResult()

                    val bundle = Bundle()
                    bundle.putString("exam", selectExam)
                    unAssignedResultragment.arguments = bundle

                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, unAssignedResultragment)
                    fragmentTransaction.commit()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title = null
                val allstdListFragment = FragmentSemesterExamReportDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_mark_in_exam) {
                selectExam = spinner_exam?.selectedItem.toString()
                if (selectExam == resources.getString(R.string.select_exam)) {
                    selectExam = ""
                } else {
                    selectExam = parent.getItemAtPosition(position).toString()
                    val da = DAO(activity!!)
                    da.open()
                    localExamID = da.GetUnAssignedExamID(selectExam!!)

                    da.close()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Unassigned mark", "Unassigned Exam Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClick(v: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter = ArrayAdapter(activity,
                android.R.layout.simple_spinner_item, exam)
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Unassigned Input mark", "Exam unassigned spinner data")
        }
    }

    private fun loadServerExamData() {

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?typeId=2201&access_token="+accessToken

        shimmerLayout?.visibility=View.VISIBLE
        shimmerLayout?.startShimmer()

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)

                            val examList = Exam()
                            examList.setExamId(c.getString("id"))
                            examList.setExamName(c.getString("name"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_EXAM + "(ExamId, ExamName) " +
                                        "VALUES(?, ?)",
                                arrayOf(

                                    c.getString("id"),
                                    c.getString("name")
                                )
                            )

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        shimmerLayout?.visibility=View.GONE
                        shimmerLayout?.stopShimmer()
                    }
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()
                },
                Response.ErrorListener { e ->
                    Log.d("Input mark: Exam", e.toString())
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        val socketTimeout = 1000000//10 Minutes-change to what you want//1000000 milliseconds = 10 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {
        private val TAG = "FragmentUnassignedExamSelection"
        var localExamID: String? = null
    }
}
