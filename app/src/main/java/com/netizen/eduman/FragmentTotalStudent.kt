package com.netizen.eduman


import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.TotalStudentAdapter
import com.netizen.eduman.FragmentSAttendanceSelection.Companion.localPeriodID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.TotalStudent
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*

class FragmentTotalStudent : Fragment() {

    internal var totalStudentArrayList = ArrayList<TotalStudent>()
    private var recyclerView: RecyclerView? = null
    private var adapter: TotalStudentAdapter? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var v: View? = null
    private var status: String? = null
    private var header: String? = null
    private var section: String? = null
    private var section_id: String? = null
    private var section_date: String? = null

    private var header_txt: TextView? = null
    private var section_txt: TextView? = null
    private var txt_back: TextView? = null

    private var searchView: SearchView? = null
    var viewDialog: ViewDialog? = null


    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    private var swipeRefresh: SwipeRefreshLayout? = null
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.total_student_recyler_list_pop_up_st_smry, container, false)
        initializeViews()
        recyclerViewInit()
        setHasOptionsMenu(true)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_atd_present_absent_leave")
        editor.apply()

        try {

            if (savedInstanceState == null) {
                if (arguments != null) {

                    status = arguments?.getString("status")
                    header = arguments?.getString("header")
                    section = arguments?.getString("section")
                    section_id = arguments?.getString("section_id")
                    section_date = arguments?.getString("section_date")

                    val mainFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                    val parsedDate = mainFormat.parse(section_date)
                    val formatGet = SimpleDateFormat("MM/dd/yyyy", Locale.US)
                    section_date = formatGet.format(parsedDate)

                    header_txt?.text = header
                    section_txt?.text = section

                    GetTotalStudentAttendanceSummary()
                }
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }

        return v

    }

    override fun onResume() {
        super.onResume()
        activity?.title="Report"
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        return if (id == R.id.action_search) {

            true
        } else super.onOptionsItemSelected(item)

    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

        // Associate searchable configuration with the SearchView
        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #5459EC>" + resources.getString(R.string.search_student) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier("android:id/search_src_text", null, null)
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.colorEduman)

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_button", null, null)
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_close_btn", null, null)
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })
    }

    private fun initializeViews() {
        viewDialog = activity?.let { ViewDialog(it) };
        mHandler = Handler()
        swipeRefresh = v?.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
        swipeRefresh?.setOnRefreshListener {
            mRunnable = Runnable {
                adapter?.notifyDataSetChanged()
                totalStudentArrayList
                swipeRefresh?.isRefreshing = false
            }

            mHandler.postDelayed(
                mRunnable, 50
            )
        }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        header_txt = v?.findViewById<View>(R.id.attendance_summary_header_popup) as TextView
        section_txt = v?.findViewById<View>(R.id.total_atdnce_smry_text_popup_section) as TextView

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null

                val allstdListFragment = FragmentSAttendanceList()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun recyclerViewInit() {
        // Initialize item list
        totalStudentArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.total_st_atd_smry_rcyler_list_id_popup) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = TotalStudentAdapter(activity!!, totalStudentArrayList)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    fun GetTotalStudentAttendanceSummary() {

        val hitURL = AppController.BaseUrl + "attendance/report/status-and-sec-wise/std-details/by/period-id/and/date?attendanceDate=" + section_date + "&periodId=" + localPeriodID + "&classConfigId=" + section_id + "&attendanceStatus=$status&access_token=" + accessToken

        viewDialog?.showDialog()

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val totalStudent = TotalStudent()

                        totalStudent.setStd_id(c.getString("customStudentId"))
                        totalStudent.setStd_roll(c.getString("studentRoll"))
                        totalStudent.setStd_name(c.getString("studentName"))
                        totalStudent.setStd_gender(c.getString("gender"))
                        totalStudent.setStd_mobile_no(c.getString("mobileNo"))
                        totalStudent.setStd_status(c.getString("attendanceStatus"))

                        // adding item to ITEM list
                        totalStudentArrayList.add(totalStudent)

                    }
                    if (totalStudentArrayList.isEmpty()) {
                        Log.d("Attendance: summary", "Attendance summary data not found")
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.ic_not_found,
                                "Failure",
                                "Sorry! no data found"
                            )
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                adapter?.notifyDataSetChanged()

                viewDialog?.hideDialog()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)

                viewDialog?.hideDialog()

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }
        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {
        private val TAG = "FragmentTotalStudent"
    }
}
