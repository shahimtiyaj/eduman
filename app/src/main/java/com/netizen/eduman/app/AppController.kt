package com.netizen.eduman.app

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDelegate
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class AppController : Application() {
    private var mRequestQueue: RequestQueue? = null

    val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(applicationContext)
            }
            return this.mRequestQueue!!
        }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

   /*     val fabric = Fabric.Builder(this)
            .kits(Crashlytics())
            .debuggable(true)
            .build()
        Fabric.with(fabric)*/
    }

    companion object {

        val TAG = AppController::class.java.simpleName
       // val BaseUrl = "https://api.netizendev.com:2083/emapi/" //Dev Live Url
        val BaseUrl = "https://api.edumanbd.com/emapi/" //Product Live Url
        //val BaseUrl = "http://192.168.0.5:8080/" //Local Url

        var context: Context? = null
            private set
        @get:Synchronized
        var instance: AppController? = null
            private set

        val mainUrl: String
            get() = BaseUrl
    }

    fun Alert(ctx: Context?, icon: Int?, title: String?, message: String?) {
        ctx?.let {
            icon?.let { it1 ->
                AlertDialog.Builder(it)
                    .setIcon(it1)
                    .setCancelable(false)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("OK") { dialog, which ->
                        dialog.dismiss()
                    }
                    .setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }.show()
            }
        }
    }

    fun noInternetConnection() {
        val context = context
        val inflater = LayoutInflater.from(context)
        val customToastroot = inflater.inflate(com.netizen.eduman.R.layout.network_no_connection, null)
        val customtoast = Toast(context)
        customtoast.view = customToastroot
        customtoast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 0)
        customtoast.duration = Toast.LENGTH_LONG
        customtoast.show()
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
