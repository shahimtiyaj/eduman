package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO


class FragmentAttdanceDash : Fragment() {

    private var studentATDCard: CardView? = null
    private var attendanceReportCard: CardView? = null
    lateinit var v: View

    private var txt_back: TextView? = null
    private var txt_scl_name: TextView? = null
    private var txt_inst_id: TextView? = null
    private var txt_academic_year: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.attendance_dashboard_final, container, false)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_atd_dash")
        editor.apply()

        studentATDCard = v.findViewById<View>(R.id.atd_dash_std_cardId) as CardView
        attendanceReportCard = v.findViewById<View>(R.id.std_atd_report_card) as CardView

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        txt_scl_name = v.findViewById<View>(R.id.school_name) as TextView
        txt_inst_id = v.findViewById<View>(R.id.school_id) as TextView
        txt_academic_year = v.findViewById<View>(R.id.academic_year) as TextView

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val inst_info = dao?.getIntituteInfo()

        txt_scl_name?.setText(inst_info?.getuserinstituteName())
        txt_inst_id?.setText("  Institute ID        :  " + inst_info?.getinstituteId())
        txt_academic_year?.setText("Academic Year  :  " + inst_info?.getacademic_year())

        OnclickView()

        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Attendance"
    }

    fun OnclickView() {

        studentATDCard?.setOnClickListener {
            try {
                activity?.title=null

                val stdtabFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                fragmentTransaction?.replace(R.id.fragment_container, stdtabFragment)
                fragmentTransaction?.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        attendanceReportCard?.setOnClickListener {
            activity?.title=null

            val dashFragment = FragmentStdHRreportDasboard()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, dashFragment)
            fragmentTransaction.commit()
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null

                val allstdListFragment = FragmentDashboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
}
