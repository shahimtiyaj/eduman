package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.CustomSearchableSpinner
import com.netizen.eduman.model.Section
import es.dmoral.toasty.Toasty
import java.util.*

class FragGetStudentListSelection : Fragment() {

    private var btn_get_all_std_list: Button? = null
    private var btn_enroll_student: Button? = null
    private var btn_find_student: Button? = null
    private var spinner_section: CustomSearchableSpinner? = null
    private var selectSection: String? = null
    internal var sectionsArrayList = ArrayList<Section>()

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    private var txt_back: TextView? = null

    private var v: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.student_list_selection_layout, container, false)

        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "student_get_list")
        editor.apply()

        setsectionSpinnerData()

        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Report"
    }

    private fun initializeViews() {

        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_get_all_std_list = v?.findViewById<View>(R.id.btn_get_list) as Button

        btn_find_student?.setBackgroundColor(resources.getColor(R.color.colorEduman))
        btn_find_student?.setTextColor(resources.getColor(R.color.white))

        btn_enroll_student?.setBackgroundColor(Color.WHITE)
        btn_enroll_student?.setTextColor(Color.BLUE)

        btn_enroll_student?.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add_blue, 0);
        btn_find_student?.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search_white, 0);

        spinner_section = v?.findViewById<View>(R.id.spinner_section) as CustomSearchableSpinner

        spinner_section?.setTitle("Search Section")

        spinner_section?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false

                try {

                    val spinner = parent as CustomSearchableSpinner

                    if (spinner.id == R.id.spinner_section) {
                        selectSection = spinner_section!!.selectedItem.toString()
                        if (selectSection == resources.getString(R.string.select_section)) {
                            selectSection = ""
                        } else {
                            selectSection = parent.getItemAtPosition(position).toString()

                            val da = DAO(activity!!)
                            da.open()
                            localSectionID = da.GetSectionID(selectSection!!)
                            da.close()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    Log.d("Student reg  ", "Spinner data")
                }
            }

        }


        btn_get_all_std_list?.setOnClickListener {
            try {

                if (selectSection == null || selectSection?.trim { it <= ' ' }!!.isEmpty() || selectSection == "Select Section") {

                    activity?.let { Toasty.error(it, "Select Section", Toast.LENGTH_SHORT, true).show() };

                } else if (!AppController.instance?.isNetworkAvailable()!!) {

                    AppController.instance?.noInternetConnection()
                } else {
                    activity?.title=null

                    val senrollmentFragment = FragmentStudentList()

                    val bundle = Bundle()
                    bundle.putString("section", selectSection)
                    senrollmentFragment.arguments = bundle

                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                    fragmentTransaction.commit()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null

                val allstdListFragment = FragmentStudentDashboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in attendances?.indices!!) {
                attendances.get(i).getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity!!, R.layout.spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }

    }

    companion object {
        private val TAG = "FragmentSAttendanceList"
        var localSectionID = ""
    }
}
