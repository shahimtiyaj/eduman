package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.shimmer.ShimmerFrameLayout
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.CustomSearchableSpinner
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import es.dmoral.toasty.Toasty
import org.json.JSONException
import java.util.*

class FragmentRemarkUpdateSelection : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_remarks_update_search: Button? = null
    private var spinner_section: CustomSearchableSpinner? = null
    private var spinner_exam: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    lateinit var v: View

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    private var txt_back: TextView? = null
    var viewDialog: ViewDialog? = null
    private var shimmerLayout: ShimmerFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.remark_update_selection_layout, container, false)
        initializeViews()
        shimmerLayout = v.findViewById(R.id.shimmer_layout_id)
        //activity?.title = null

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "remark_update_selection")
        editor.apply()

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        dao?.deleteExamList()
        dao?.close()

        setsectionSpinnerData()

        return v
    }

    override fun onStart() {
        super.onStart()
       // activity?.title="Remarks"
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        shimmerLayout?.stopShimmer()
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_remarks_update_search = v.findViewById<View>(R.id.btn_all_remarks_update_search) as Button

        spinner_section = v.findViewById<View>(R.id.spinner_remarks_update_section) as CustomSearchableSpinner

        spinner_section?.setTitle("Search Section")

        spinner_section?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false

                try {

                    val spinner = parent as CustomSearchableSpinner

                    if (spinner.id == R.id.spinner_remarks_update_section) {
                        selectSection = spinner_section?.selectedItem.toString()
                        if (selectSection == resources.getString(R.string.select_section)) {
                            selectSection = ""
                        } else {
                            selectSection = parent.getItemAtPosition(position).toString()
                            val da = DAO(activity!!)
                            da.open()
                            localSectionID = da.GetSectionID(selectSection!!)

                            loadServerExamData()

                            da.close()
                        }
                    }

                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    Log.d("Student reg  ", "Spinner data")
                }
            }

        }

        spinner_exam = v.findViewById<View>(R.id.spinner_remarks_update_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        btn_remarks_update_search?.setOnClickListener {
            try {

                if (selectSection == null || selectSection?.trim { it <= ' ' }!!.isEmpty() || selectSection == "Select Section") {

                    activity?.let { Toasty.error(it, "Select Section", Toast.LENGTH_SHORT, true).show() };

                } else if (selectExam == null || selectExam?.trim { it <= ' ' }!!.isEmpty() || selectExam == "Select Exam") {

                    activity?.let { Toasty.error(it, "Select Exam", Toast.LENGTH_SHORT, true).show() };

                } else if (!AppController.instance?.isNetworkAvailable()!!) {

                    AppController.instance?.noInternetConnection()
                } else {
                    activity?.title=null

                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.deleteStudentResultList()
                    dao?.close()

                    val allstdListFragment = FragmentRemarkUpdate()
                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                    fragmentTransaction.commit()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = FragmentSemesterExamDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_remarks_update_exam) {
                selectExam = spinner_exam?.selectedItem.toString()
                if (selectExam == resources.getString(R.string.select_exam)) {
                    selectExam = ""
                } else {
                    selectExam = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localExamID = da.GetExamConfigID(selectExam!!)
                    da.close()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark Update ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {

    }


    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val remarkUpdate = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in remarkUpdate?.indices!!) {
                remarkUpdate[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(
                activity,
                android.R.layout.simple_spinner_item,
                section
            )
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section!!.adapter = sectionAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: java.lang.NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark update ", "Section spinner data")
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {

            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter =
                ArrayAdapter(
                    Objects.requireNonNull<FragmentActivity>(activity),
                    android.R.layout.simple_spinner_item,
                    exam
                )
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam!!.adapter = examAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: java.lang.NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark update exam ", " spinner data")
        }
    }


    private fun loadServerExamData() {
        val hitURL = AppController.BaseUrl + "exam/configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID

        shimmerLayout?.visibility=View.VISIBLE
        shimmerLayout?.startShimmer()

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val examConfigId = c.getString("examConfigId")

                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(jobj.getString("id"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_EXAM + "(examConfigId, ExamId, ExamName) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(examConfigId.toString(), jobj.getString("id"), jobj.getString("name"))
                            )

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()

                        shimmerLayout?.visibility=View.GONE
                        shimmerLayout?.stopShimmer()
                    }
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()
                },
                Response.ErrorListener { error ->
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {
        private val TAG = "FragmentRemarkUpdateSelection"
        var localSectionID = ""
        var localExamID = ""
    }
}
