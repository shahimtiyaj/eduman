package com.netizen.eduman.JAVACode;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.netizen.eduman.R;
import com.netizen.eduman.app.AppController;
import com.netizen.eduman.model.Student;

import java.util.List;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.StudentViewHolder> {

    private Context mContext;
    //Store a member variable for the item
    private List<Student> studentList;

    /*
     Provide a direct reference to each of the views within a data item
     Used to cache the views within the item layout for fast access
     */
    public class StudentViewHolder extends RecyclerView.ViewHolder {
        /* holder should contain a member variable
         for any view that will be set as you render a row
        */
        public TextView st_id, st_roll, st_name, group, category, gender, religion, fname, mname, mobile;
        public ImageView image;

        /* We also create a constructor that accepts the entire item row
         and does the view lookups to find each subview
         */
        public StudentViewHolder(View view) {
            super(view);
            st_id = (TextView) view.findViewById(R.id.student_id);
            st_roll = (TextView) view.findViewById(R.id.student_roll);
            st_name = (TextView) view.findViewById(R.id.student_name);
            group = (TextView) view.findViewById(R.id.student_group);
            category = (TextView) view.findViewById(R.id.student_category);
            gender = (TextView) view.findViewById(R.id.student_gender);
            religion = (TextView) view.findViewById(R.id.student_religion);
            fname = (TextView) view.findViewById(R.id.student_father_name);
            mname = (TextView) view.findViewById(R.id.student_mother_name);
            mobile = (TextView) view.findViewById(R.id.student_mobile);

            image = (ImageView) view.findViewById(R.id.student_image_id);
        }
    }

    // Pass in the item array and context into the constructor
    public StudentListAdapter(Context mContext, List<Student> studentList) {
        this.mContext = mContext;
        this.studentList = studentList;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate the custom layout
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_list_item, parent, false);
        // Return a new holder instance
        return new StudentViewHolder(itemView);
    }


    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(final StudentViewHolder holder, int position) {
        // Get the item model based on position
        Student student = studentList.get(position);
        // Set item views based on our views and data model
        holder.st_id.setText(student.getStudent_id());
        holder.st_roll.setText(student.getStudent_roll());
        holder.st_name.setText(student.getStudent_name());
        holder.group.setText(student.getGroup());
        holder.category.setText(student.getCategory());
        holder.gender.setText(student.getGender());
        holder.religion.setText(student.getReligion());
        holder.fname.setText(student.getFather_name());
        holder.mname.setText(student.getMother_name());
        holder.mobile.setText(student.getMobile_no());


/*
        Glide.with(AppController.)
                .load("https://vancouver.housing.ubc.ca/wp-content/uploads/2014/04/icon_prospective_students.png")
                .into(holder.image);*/
/*
        Glide.with(AppController.getInstance())
                .load(student.getStudent_image())
                .into(holder.image);*/
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return studentList.size();
    }

}
