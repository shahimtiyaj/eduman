package com.netizen.eduman

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.AttendanceAdapter
import com.netizen.eduman.FragmentTakeAtdSelection.Companion.dateOfAttendance
import com.netizen.eduman.FragmentTakeAtdSelection.Companion.localPeriodID
import com.netizen.eduman.FragmentTakeAtdSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_TAKEATTENDANCE
import com.netizen.eduman.model.Attendance
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.jetbrains.annotations.Nullable
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*

class FragmentTakeAttendance : Fragment() {
    private var btn_attendance_save: Button? = null
    private var txt_back: TextView? = null
    internal var sectionsArrayList = ArrayList<Section>()
    private var v: View? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: AttendanceAdapter? = null
    private var attendancessArrayList: ArrayList<Attendance>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var total_found: TextView? = null
    private var jObjPost: String? = null
    private var section: String? = null
    private var section_txt: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences
    private var searchView: SearchView? = null

    var viewDialog: ViewDialog? = null

    internal var ok: TextView? = null
    internal var no: TextView? = null
    private var myDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(@Nullable inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.take_attendance_layout_list, container, false)

        initializeViews()

        recyclerViewInit()

        setHasOptionsMenu(true)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "std_take_atd")
        editor.apply()
        section_txt = v?.findViewById<View>(R.id.school_section) as TextView

        try {
            if (savedInstanceState == null) {
                if (arguments != null) {
                    section = arguments?.getString("section")
                    section_txt?.text = section
                }
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }

        if (!AppController.instance?.isNetworkAvailable()!!) {

            AppController.instance?.noInternetConnection()
        } else {

            try {
                getAllTakeAttendanceStudent()
            } catch (e: InvocationTargetException) {
                e.printStackTrace();
            } catch (e: Exception) {
                e.printStackTrace();
            }
        }

        return v
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
        activity?.title="Take Student Attendance"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        return if (id == R.id.action_search) {

            true
        } else super.onOptionsItemSelected(item)

    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #5459EC>" + resources.getString(R.string.search_student) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier("android:id/search_src_text", null, null)
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.colorEduman)

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_button", null, null)
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_close_btn", null, null)
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }
        myDialog = Dialog(requireActivity())

        sharedpreferences = activity?.getSharedPreferences(
            MyPREFERENCES, 0
        )!!
        accessToken = sharedpreferences.getString("accessToken", null)

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        total_found = v?.findViewById<View>(R.id.total_found) as TextView
        total_found?.text = dao?.totalAttendanceStudentCount().toString()
        dao?.close()

        btn_attendance_save = v?.findViewById<View>(R.id.btn_attendance_save) as Button

        btn_attendance_save?.setOnClickListener {
            try {
                attendancedataSendToServer()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null

                val allstdListFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*
   recycler view initialization method
   */
    fun recyclerViewInit() {
        // Initialize item list
        attendancessArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_attendance_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = AttendanceAdapter(activity!!, attendancessArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    fun Alert(ctx: Context, icon: Int, title: String, message: String) {
        AlertDialog.Builder(ctx)
            .setIcon(icon)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton("OK") { _, _ ->
                activity?.title=null
                val allstdListFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            }
            .setNegativeButton("Cancel") { _, _ ->
                activity?.title=null
                val allstdListFragment = FragmentTakeAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            }.show()

    }

    fun attendancedataSendToServer() {

        val hitURL = AppController.mainUrl + "manual/attendance/save/for/student?access_token=" + accessToken

        viewDialog?.showDialog()

        sendAttendanceList()

        val postRequest = object : CustomRequest(
            Request.Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val status = response.getString("message")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        viewDialog?.hideDialog()

                        showPopup()


                    } else {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.ic_error_black_24dp,
                                "Failure",
                                status
                            )
                        }
                        viewDialog?.hideDialog()

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()

                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun loadMessageList() {

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        attendancessArrayList = dao?.allTakeAttendance
        if (!attendancessArrayList?.isEmpty()!!) {
            adapter = AttendanceAdapter(context!!, attendancessArrayList!!)
            recyclerView?.setHasFixedSize(true)
            val mLayoutManager = LinearLayoutManager(context)
            recyclerView?.layoutManager = mLayoutManager
            recyclerView?.adapter = adapter
            adapter?.notifyDataSetChanged()
        }
        if (attendancessArrayList?.isEmpty()!!) {
            AppController.instance?.Alert(activity!!, R.drawable.ic_not_found, "Failure", "Sorry No Data Found !")
        }
        dao?.close()
    }

    private fun sendAttendanceList() {
        attendancessArrayList = ArrayList()

        val mainFormat = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())
        val parsedDate = mainFormat.parse(dateOfAttendance)
        val formatGet = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val dateForSendData = formatGet.format(parsedDate)


        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allTakeAttendanceChecked

            val jsonArray = JSONArray()
            val jsonObj = JSONObject()


            if (attendances != null) {
                for (attend in attendances) {
                    jsonArray.put(attend.getstudent_identification())
                }
            }

            try {

                jsonObj.put("identificationIds", jsonArray)
                jsonObj.put("attendanceDate", dateForSendData)//"01/01/2019" //dateOfAttendance
                jsonObj.put("classConfigId", localSectionID)
                jsonObj.put("periodId", localPeriodID) //"922102301"

            } catch (e: JSONException) {
                e.printStackTrace()
            }

            jObjPost = jsonObj.toString()
            Log.d("Json", jObjPost)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getAllTakeAttendanceStudent() {

        val mainFormat = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        val parsedDate = mainFormat.parse(dateOfAttendance)
        val formatGet = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val date = formatGet.format(parsedDate)

        val hitURL =
            AppController.mainUrl + "student/list/by/class-config-id?access_token=" + accessToken + "&attendanceDate=$date&classConfigId=$localSectionID&periodId=$localPeriodID"

        viewDialog?.showDialog()

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    val getData = response.getJSONArray("item")
                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        DAO.executeSQL(
                            ("INSERT OR REPLACE INTO " + TABLE_TAKEATTENDANCE + "(studentId, studentRoll, studentName, studentGender, identificationId) " +
                                    "VALUES(?, ?, ?, ?, ?)"),
                            arrayOf(
                                c.getString("customStudentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("studentGender"),
                                c.getString("identificationId")
                            )
                        )
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                adapter?.notifyDataSetChanged()
                loadMessageList()
                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                total_found?.text = dao?.totalAbssentStudent().toString()
                dao?.close()

                viewDialog?.hideDialog()


            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()


                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    fun showPopup() {
        val ok: Button

        val txt: TextView

        myDialog?.setContentView(R.layout.sucessfull_alert_layout)
        //val layone = myDialog?.findViewById(R.id.linear_item) as LinearLayout
        ok = myDialog?.findViewById(R.id.back) as Button

        txt = myDialog?.findViewById(R.id.tvItemSelected1)!!

        txt.setText("Student Attendance Successfully Saved !")

        myDialog?.setCancelable(false)

        ok.setOnClickListener {
            myDialog?.dismiss()

            val allstdListFragment = FragmentTakeAtdSelection()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
            fragmentTransaction.commit()
        }

        myDialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }

    companion object {
        private val TAG = "FragmentTakeAttendance"
    }
}
