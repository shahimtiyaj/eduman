package com.netizen.eduman

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.*
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.ResultSummaryAdapter
import com.netizen.eduman.FragmentStudentResultSelection.Companion.localExamID
import com.netizen.eduman.FragmentStudentResultSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.ResultSummary
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.lang.reflect.InvocationTargetException
import java.util.*

class FragmentStudentResult : Fragment() {
    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    lateinit var v: View
    private var recyclerView: RecyclerView? = null
    private var adapter: ResultSummaryAdapter? = null
    private var resultSummaryArrayList: ArrayList<ResultSummary>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var total_found: TextView? = null
    private var txt_back: TextView? = null

    private var section: String? = null
    private var section_txt: TextView? = null

    private var searchView: SearchView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    internal lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    private var swipeRefresh: SwipeRefreshLayout? = null
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.result_summary_recyler_list_view, container, false)
        initializeViews()
        recyclerViewInit()
        setHasOptionsMenu(true)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "student_result_details")
        editor.apply()

        section_txt = v.findViewById<View>(R.id.school_section) as TextView

        try {
            if (savedInstanceState == null) {
                if (arguments != null) {
                    section = arguments?.getString("section")
                    section_txt?.text = section
                }
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace();
        } catch (e: Exception) {
            e.printStackTrace();
        }

        if (!AppController.instance?.isNetworkAvailable()!!) {
            AppController.instance?.noInternetConnection()
        } else {
            GetStudentResultSummary()
        }

        return v
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        return if (id == R.id.action_search) {

            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onResume() {
        super.onResume()
        activity?.title="Report"
    }


    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #5459EC>" + resources.getString(R.string.search_student) + "</font>")

        val id = searchView?.getContext()?.resources?.getIdentifier("android:id/search_src_text", null, null)
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.colorEduman)

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_button", null, null)
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_close_btn", null, null)
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView?.maxWidth = Integer.MAX_VALUE

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

    }

    override fun onStart() {
        super.onStart()
    }



    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }

        mHandler = Handler()
        swipeRefresh = v.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout

        swipeRefresh?.setOnRefreshListener {
            mRunnable = Runnable {
                adapter?.notifyDataSetChanged()
                resultSummaryArrayList
                swipeRefresh?.isRefreshing = false
            }

            mHandler.postDelayed(
                mRunnable, 50
            )
        }

        total_found = v.findViewById<View>(R.id.total_found) as TextView

        sharedpreferences = activity!!.getSharedPreferences(
            MyPREFERENCES, 0
        )
        accessToken = sharedpreferences.getString("accessToken", null)

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = FragmentStudentResultSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*
   recycler view initialization method
   */
    fun recyclerViewInit() {
        // Initialize item list
        resultSummaryArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v.findViewById<View>(R.id.all_result_summary_recyler_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = ResultSummaryAdapter(activity!!, resultSummaryArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    fun Alert(ctx: Context, icon: Int, title: String, message: String) {
        AlertDialog.Builder(ctx)
            .setIcon(icon)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK") { _, _ ->
                activity?.title=null
                val allstdListFragment = FragmentStudentResultSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            }
            .setNegativeButton("Cancel") { _, _ ->
                activity?.title=null
                val allstdListFragment = FragmentStudentResultSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            }.show()
    }

    fun GetStudentResultSummary() {
        val hitURL = AppController.BaseUrl + "exam/report/section-wise/result/details?access_token=" + accessToken + "&classConfigId=" + localSectionID + "&examConfigId=" + localExamID

        viewDialog?.showDialog()

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        val resultSummary = ResultSummary()
                        resultSummary.result_std_id = c.getString("customStudentId")
                        resultSummary.result_std_roll = c.getString("studentRoll")
                        resultSummary.result_std_name = c.getString("studentName")
                        resultSummary.result_std_total_mark = c.getString("totalMarks")
                        resultSummary.result_std_grade = c.getString("letterGrade")
                        resultSummary.result_std_gpa = c.getString("gradingPoint")
                        resultSummary.result_std_status = c.getString("passFailStatus")
                        resultSummary.result_std_sec_merit = c.getString("sectionPosition")
                        resultSummary.result_std_no_failed = c.getString("numOfFailedSubjects")

                        // adding item to ITEM list
                        resultSummaryArrayList?.add(resultSummary)

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                if (resultSummaryArrayList?.isEmpty()!!) {
                    Alert(activity!!, R.drawable.ic_not_found, "Failure", "Sorry No Data Found !")
                }

                adapter?.notifyDataSetChanged()
                total_found?.setText(resultSummaryArrayList?.size.toString())
                viewDialog?.hideDialog()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)

                viewDialog?.hideDialog()

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {

        private val TAG = "FragmentStudentResult"
    }

}
