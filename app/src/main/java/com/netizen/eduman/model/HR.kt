package com.netizen.eduman.model

class HR {
    private var hr_id: String? = null
    private var hr_name: String? = null
    private var hr_gender: String? = null
    private var hr_religion: String? = null
    private var hr_category: String? = null
    private var hr_designation: String? = null
    private var hr_blood_grp: String? = null
    private var hr_phone: String? = null
    private var hr_image: String? = null

    fun getHr_id(): String? {
        return hr_id
    }

    fun setHr_id(hr_id: String) {
        this.hr_id = hr_id
    }

    fun getHr_name(): String? {
        return hr_name
    }

    fun setHr_name(hr_name: String) {
        this.hr_name = hr_name
    }

    fun getHr_gender(): String? {
        return hr_gender
    }

    fun setHr_gender(hr_gender: String) {
        this.hr_gender = hr_gender
    }

    fun getHr_religion(): String? {
        return hr_religion
    }

    fun setHr_religion(hr_religion: String) {
        this.hr_religion = hr_religion
    }

    fun getHr_category(): String? {
        return hr_category
    }

    fun setHr_category(hr_category: String) {
        this.hr_category = hr_category
    }

    fun getHr_designation(): String? {
        return hr_designation
    }

    fun setHr_designation(hr_designation: String) {
        this.hr_designation = hr_designation
    }

    fun getHr_blood_grp(): String? {
        return hr_blood_grp
    }

    fun setHr_blood_grp(hr_blood_grp: String) {
        this.hr_blood_grp = hr_blood_grp
    }

    fun getHr_phone(): String? {
        return hr_phone
    }

    fun setHr_phone(hr_phone: String) {
        this.hr_phone = hr_phone
    }

    fun getHr_image(): String? {
        return hr_image
    }

    fun setHr_image(hr_image: String) {
        this.hr_image = hr_image
    }
}