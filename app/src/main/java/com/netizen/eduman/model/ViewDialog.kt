package com.netizen.eduman.model

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController

class ViewDialog(internal var activity: Activity) {

    internal lateinit var dialog: Dialog

    fun showDialog() {

        dialog = Dialog(activity)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.setCancelable(false)

        dialog.setContentView(R.layout.loader_layout)

        val gifImageView = dialog.findViewById<ImageView>(R.id.custom_loading_imageView)

        val imageViewTarget = GlideDrawableImageViewTarget(gifImageView)

        Glide.with(AppController.context)
            .load(R.raw.loader_icon_n)
            .centerCrop()
            .crossFade()
            .into(imageViewTarget)

        /*Glide.with(activity)
        .load(R.drawable.loader_icon_n)
        .placeholder(R.drawable.loader_icon_n)
        .centerCrop()
        .crossFade()
        .into(imageViewTarget)*/

        dialog.show()

    }

    fun hideDialog() {
        dialog.dismiss()
    }

}