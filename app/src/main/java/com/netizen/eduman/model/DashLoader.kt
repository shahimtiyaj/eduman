package com.netizen.eduman.model

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget
import com.netizen.eduman.R


class DashLoader(internal var activity: Activity) {
    internal lateinit var dialog: Dialog

    fun showDialog() {

        dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.setCancelable(false)
        // dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.GREEN));


        dialog.setContentView(R.layout.dash_loader_layout)

       // dialog.window?.setBackgroundDrawableResource(R.color.colorEduman)


        val gifImageView = dialog.findViewById<ImageView>(R.id.custom_loading_imageView)


        val imageViewTarget = GlideDrawableImageViewTarget(gifImageView)

        Glide.with(activity)
            .load(R.drawable.load)
            .placeholder(R.drawable.load)
            .centerCrop()
            .crossFade()
            .into(imageViewTarget)


        dialog.show()
    }


    fun hideDialog() {
        dialog.dismiss()
    }

}