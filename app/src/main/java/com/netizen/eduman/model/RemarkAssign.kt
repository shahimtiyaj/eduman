package com.netizen.eduman.model

class RemarkAssign {
    private var remarksId: String? = null
    private var remarksName: String? = null

    fun getRemarksId(): String? {
        return remarksId
    }

    fun setRemarksId(remarksId: String) {
        this.remarksId = remarksId
    }

    fun getRemarksName(): String? {
        return remarksName
    }

    fun setRemarksName(remarksName: String) {
        this.remarksName = remarksName
    }
}