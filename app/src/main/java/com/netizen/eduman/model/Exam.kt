package com.netizen.eduman.model

class Exam {
    private var ExamId: String? = null
    private var ExamName: String? = null

    fun getExamId(): String? {
        return ExamId
    }

    fun setExamId(examId: String) {
        ExamId = examId
    }

    fun getExamName(): String? {
        return ExamName
    }

    fun setExamName(examName: String) {
        ExamName = examName
    }

}