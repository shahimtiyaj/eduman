package com.netizen.eduman.model

class Designation {
    private var designationId: String? = null
    private var designationName: String? = null


    fun getDesignationId(): String? {
        return designationId
    }

    fun setDesignationId(designationId: String) {
        this.designationId = designationId
    }

    fun getDesignationName(): String? {
        return designationName
    }

    fun setDesignationName(designationName: String) {
        this.designationName = designationName
    }
}