package com.netizen.eduman.model

class MarkDistribution {
    var exameCode: String? = null
    var totalMark: String? = null
    var passMark: String? = null
    var acceptance: String? = null
    var defaultId: String? = null

    constructor() {

    }

    constructor(exameCode: String, totalMark: String, passMark: String, acceptance: String, defaultId: String) {
        this.exameCode = exameCode
        this.totalMark = totalMark
        this.passMark = passMark
        this.acceptance = acceptance
        this.defaultId = defaultId
    }

    fun exameCode(): String? {
        return exameCode
    }

    fun exameCode(exameCode: String) {
        this.exameCode = exameCode
    }

    fun totalMark(): String? {
        return totalMark
    }

    fun totalMark(totalMark: String) {
        this.totalMark = totalMark
    }

    fun passMark(): String? {
        return passMark
    }

    fun passMark(passMark: String) {
        this.passMark = passMark
    }

    fun acceptance(): String? {
        return acceptance
    }

    fun acceptance(acceptance: String) {
        this.acceptance = acceptance
    }

    fun defaultId(): String? {
        return defaultId
    }

    fun defaultId(defaultId: String) {
        this.defaultId = defaultId
    }

}
