package com.netizen.eduman.model

class HRPresent {
    private var hr_p_id: String? = null
    private var hr_p_name: String? = null
    private var hr_p_category: String? = null
    private var hr_p_designation: String? = null
    private var hr_p_mobile: String? = null
    private var hr_p_in_time: String? = null
    private var hr_p_in_time_status: String? = null
    private var hr_p_out_time: String? = null
    private var hr_p_out_time_status: String? = null

    fun getHr_p_id(): String? {
        return hr_p_id
    }

    fun setHr_p_id(hr_p_id: String) {
        this.hr_p_id = hr_p_id
    }

    fun getHr_p_name(): String? {
        return hr_p_name
    }

    fun setHr_p_name(hr_p_name: String) {
        this.hr_p_name = hr_p_name
    }

    fun getHr_p_category(): String? {
        return hr_p_category
    }

    fun setHr_p_category(hr_p_category: String) {
        this.hr_p_category = hr_p_category
    }

    fun getHr_p_designation(): String? {
        return hr_p_designation
    }

    fun setHr_p_designation(hr_p_designation: String) {
        this.hr_p_designation = hr_p_designation
    }

    fun getHr_p_mobile(): String? {
        return hr_p_mobile
    }

    fun setHr_p_mobile(hr_p_mobile: String) {
        this.hr_p_mobile = hr_p_mobile
    }

    fun getHr_p_in_time(): String? {
        return hr_p_in_time
    }

    fun setHr_p_in_time(hr_p_in_time: String) {
        this.hr_p_in_time = hr_p_in_time
    }

    fun getHr_p_in_time_status(): String? {
        return hr_p_in_time_status
    }

    fun setHr_p_in_time_status(hr_p_in_time_status: String) {
        this.hr_p_in_time_status = hr_p_in_time_status
    }

    fun getHr_p_out_time(): String? {
        return hr_p_out_time
    }

    fun setHr_p_out_time(hr_p_out_time: String) {
        this.hr_p_out_time = hr_p_out_time
    }

    fun getHr_p_out_time_status(): String? {
        return hr_p_out_time_status
    }

    fun setHr_p_out_time_status(hr_p_out_time_status: String) {
        this.hr_p_out_time_status = hr_p_out_time_status
    }
}