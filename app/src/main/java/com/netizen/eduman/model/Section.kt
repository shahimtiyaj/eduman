package com.netizen.eduman.model

class Section{

    private var sectionId: String? = null
    private var sectionName: String? = null

    constructor() {}

    constructor(sectionName: String) {
        this.sectionName = sectionName
    }

    fun getSectionId(): String? {
        return sectionId
    }

    fun setSectionId(sectionId: String) {
        this.sectionId = sectionId
    }

    fun getSectionName(): String? {
        return sectionName
    }

    fun setSectionName(sectionName: String) {
        this.sectionName = sectionName
    }
}