package com.netizen.eduman.model

class UserRole() {
    private var account: String? = null
    private var admin: String? = null
    private var operator: String? = null
    private var teacher: String? = null

    constructor(account: String, admin: String, operator: String, teacher: String) : this() {
        this.account = account
        this.admin = admin
        this.operator = operator
        this.teacher = teacher
    }

    fun getaccount(): String? {
        return account
    }

    fun setaccount(account: String) {
        this.account = account
    }

    fun getadmin(): String? {
        return admin
    }

    fun setadmin(admin: String) {
        this.admin = admin
    }

    fun getoperator(): String? {
        return operator
    }

    fun setoperator(operator: String) {
        this.operator = operator
    }

    fun getteacher(): String? {
        return teacher
    }

    fun setteacher(teacher: String) {
        this.teacher = teacher
    }
}