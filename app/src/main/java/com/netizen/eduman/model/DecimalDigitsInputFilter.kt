package com.netizen.eduman.model

import android.text.Spanned
import android.text.InputFilter
import java.util.regex.Pattern


class DecimalDigitsInputFilter : InputFilter {

    internal var mPattern: Pattern

    init {
        mPattern = Pattern.compile("[0-9]*+((\\.[0-9]?)?)||(\\.)?")
    }

    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val matcher = mPattern.matcher(dest)
        return if (!matcher.matches()) "" else null
    }
}