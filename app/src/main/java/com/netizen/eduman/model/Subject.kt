package com.netizen.eduman.model

class Subject {
    private var subjectId: String? = null
    private var subjectName: String? = null

    fun getSubjectId(): String? {
        return subjectId
    }

    fun setSubjectId(subjectId: String) {
        this.subjectId = subjectId
    }

    fun getSubjectName(): String? {
        return subjectName
    }

    fun setSubjectName(subjectName: String) {
        this.subjectName = subjectName
    }
}