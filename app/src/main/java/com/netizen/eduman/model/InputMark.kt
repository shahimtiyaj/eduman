package com.netizen.eduman.model

class InputMark {

    var in_mark_st_identification_id: String? = null
    var markinputId: String? = null

    var total_marks: String? = null
    var grade: String? = null


    var in_mark_st_id: String? = null
    var in_mark_st_roll: String? = null
    var in_mark_st_name: String? = null
    var in_mark_f_exam_code: String? = null
    var in_mark_s_exam_code: String? = null

    var shortCode1: String? = null
    var shortCode2: String? = null
    var shortCode3: String? = null
    var shortCode4: String? = null
    var shortCode5: String? = null
    var shortCode6: String? = null

    constructor() {

    }

    constructor(
        markinputId: String, in_mark_st_identification_id: String, in_mark_st_id: String, in_mark_st_roll: String,
        in_mark_st_name: String, shortCode1: String, shortCode2: String, shortCode3: String, shortCode4: String, shortCode5: String, shortCode6: String
    ) {
        this.markinputId = markinputId
        this.in_mark_st_identification_id = in_mark_st_identification_id
        this.in_mark_st_id = in_mark_st_id
        this.in_mark_st_roll = in_mark_st_roll
        this.in_mark_st_name = in_mark_st_name
        this.shortCode1 = shortCode1
        this.shortCode2 = shortCode2
        this.shortCode3 = shortCode3
        this.shortCode4 = shortCode4
        this.shortCode5 = shortCode5
        this.shortCode6 = shortCode6
    }


    fun markinputId(): String? {
        return markinputId
    }

    fun markinputId(markinputId: String) {
        this.markinputId = markinputId
    }



    fun in_mark_st_identification_id(): String? {
        return in_mark_st_identification_id
    }

    fun in_mark_st_identification_id(in_mark_st_identification_id: String) {
        this.in_mark_st_identification_id = in_mark_st_identification_id
    }


    fun shortCode1(): String? {
        return shortCode1
    }

    fun shortCode1(shortCode1: String) {
        this.shortCode1 = shortCode1
    }

    fun shortCode2(): String? {
        return shortCode2
    }

    fun shortCode2(shortCode2: String) {
        this.shortCode2 = shortCode2
    }

    fun shortCode3(): String? {
        return shortCode3
    }

    fun shortCode3(shortCode3: String) {
        this.shortCode3 = shortCode3
    }

    fun shortCode4(): String? {
        return shortCode4
    }

    fun shortCode4(shortCode4: String) {
        this.shortCode4 = shortCode4
    }

    fun shortCode5(): String? {
        return shortCode5
    }

    fun shortCode5(shortCode5: String) {
        this.shortCode5 = shortCode5
    }

    fun shortCode6(): String? {
        return shortCode6
    }

    fun shortCode6(shortCode6: String) {
        this.shortCode6 = shortCode6
    }

    fun in_mark_f_exam_code(): String? {
        return in_mark_f_exam_code
    }

    fun in_mark_f_exam_code(in_mark_f_exam_code: String) {
        this.in_mark_f_exam_code = in_mark_f_exam_code
    }

    fun in_mark_s_exam_code(): String? {
        return in_mark_s_exam_code
    }

    fun in_mark_s_exam_code(in_mark_s_exam_code: String) {
        this.in_mark_s_exam_code = in_mark_s_exam_code
    }


    fun in_mark_st_id(): String? {
        return in_mark_st_id
    }

    fun in_mark_st_id(in_mark_st_id: String) {
        this.in_mark_st_id = in_mark_st_id
    }

    fun in_mark_st_roll(): String? {
        return in_mark_st_roll
    }

    fun in_mark_st_roll(in_mark_st_roll: String) {
        this.in_mark_st_roll = in_mark_st_roll
    }

    fun in_mark_st_name(): String? {
        return in_mark_st_name
    }

    fun in_mark_st_name(in_mark_st_name: String) {
        this.in_mark_st_name = in_mark_st_name
    }

    fun total_marks(): String? {
        return total_marks
    }

    fun total_marks(total_marks: String) {
        this.total_marks = total_marks
    }


    fun grade(): String? {
        return grade
    }

    fun grade(total_marks: String) {
        this.grade = grade
    }

}
