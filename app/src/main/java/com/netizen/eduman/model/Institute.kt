package com.netizen.eduman.model

class Institute() {
    private var instituteId: String? = null
    private var instituteName: String? = null
    private var instituteAddress: String? = null
    private var academic_year: String? = null
    private var instituteLogo: String? = null

    constructor(instituteId: String, instituteName: String, instituteAddress: String) : this() {
        this.instituteId = instituteId
        this.instituteName = instituteName
        this.instituteAddress = instituteAddress
    }

    fun getinstituteId(): String? {
        return instituteId
    }

    fun setuinstituteId(instituteId: String) {
        this.instituteId = instituteId
    }

    fun getuserinstituteName(): String? {
        return instituteName
    }

    fun setinstituteName(instituteName: String) {
        this.instituteName = instituteName
    }

    fun getinstituteAddress(): String? {
        return instituteAddress
    }

    fun setinstituteAddress(instituteAddress: String) {
        this.instituteAddress = instituteAddress
    }

    fun getacademic_year(): String? {
        return academic_year
    }

    fun setacademic_year(academic_year: String) {
        this.academic_year = academic_year
    }

    fun getinstituteLogo(): String? {
        return instituteLogo
    }

    fun setinstituteLogo(instituteLogo: String) {
        this.instituteLogo = instituteLogo
    }
}