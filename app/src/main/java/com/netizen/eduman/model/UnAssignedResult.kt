package com.netizen.eduman.model


class UnAssignedResult {

    var sectionName: String? = null
    var numOfTotalSubjects: String? = null
    var numOfMarkInputtedSubjects: String? = null
    var numOfMarkUnInputtedSubjects: String? = null
    var markUnInputtedSubjects: String? = null

    //default
    constructor() {

    }

    constructor(
        sectionName: String?,
        numOfTotalSubjects: String?,
        numOfMarkInputtedSubjects: String?,
        numOfMarkUnInputtedSubjects: String?,
        markUnInputtedSubjects: String?
    ) {
        this.sectionName = sectionName
        this.numOfTotalSubjects = numOfTotalSubjects
        this.numOfMarkInputtedSubjects = numOfMarkInputtedSubjects
        this.numOfMarkUnInputtedSubjects = numOfMarkUnInputtedSubjects
        this.markUnInputtedSubjects = markUnInputtedSubjects
    }

}
