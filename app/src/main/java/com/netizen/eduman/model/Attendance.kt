package com.netizen.eduman.model


class Attendance {
    var student_id: String? = null
    var student_roll_no: String? = null
    var student_name: String? = null
    var student_gender: String? = null
    var isStudent_absent: Int = 0

    private var isSelected: Boolean = false


    var student_identification: String? = null

    constructor() {}

    constructor(student_identification: String, student_absent: Int) {
        this.student_identification = student_identification
        this.isStudent_absent = student_absent
    }

    constructor(
        student_id: String,
        student_roll_no: String,
        student_name: String,
        student_gender: String,
        student_identification: String,
        student_absent: Int
    ) {
        this.student_id = student_id
        this.student_roll_no = student_roll_no
        this.student_name = student_name
        this.student_gender = student_gender
        this.student_identification = student_identification
        this.isStudent_absent = student_absent
    }


    fun student_id(): String? {
        return student_id
    }

    fun student_id(student_id: String) {
        this.student_id = student_id
    }

    fun student_roll_no(): String? {
        return student_roll_no
    }

    fun student_roll_no(student_roll_no: String) {
        this.student_roll_no = student_roll_no
    }

    fun student_name(): String? {
        return student_name
    }

    fun student_name(student_name: String) {
        this.student_name = student_name
    }

    fun student_gender(): String?{
        return student_gender
    }

    fun student_gender(student_gender: String) {
        this.student_gender = student_gender
    }

    fun getStudent_absent(): Int ?{
        return isStudent_absent
    }

    fun setstudent_absent(student_absent: Int) {
        this.isStudent_absent = student_absent
    }

    fun getstudent_identification(): String ?{
        return student_identification
    }

    fun student_identification(student_identification: String) {
        this.student_identification = student_identification
    }

    fun getSelected(): Boolean {
        return isSelected
    }

    fun setSelected(selected: Boolean) {
        isSelected = selected
    }
}
