package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.shimmer.ShimmerFrameLayout
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper
import com.netizen.eduman.model.*
import com.netizen.eduman.network.CloudRequest
import es.dmoral.toasty.Toasty
import org.json.JSONException
import java.util.*

class FragmentRemarkAssignSelection : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    private var btn_remarks_search: Button? = null
    private var spinner_section: CustomSearchableSpinner? = null
    private var spinner_exam: Spinner? = null
    private var spinner_remarks: Spinner? = null

    private var selectSection: String? = null
    private var selectExam: String? = null
    private var selectRemarks: String? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()
    internal var remarkAssignsArrayList = ArrayList<RemarkAssign>()

    lateinit var v: View
    private var txt_back: TextView? = null

    private var inputRemarksDescription: EditText? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var dataload = false

    var viewDialog: ViewDialog? = null

    private var shimmerLayout: ShimmerFrameLayout? = null
    private var shimmerLayout1: ShimmerFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.remark_assign_selection_layout, container, false)

        initializeViews()

        shimmerLayout = v.findViewById(R.id.shimmer_layout_id)
        shimmerLayout1 = v.findViewById(R.id.shimmer_layout_id1)

       // activity?.title = null

        val sharedPreferences = activity?.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putString("now", "remark_assign_selection")
        editor?.apply()

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        dao?.deleteExamList()
        dao?.close()

        setsectionSpinnerData()

        return v
    }

    override fun onStart() {
        super.onStart()
        activity?.title="Remarks"
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        shimmerLayout?.stopShimmer()
        shimmerLayout1?.stopShimmer()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && !dataload) {

            if (!AppController.instance?.isNetworkAvailable()!!) {
                AppController.instance?.noInternetConnection()
            } else {
            }

            dataload = true;
        }
    }

    private fun initializeViews() {
        viewDialog = activity?.let { ViewDialog(it) }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_remarks_search = v.findViewById<View>(R.id.btn_all_remarks_search) as Button

        inputRemarksDescription = v.findViewById<View>(R.id.et_student_remarks_description) as EditText

        spinner_section = v.findViewById<View>(R.id.spinner_remarks_section) as CustomSearchableSpinner

        spinner_section?.setTitle("Search Section")

        spinner_section?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false

                try {

                    val spinner = parent as CustomSearchableSpinner

                    if (spinner.id == R.id.spinner_remarks_section) {
                        selectSection = spinner_section?.selectedItem.toString()
                        if (selectSection == resources.getString(R.string.select_section_input_mark)) {
                            selectSection = ""
                        } else {
                            selectSection = parent.getItemAtPosition(position).toString()

                            val da = DAO(activity!!)

                            da.open()

                            localSectionID = da.GetSectionID(selectSection!!)

                            loadServerExamData()

                            da.close()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    Log.d("Student reg  ", "Spinner data")
                }
            }

        }

        spinner_exam = v.findViewById<View>(R.id.spinner_remarks_exam) as Spinner
        spinner_exam?.onItemSelectedListener = this

        spinner_remarks = v.findViewById<View>(R.id.spinner_remarks_title) as Spinner
        spinner_remarks?.onItemSelectedListener = this

        btn_remarks_search?.setOnClickListener {
            try {

                if (selectSection == null || selectSection?.trim { it <= ' ' }!!.isEmpty() || selectSection == "Select Section") {

                    activity?.let { Toasty.error(it, "Select Section", Toast.LENGTH_SHORT, true).show() };

                } else if (selectExam == null || selectExam?.trim { it <= ' ' }!!.isEmpty() || selectExam == "Select Exam") {

                    activity?.let { Toasty.error(it, "Select Exam", Toast.LENGTH_SHORT, true).show() };

                } else if (selectRemarks == null || selectRemarks?.trim { it <= ' ' }!!.isEmpty() || selectRemarks == "Select Remarks Title") {

                    activity?.let { Toasty.error(it, "Select Remarks Title", Toast.LENGTH_SHORT, true).show() };

                } else if (!AppController.instance?.isNetworkAvailable()!!) {

                    AppController.instance?.noInternetConnection()
                } else {
                    activity?.title=null

                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.deleteStudentResultList()
                    dao?.close()

                    val allstdListFragment = FragmentRemarksAssign()
                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                    fragmentTransaction.commit()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentSemesterExamDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_remarks_exam) {
                selectExam = spinner_exam?.selectedItem.toString()
                if (selectExam == resources.getString(R.string.select_exam)) {
                    selectExam = ""
                } else {
                    selectExam = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)

                    da.open()

                    localExamID = da.GetExamConfigID(selectExam!!)

                    loadServerRemarksData()

                    da.close()
                }
            }

            if (spinner.id == R.id.spinner_remarks_title) {
                selectRemarks = spinner_remarks?.selectedItem.toString()
                if (selectRemarks == resources.getString(R.string.select_remarks)) {
                    selectRemarks = ""
                } else {
                    selectRemarks = parent.getItemAtPosition(position).toString()

                    val da = DAO(activity!!)
                    da.open()
                    localRemarkID = da.GetRemarkID(selectRemarks!!)

                    loadServerRemarkDescriptionData()

                    da.close()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark Assign ", "Spinner data")
        }


    }


    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun onClick(v: View) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setRemarksTitleSpinnerData(remarkAssignsArrayList: ArrayList<RemarkAssign>) {
        try {
            val remarks = ArrayList<String>()
            remarks.add(resources.getString(R.string.select_remarks))
            for (i in remarkAssignsArrayList.indices) {
                remarkAssignsArrayList[i].getRemarksName()?.let { remarks.add(it) }
            }

            val remarksAdapter = ArrayAdapter(
                activity, android.R.layout.simple_spinner_item, remarks
            )
            remarksAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_remarks?.adapter = remarksAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark title ", " spinner data")
        }
    }

    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val remarkAssign = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))

            for (i in remarkAssign?.indices!!) {
                remarkAssign[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark section ", " spinner data")
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun setExamSpinnerData(examArrayList: ArrayList<Exam>) {
        try {
            val exam = ArrayList<String>()
            exam.add(resources.getString(R.string.select_exam))
            for (i in examArrayList.indices) {
                examArrayList[i].getExamName()?.let { exam.add(it) }
            }

            val examAdapter =
                ArrayAdapter(
                    Objects.requireNonNull<FragmentActivity>(activity),
                    android.R.layout.simple_spinner_item,
                    exam
                )
            examAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_exam?.adapter = examAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Remark Exam ", " Spinner data")
        }
    }

    private fun loadServerRemarksData() {
        val hitURL = AppController.BaseUrl + "exam/remarks/template/find/all/list?access_token=" + accessToken

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        remarkAssignsArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val remarkAssign = RemarkAssign()
                            remarkAssign.setRemarksId(c.getString("remarkTempId"))
                            remarkAssign.setRemarksName(c.getString("remarkTitle"))

                            remarkAssignsArrayList.add(remarkAssign)

                            DAO.executeSQL(
                                ("INSERT OR REPLACE INTO " + DBHelper.TABLE_ST_REMARKS_TITLE + "(remarkTempId, remarkTitle) " +
                                        "VALUES(?, ?)"),
                                arrayOf(c.getString("remarkTempId"), c.getString("remarkTitle"))
                            )

                            if (!remarkAssignsArrayList.isEmpty()) {
                                setRemarksTitleSpinnerData(remarkAssignsArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("Remarks data: ", e.toString())
                    }
                },
                Response.ErrorListener { error ->

                    Log.d("Remarks data: ", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun loadServerRemarkDescriptionData() {
        val hitURL =
            AppController.BaseUrl + "exam/remarks/template/find/by-remarkId?access_token=" + accessToken + "&remarkTempId=" + localRemarkID

        shimmerLayout1?.visibility=View.VISIBLE
        shimmerLayout1?.startShimmer()

        val jsonObjectRequest = object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {

                        remarksDescription = response.getString("item")
                        inputRemarksDescription!!.setText(remarksDescription)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        shimmerLayout1?.visibility=View.GONE
                        shimmerLayout1?.stopShimmer()
                    }

                    shimmerLayout1?.visibility=View.GONE
                    shimmerLayout1?.stopShimmer()
                },
                Response.ErrorListener { error ->

                    shimmerLayout1?.visibility=View.GONE
                    shimmerLayout1?.stopShimmer()

                    Log.d("Remarks des", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }

    }

    private fun loadServerExamData() {
        val hitURL = AppController.BaseUrl + "exam/configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID

        shimmerLayout?.visibility=View.VISIBLE
        shimmerLayout?.startShimmer()

        val jsonObjectRequest = @RequiresApi(Build.VERSION_CODES.KITKAT)
        object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        examArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val examConfigId = c.getString("examConfigId")

                            val jobj = c.getJSONObject("examObject")
                            val examList = Exam()
                            examList.setExamId(jobj.getString("id"))
                            examList.setExamName(jobj.getString("name"))

                            examArrayList.add(examList)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + DBHelper.TABLE_EXAM + "(examConfigId, ExamId, ExamName) " +
                                        "VALUES(?, ?, ?)",
                                arrayOf(examConfigId.toString(), jobj.getString("id"), jobj.getString("name"))
                            )

                            if (!examArrayList.isEmpty()) {
                                setExamSpinnerData(examArrayList)
                            }

                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()

                        shimmerLayout?.visibility=View.GONE
                        shimmerLayout?.stopShimmer()
                    }

                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                },
                Response.ErrorListener { error ->

                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                    Log.d("Remarks Exam", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }

        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        //Volley cache true
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    companion object {
        var localSectionID = ""
        var localExamID = ""
        var localRemarkID = ""
        var remarksDescription = ""
    }
}
