package com.netizen.eduman

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.*
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.HRListAdapter
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.HR
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.util.*

class FragmentHRList : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var adapter: HRListAdapter? = null
    private var hrsArrayList: ArrayList<HR>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var txt_back: TextView? = null
    private var total_found: TextView? = null
    private var v: View? = null
    private var searchView: SearchView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null
    private var swipeRefresh: SwipeRefreshLayout? = null
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_recyler_listview, container, false)

        initializeViews()
        recyclerViewInit()
        setHasOptionsMenu(true)

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "hr_list")
        editor.apply()

        if (!AppController.instance?.isNetworkAvailable()!!) {
            AppController.instance?.noInternetConnection()
        } else {
            GetAllHRDataWebService()
        }

        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Report"
    }


    private fun initializeViews() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        viewDialog = activity?.let { ViewDialog(it) }
      mHandler = Handler()
     swipeRefresh = v?.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
         swipeRefresh?.setOnRefreshListener {
             mRunnable = Runnable {
                 adapter?.notifyDataSetChanged()
                 hrsArrayList
                 swipeRefresh?.isRefreshing = false
             }

             mHandler.postDelayed(
                 mRunnable, 50
             )
         }


        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)
        txt_back = v?.findViewById<View>(R.id.back_text) as TextView
        total_found = v?.findViewById<View>(R.id.total_found) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = FragmentHRDashboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        return if (id == R.id.action_search) {

            true
        } else super.onOptionsItemSelected(item)

    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

        // Associate searchable configuration with the SearchView
        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #5459EC>" + resources.getString(R.string.search_hr) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier("android:id/search_src_text", null, null)
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.colorEduman)

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_button", null, null)
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)

        val closeId = searchView?.getContext()?.resources?.getIdentifier("android:id/search_close_btn", null, null)
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

    }

    fun recyclerViewInit() {

        // Initialize item list
        hrsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_hr_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = HRListAdapter(activity!!, hrsArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter

    }

    fun GetAllHRDataWebService() {
        val hitURL = AppController.BaseUrl + "staff/basic/list/with/photo?&access_token=" + accessToken

        viewDialog?.showDialog()

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val hrlist = HR()
                        //set the json data in the model
                        hrlist.setHr_id(c.getString("customStaffId"))
                        hrlist.setHr_name(c.getString("staffName"))
                        hrlist.setHr_gender(c.getString("gender"))
                        hrlist.setHr_religion(c.getString("staffReligion"))
                        hrlist.setHr_category(c.getString("staffCategory"))
                        hrlist.setHr_designation(c.getString("designationName"))
                        hrlist.setHr_blood_grp(c.getString("bloodGroup"))
                        hrlist.setHr_phone(c.getString("staffMobile1"))
                        hrlist.setHr_image(c.getString("image"))

                        // adding item to ITEM list
                        hrsArrayList?.add(hrlist)
                    }


                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                if (hrsArrayList?.isEmpty()!!) {
                    AppController.instance?.Alert(
                        activity!!,
                        R.drawable.ic_not_found,
                        "Failure",
                        "Sorry No Data Found !"
                    )
                }

                //Notify adapter if data change
                adapter?.notifyDataSetChanged()
                total_found?.text = hrsArrayList?.size.toString()
                viewDialog?.hideDialog()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                //headers.put("access_token", "7e1bb8bc-c8ef-4821-8811-97a1eef7135c");

                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }

    }

    companion object {

        private val TAG = "FragmentHRList"
    }

}

