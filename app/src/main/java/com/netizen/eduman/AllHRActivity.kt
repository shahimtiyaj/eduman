package com.netizen.eduman

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.HRListAdapter
import com.netizen.eduman.model.HR
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.util.ArrayList
import java.util.HashMap


class AllHRActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var adapter: HRListAdapter? = null
    private var hrsArrayList: ArrayList<HR>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hr_recyler_listview)
        recyclerViewInit()
        GetAllHRDataWebService()
    }

    /*
recycler view initialization method
*/
    fun recyclerViewInit() {
        // Initialize item list
        hrsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = findViewById(R.id.all_hr_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = HRListAdapter(applicationContext, hrsArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(this)
        // Set layout manager to position the items
        recyclerView!!.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView!!.adapter = adapter
    }

    fun GetAllHRDataWebService() {
        val hitURL = "http://192.168.31.5:8080/staff/basic/list/with/photo?&access_token=5d80f06c-b278-4122-8116-88db9b382662"

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Request.Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val hrlist = HR()
                        //set the json data in the model
                        hrlist.setHr_id(c.getString("staffId"))
                        hrlist.setHr_name(c.getString("staffName"))
                        hrlist.setHr_gender(c.getString("gender"))
                        hrlist.setHr_religion(c.getString("staffReligion"))
                        hrlist.setHr_category(c.getString("staffCategory"))
                        hrlist.setHr_designation(c.getString("designationName"))
                        hrlist.setHr_blood_grp(c.getString("bloodGroup"))
                        hrlist.setHr_phone(c.getString("staffMobile1"))
                        hrlist.setHr_image(c.getString("image"))

                        // adding item to ITEM list
                        hrsArrayList!!.add(hrlist)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                //Notify adapter if data change
                adapter!!.notifyDataSetChanged()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()

                headers["Content-Type"] = "application/x-www-form-urlencoded"
                //headers.put("access_token", "7e1bb8bc-c8ef-4821-8811-97a1eef7135c");

                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        CloudRequest.getInstance(this).addToRequestQueue(postRequest)
    }

    companion object {
        private val TAG = "AllHRActivity"
    }

}

