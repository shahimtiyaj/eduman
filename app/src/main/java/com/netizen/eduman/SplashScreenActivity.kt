package com.netizen.eduman

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import com.netizen.eduman.Slide.SlidingsActivity
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestFullScreenWindow()
        setContentView(R.layout.splash_final)
        viewInitialization()
        splashThread()
    }

    private fun requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    private fun viewInitialization() {
        val imageView = findViewById<View>(R.id.imageView)
        val anim = AnimationUtils.loadAnimation(applicationContext, R.anim.fade)
        imageView.startAnimation(anim)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    private fun splashThread() {
        val timer = object : Thread() {
            override fun run() {
                try {
                    DAO.executeSQL(
                        "INSERT OR REPLACE INTO " + DBHelper.TABLE_SLIDE + "(checkSlide) " + "VALUES(?)", arrayOf("0"))

                    sleep(2000)
                    val dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    val userLogin = dao?.getSliderCheck()
                    if (userLogin?.getcheck()=="0") {
                        val intent = Intent(applicationContext, SlidingsActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.left_in, R.anim.left_out)
                        finish()
                    }

                    else{
                        val intent = Intent(applicationContext, LoginActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.left_in, R.anim.left_out)
                        finish()
                    }
                    super.run()
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
        timer.start()
    }
}

