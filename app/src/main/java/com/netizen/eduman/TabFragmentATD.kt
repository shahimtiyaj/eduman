package com.netizen.eduman

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatDelegate
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class TabFragmentATD : Fragment() {

    private val tabIcons = intArrayOf(R.drawable.attendance, R.drawable.ic_student, R.drawable.hr_manager)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val x = inflater.inflate(R.layout.fragment_tab, null)
        tabLayout = x.findViewById<View>(R.id.tabs) as TabLayout
        viewPager = x.findViewById<View>(R.id.viewpager) as ViewPager
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        viewPager.adapter =MyAdapter(childFragmentManager)
        viewPager.setOffscreenPageLimit(2);

        tabLayout.post {
            tabLayout.setupWithViewPager(viewPager)

            tabLayout.getTabAt(0)!!.setIcon(tabIcons[0])
            tabLayout.getTabAt(1)!!.setIcon(tabIcons[1])
           // tabLayout.getTabAt(2)!!.setIcon(tabIcons[2])
            tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        }

        return x
    }

    internal inner class MyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return FragmentTakeAttendance()
                1 -> return FragmentSAttendanceList()
              //  2 -> return TabFragmentHPAL()
            }
            return null
        }

        override fun getCount(): Int {
            return int_items
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {

            }//   return "HR Enlistment";
            //   return "HR List";
            return null
        }
    }

    companion object {

        lateinit var tabLayout: TabLayout
        lateinit var viewPager: ViewPager
        var int_items = 2
    }
}
