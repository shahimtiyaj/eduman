package com.netizen.eduman

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import com.victor.loading.newton.NewtonCradleLoading
import de.hdodenhof.circleimageview.CircleImageView
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import java.util.*

class FragmentDashboard : Fragment() {

    private var studentCard: CardView? = null
    private var hrCard: CardView? = null
    private var attendanceCard: CardView? = null
    private var semesterExamCard: CardView? = null
    lateinit var v: View

    private var txt_scl_name: TextView? = null
    private var txt_inst_id: TextView? = null
    private var txt_academic_year: TextView? = null
    private var txt_school_address: TextView? = null
    private var mProfileImageView: CircleImageView? = null

    private var txt_sms: TextView? = null
    private var txt_bill: TextView? = null
    private var s: TextView? = null
    private var s1: TextView? = null
    private var s2: TextView? = null
    private var s3: TextView? = null
    private var s4: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences
    var newtonCradleLoading: NewtonCradleLoading? = null

    var linearLayout1: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.dashboard_final, container, false)

      try {

            newtonCradleLoading =v.findViewById(R.id.newton_cradle_loading) as NewtonCradleLoading
            linearLayout1 = v.findViewById(R.id.loader) as LinearLayout
            linearLayout1?.visibility=View.VISIBLE

            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

            txt_scl_name = v.findViewById<View>(R.id.school_name) as TextView
            txt_inst_id = v.findViewById<View>(R.id.school_id) as TextView
            txt_academic_year = v.findViewById<View>(R.id.academic_year) as TextView
            txt_school_address = v.findViewById<View>(R.id.school_address) as TextView
            mProfileImageView = v.findViewById<View>(R.id.schoolLogo) as CircleImageView

            txt_sms = v.findViewById<View>(R.id.sms) as TextView
            txt_bill = v.findViewById<View>(R.id.bill) as TextView

            sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
            accessToken = sharedpreferences.getString("accessToken", null)

            s = v.findViewById<View>(R.id.s) as TextView
            s1 = v.findViewById<View>(R.id.s1) as TextView
            s2 = v.findViewById<View>(R.id.s2) as TextView
            s3 = v.findViewById<View>(R.id.s4) as TextView
            s4 = v.findViewById<View>(R.id.s5) as TextView

            sms()
            InstituteData()

      } catch (e: java.lang.Exception) {
          e.printStackTrace()
      } catch (e: IllegalStateException) {
          e.printStackTrace()
      } catch (e: NullPointerException) {
          e.printStackTrace()
      } catch (e: IllegalArgumentException) {
          e.printStackTrace()
      } finally {
          Log.d("School Info:", "Data")
      }

       studentCard = v.findViewById<View>(R.id.studentcardId) as CardView
           hrCard = v.findViewById<View>(R.id.hrcardId) as CardView
           attendanceCard = v.findViewById<View>(R.id.attendancecardId) as CardView
           semesterExamCard = v.findViewById<View>(R.id.semesterExamcardId) as CardView

       val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
       val editor = sharedPreferences.edit()
       editor.putString("now", "home")
       editor.apply()

       OnclickView()
        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Home"
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        Log.d("ArraySize", b.size.toString())
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }

    fun billPayment() {

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val inst_info = dao?.getIntituteInfo()

        val hitURL = "https://api.netiworld.com/netiapi/eduman/due-bill?instituteID=" + inst_info?.getinstituteId()

        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->

                Log.d("Taka:", " Response: $response")

                val initValue = 10.toString()

                try {
                    val unpaidAmt = response.toString()

                    Log.d("Taka:", " Response: $unpaidAmt")

                    if (unpaidAmt > initValue) {
                        activity?.let { Toasty.error(it, "Unpaid", Toast.LENGTH_SHORT, true).show() };
                    }

                    if (unpaidAmt < initValue) {
                        activity?.let { Toasty.error(it, "Paid", Toast.LENGTH_SHORT, true).show() };
                    } else {
                        activity?.let { Toasty.error(it, "N/A", Toast.LENGTH_SHORT, true).show() };
                    }


                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    Log.d("Bill Pay Info:", "Data")
                }

            },

            Response.ErrorListener { volleyError ->
                Log.i("Volley net error:", volleyError.toString())

                val unpaidAmt = volleyError.message?.toDoubleOrNull()
                Log.i("Unpaid:", unpaidAmt.toString())

                when {
                    unpaidAmt.toString() > 10.toString() -> {
                        txt_bill?.text = "UNPAID"
                    }
                    unpaidAmt.toString() < 10.toString() -> //activity?.let { Toasty.error(it, "PAID", Toast.LENGTH_SHORT, true).show() }
                        txt_bill?.text = "PAID"
                    else ->
                        txt_bill?.text = "N/A"
                }

            }
        ) {
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                Log.i("response network .....", response.toString())

                return super.parseNetworkResponse(response)
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }


            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 500000//5 Minutes-change to what you want//500000 milliseconds = 5 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    fun sms() {
        newtonCradleLoading?.start()
        val hitURL = AppController.BaseUrl + "sms/report/sms/balance?access_token=" + accessToken
        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                Log.d("sms:", " Response: $response")
                try {
                    val smsBalance = response.getString("item")
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        txt_sms?.text = smsBalance
                        billPayment()
                        InstituteData()
                    } else {
                        txt_sms?.text = smsBalance
                    }

                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } finally {
                    Log.d("SMS Info:", "Data")
                }

                newtonCradleLoading?.stop()
                linearLayout1?.visibility=View.GONE
            },

            Response.ErrorListener { volleyError ->
                Log.i("Volley net error:", volleyError.toString())
                newtonCradleLoading?.stop()
                linearLayout1?.visibility=View.GONE
            }
        ) {
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                Log.i("response network .....", response.toString())
                return super.parseNetworkResponse(response)
            }
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }


    fun InstituteData() {

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val inst_info = dao?.getIntituteInfo()

        var theByteArray: ByteArray? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (inst_info?.getinstituteLogo() != "") {
                theByteArray = Base64.getDecoder().decode(inst_info?.getinstituteLogo())
            }
        }

        txt_scl_name?.setText(inst_info?.getuserinstituteName())
        val typeface = Typeface.createFromAsset(activity?.assets, "fonts/Raleway-Bold.ttf")
        txt_scl_name?.setTypeface(typeface)

        txt_school_address?.setText(inst_info?.getinstituteAddress())
        txt_inst_id?.setText("  Institute ID           :  " + inst_info?.getinstituteId())
        txt_academic_year?.setText("Academic Year  :  " + inst_info?.getacademic_year())

        txt_school_address?.setText(inst_info?.getinstituteAddress())

        val typeface2 = Typeface.createFromAsset(activity?.assets, "fonts/Poppins-Regular.ttf")
        txt_school_address?.setTypeface(typeface2)
        txt_inst_id?.setTypeface(typeface2)
        txt_academic_year?.setTypeface(typeface2)

        if (inst_info?.getinstituteLogo() != null || inst_info?.getinstituteLogo() != "") {
            mProfileImageView?.setImageBitmap(theByteArray?.let { convertToBitmap(it) })
        }

        s?.visibility = (View.VISIBLE)
        s1?.visibility = (View.VISIBLE)
        s2?.visibility = (View.VISIBLE)
        s3?.visibility = (View.VISIBLE)
        s4?.visibility = (View.VISIBLE)

    }

    fun OnclickView() {
         studentCard?.setOnClickListener {
             try {
                 activity?.title=null
                 val stdtabFragment = FragmentStudentDashboard()
                 val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                 fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)
                 fragmentTransaction.commit()
             } catch (e: Exception) {
                 e.printStackTrace()
             }
         }
         hrCard?.setOnClickListener {
             activity?.title=null
             val hrmtabFragment = FragmentHRDashboard()
             val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
             fragmentTransaction.replace(R.id.fragment_container, hrmtabFragment)
             fragmentTransaction.commit()
         }

         attendanceCard?.setOnClickListener {
             activity?.title=null
             val atdtDashFragment = FragmentAttdanceDash()
             val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
             fragmentTransaction.replace(R.id.fragment_container, atdtDashFragment)
             fragmentTransaction.commit()
         }

         semesterExamCard?.setOnClickListener {
             activity?.title=null
             val fragmentSemesterExam = FragmentSemesterExamDash()
             val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
             fragmentTransaction.replace(R.id.fragment_container, fragmentSemesterExam)
             fragmentTransaction.commit()
         }
    }
}
