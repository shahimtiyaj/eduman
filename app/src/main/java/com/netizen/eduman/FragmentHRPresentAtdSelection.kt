package com.netizen.eduman

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.text.InputType
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.ViewDialog
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

class FragmentHRPresentAtdSelection : Fragment(), View.OnClickListener {

    private var btn_serach_hr_present: Button? = null
    private var txt_back: TextView? = null
    private var hr_attndance_smry_date: EditText? = null
    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    lateinit var  v: View

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences
    var viewDialog: ViewDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_attendance_selection, container, false)
        initializeViews()
        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "hr_present_selection")
        editor.apply()
        return v
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) };

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)
        activity?.title="Report"

        hr_attndance_smry_date = v.findViewById<View>(R.id.hr_attendance_summary_date) as EditText
        dateFormatter = SimpleDateFormat("MM/dd/yyyy", Locale.US)
        hr_attndance_smry_date?.inputType = InputType.TYPE_NULL
        hr_attndance_smry_date?.setOnClickListener(this)

        btn_serach_hr_present = v.findViewById<View>(R.id.btn_hr_attendance_search) as Button

        btn_serach_hr_present?.setOnClickListener {
            try {
                hr_p_attndance_smry_date = hr_attndance_smry_date?.getText().toString()

                if (TextUtils.isEmpty(hr_p_attndance_smry_date)) {
                    activity?.let { Toasty.error(it, "Select Date", Toast.LENGTH_SHORT, true).show() };
                }

               else if (!AppController.instance?.isNetworkAvailable()!!) {
                    AppController.instance?.noInternetConnection()
                }

                else {
                    val senrollmentFragment = TabFragmentHPAL()
                    val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, senrollmentFragment)
                    fragmentTransaction.commit()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        setDateTimeField()

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {

                val allstdListFragment = FragmentStdHRreportDasboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onClick(v: View) {
        if (v === hr_attndance_smry_date) {
            fromDatePickerDialog?.show()
        }
    }

    private fun setDateTimeField() {
        val newCalendar = Calendar.getInstance()
        fromDatePickerDialog = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                hr_attndance_smry_date!!.setText(dateFormatter!!.format(newDate.time))
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)
        )

        fromDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())

    }

    companion object {

        private val TAG = "FragmentHRPresentAtdSelection"
        var hr_p_attndance_smry_date = ""

    }
}
