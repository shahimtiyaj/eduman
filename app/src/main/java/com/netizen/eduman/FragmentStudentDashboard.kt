package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO

class FragmentStudentDashboard : Fragment() {

    private var studentCard: CardView? = null
    private var student_list_Card: CardView? = null

    lateinit var v: View
    private var txt_back: TextView? = null

    private var txt_scl_name: TextView? = null
    private var txt_inst_id: TextView? = null
    private var txt_academic_year: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.student_dash_board_final, container, false)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        studentCard = v.findViewById<View>(R.id.btn_enroll_student) as CardView
        student_list_Card = v.findViewById<View>(R.id.btn_find_student) as CardView

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "student_dash")
        editor.apply()

        txt_scl_name = v.findViewById<View>(R.id.school_name) as TextView
        txt_inst_id = v.findViewById<View>(R.id.school_id) as TextView
        txt_academic_year = v.findViewById<View>(R.id.academic_year) as TextView

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val inst_info = dao?.getIntituteInfo()

        txt_scl_name?.setText(inst_info?.getuserinstituteName())
        txt_inst_id?.setText("  Institute ID        :  " + inst_info?.getinstituteId())
        txt_academic_year?.setText("Academic Year  :  " + inst_info?.getacademic_year())

        try {

            OnclickView()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }

        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Student"
    }

    fun OnclickView() {
        studentCard?.setOnClickListener {
            try {
                activity?.title=null
                val stdtabFragment = FragmentSEnrollmentForm()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        student_list_Card?.setOnClickListener {
            activity?.title=null
            val hrmtabFragment = FragGetStudentListSelection()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, hrmtabFragment)
            fragmentTransaction.commit()
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = FragmentDashboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


}
