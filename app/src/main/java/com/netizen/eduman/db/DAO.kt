package com.netizen.eduman.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DBHelper.Companion.TABLE_ACADEMIC_YEAR
import com.netizen.eduman.db.DBHelper.Companion.TABLE_ATTENDANCESUMMARYPERIOD
import com.netizen.eduman.db.DBHelper.Companion.TABLE_EXAM
import com.netizen.eduman.db.DBHelper.Companion.TABLE_EXAM_RESULT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_GROUP
import com.netizen.eduman.db.DBHelper.Companion.TABLE_INPUT_MARK_SUB
import com.netizen.eduman.db.DBHelper.Companion.TABLE_LOGIN
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_INPUT_STUDENT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_MARK_SCALE_DISTRIBUTION
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION_INPUT_MARK
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SECTION_RESULT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_SLIDE
import com.netizen.eduman.db.DBHelper.Companion.TABLE_STUDENT_RESULT
import com.netizen.eduman.db.DBHelper.Companion.TABLE_TAKEATTENDANCE
import com.netizen.eduman.model.*
import java.util.*


/*
 * Data access object class
 * Data Access Objects are the main classes where we define our database interactions
 */
class DAO(context: Context) {

    // Database fields
    private var db: SQLiteDatabase? = null
    private val dbHelper: DBHelper

    init {
        dbHelper = DBHelper(context)
    }

    @Throws(Throwable::class)
    protected fun finalize() {
        // TODO Auto-generated method stub
        if (db != null && db!!.isOpen)
            db!!.close()
    }

    /*
    Open any close database object
     */
    @Throws(SQLException::class)
    fun open() {
        //Create and/or open a database that will be used for reading and writing.
        db = dbHelper.writableDatabase
    }

    /*
     Close any open database object.
     */
    fun close() {
        dbHelper.close()
    }


    /*
    execSQL doesn't return anything and used for creating,updating, replacing
     */
    @Throws(SQLException::class)
    fun execSQL(sql: String, param: Array<String>) {
        db!!.execSQL(sql, param)
    }


    companion object {
        private val TAG = DAO::class.java.simpleName

        /*
        executeSQL doesn't return anything and used for creating,updating, replacing
        Not need to create extra DAO object .
     */
        fun executeSQL(sql: String, param: Array<String>) {
            val da = AppController.instance?.let { DAO(it) }
            da?.open()
            try {
                da?.execSQL(sql, param)
            } catch (e: Exception) {
                throw e
            } finally {
                da?.close()
            }
        }
    }

    // TODO Auto-generated catch block
    val allTakeAttendance: ArrayList<Attendance>

        get() {
            val attendancesArrayList = ArrayList<Attendance>()
            var attendance: Attendance?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_TAKEATTENDANCE ",
                    arrayOf(
                        "[studentId]",
                        "[studentRoll]",
                        "[studentName]",
                        "[studentGender]",
                        "[identificationId]",
                        "[checked]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        attendance = Attendance(
                            curs.getString(0),
                            curs.getString(1),
                            curs.getString(2),
                            curs.getString(3),
                            curs.getString(4),
                            curs.getInt(5)

                        )
                        attendancesArrayList.add(attendance)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return attendancesArrayList
        }

    // TODO Auto-generated catch block
    val allTakeAttendanceChecked: ArrayList<Attendance>
        get() {
            val attendancesArrayList = ArrayList<Attendance>()
            var attendance: Attendance?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_TAKEATTENDANCE WHERE checked = 1",
                    arrayOf(
                        "[studentId]",
                        "[studentRoll]",
                        "[studentName]",
                        "[studentGender]",
                        "[identificationId]",
                        "[checked]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        attendance = Attendance(
                            curs.getString(0),
                            curs.getString(1),
                            curs.getString(2),
                            curs.getString(3),
                            curs.getString(4),
                            curs.getInt(5)

                        )
                        attendancesArrayList.add(attendance)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return attendancesArrayList
        }

    fun GetSectionResultID(className: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [classId] FROM SectionResult WHERE className = '$className'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs?.moveToFirst()!!) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun GetSectionID(sectionShiftName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [classConfigId] FROM Section WHERE classShiftSection = '$sectionShiftName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs?.moveToFirst()!!) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun GetMarkInputSectionID(sectionShiftName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [classConfigId] FROM Section WHERE classShiftSection = '$sectionShiftName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs?.moveToFirst()!!) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetPeriodID(periodName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [periodId] FROM Attendance WHERE periodName = '$periodName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun UpdateSingleCategoryFlag(cflag: Int, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[checked]", cflag)
        db!!.update(TABLE_TAKEATTENDANCE, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun totalAbssentStudent(): Int {
        var nombr = 0
        val cursor = db!!.rawQuery("SELECT studentId FROM TakeAttendance", null)
        nombr = cursor.count
        return nombr
    }

    fun GetAtSummaryPeriodID(periodName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [typeId] FROM AttendanceSummaryPeriod WHERE periodName = '$periodName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun getDefaultIdFromMarkDT(): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [defaultId] FROM MarkScaleDistribution"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun GetExamName(de: String): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [shortCodeName] FROM MarkScaleDistribution WHERE defaultId = $de"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }


    fun GetTotalMark(de: String): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [totalMark] FROM MarkScaleDistribution WHERE defaultId = $de"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetExamCode3(): String {
        var selectQuery = ""
        var curs: Cursor? = null
        try {
            selectQuery = "SELECT [shortCodeName] FROM MarkScaleDistribution WHERE defaultId = 3"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun UpdateExamCode1(examCode1: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode1]", examCode1)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun UpdateExamCode2(examCode2: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode2]", examCode2)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun UpdateExamCode3(examCode3: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()

        // cv.put("[shortCode1]", examCode3)
        // cv.put("[shortCode2]", examCode3)
        cv.put("[shortCode3]", examCode3)
        // cv.put("[shortCode4]", examCode3)
        // cv.put("[shortCode5]", examCode3)
        // cv.put("[shortCode6]", examCode3)


        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun UpdateExamCode4(examCode4: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode4]", examCode4)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun UpdateExamCode5(examCode5: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode5]", examCode5)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }

    fun UpdateExamCode6(examCode6: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[shortCode6]", examCode6)

        db?.update(TABLE_MARK_INPUT_STUDENT, cv, "[identificationId]=?", arrayOf(identificationId))

    }


    //May 2-2019

    fun getALLInputMark(): ArrayList<InputMark> {
        val inputMarkArrayList = ArrayList<InputMark>()
        var inputMark: InputMark? = null
        var curs: Cursor? = null
        try {
            curs = db?.query(
                TABLE_MARK_INPUT_STUDENT,
                arrayOf(
                    "[markinputId]",
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentName]",
                    "[studentRoll]",
                    "[shortCode1]",
                    "[shortCode2]",
                    "[shortCode3]",
                    "[shortCode4]",
                    "[shortCode5]",
                    "[shortCode6]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    inputMark = InputMark(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getString(7),
                        curs.getString(8),
                        curs.getString(9),
                        curs.getString(10)

                    )
                    inputMarkArrayList.add(inputMark)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return inputMarkArrayList
    }

    fun getALLMarkDistrubutionScale(): ArrayList<MarkDistribution> {
        val attendancesArrayList = ArrayList<MarkDistribution>()
        var attendance: MarkDistribution? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_MARK_SCALE_DISTRIBUTION,
                arrayOf("[shortCodeName]", "[passMark]", "[totalMark]", "[acceptance]", "[defaultId]"),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    attendance = MarkDistribution(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4)

                    )
                    attendancesArrayList.add(attendance)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return attendancesArrayList
    }


    fun UpdateRemarkStudentFlag(cflag: Int, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[checked]", cflag)
        db?.update(
            TABLE_STUDENT_RESULT, cv, "[identificationId]=?",
            arrayOf(identificationId)
        )
    }


    fun getAllRemarksStudentChecked(): ArrayList<ResultSummary> {
        val remarksArrayList = ArrayList<ResultSummary>()
        var resultSummary: ResultSummary? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                "$TABLE_STUDENT_RESULT WHERE checked = 1",
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentRoll]",
                    "[studentName]",
                    "[totalMarks]",
                    "[letterGrade]",
                    "[gradingPoint]",
                    "[checked]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    resultSummary = ResultSummary(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getInt(7)
                    )
                    remarksArrayList.add(resultSummary)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return remarksArrayList
    }

    fun getAllRemarksStudent(): ArrayList<ResultSummary> {
        val remarksArrayList = ArrayList<ResultSummary>()
        var resultSummary: ResultSummary? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_STUDENT_RESULT,
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentRoll]",
                    "[studentName]",
                    "[totalMarks]",
                    "[letterGrade]",
                    "[gradingPoint]",
                    "[checked]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    resultSummary = ResultSummary(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getInt(7)
                    )
                    remarksArrayList.add(resultSummary)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return remarksArrayList
    }

    fun getAllRemarksStudentForUpdate(): ArrayList<ResultSummary> {
        val remarksArrayList = ArrayList<ResultSummary>()
        var resultSummary: ResultSummary? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_STUDENT_RESULT,
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentRoll]",
                    "[studentName]",
                    "[totalMarks]",
                    "[letterGrade]",
                    "[gradingPoint]",
                    "[remarkId]",
                    "[remarks]",
                    "[remarksTitle]",
                    "[checked]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    resultSummary = ResultSummary(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getString(7),
                        curs.getString(8),
                        curs.getString(9),
                        curs.getInt(10)
                    )
                    remarksArrayList.add(resultSummary)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return remarksArrayList
    }

    fun getAllRemarksStudentUpdate(): ArrayList<ResultSummary> {
        val remarksArrayList = ArrayList<ResultSummary>()
        var resultSummary: ResultSummary? = null
        var curs: Cursor? = null

        try {
            curs = db?.query(
                TABLE_STUDENT_RESULT + " WHERE checked = 1",
                arrayOf(
                    "[identificationId]",
                    "[customStudentId]",
                    "[studentRoll]",
                    "[studentName]",
                    "[totalMarks]",
                    "[letterGrade]",
                    "[gradingPoint]",
                    "[remarkId]",
                    "[remarks]",
                    "[remarksTitle]",
                    "[checked]"
                ),
                null,
                null,
                null,
                null,
                null
            )

            if (curs!!.moveToFirst()) {
                do {
                    resultSummary = ResultSummary(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getString(4),
                        curs.getString(5),
                        curs.getString(6),
                        curs.getString(7),
                        curs.getString(8),
                        curs.getString(9),
                        curs.getInt(10)
                    )
                    remarksArrayList.add(resultSummary)
                } while (curs.moveToNext())
            }

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())

        } finally {
            curs?.close()
        }

        return remarksArrayList
    }

    fun UpdateRemarkDescription(remarksDes: String, identificationId: String) {
        val cv = ContentValues()
        cv.clear()
        cv.put("[remarks]", remarksDes)
        db?.update(
            TABLE_STUDENT_RESULT, cv, "[identificationId]=?",
            arrayOf(identificationId)
        )

    }


    fun totalRemarksUpdateStudent(): Int {
        var nombr = 0
        val cursor = db?.rawQuery("SELECT identificationId FROM StudentResult", null)
        nombr = cursor?.count!!
        return nombr
    }

    //------------------------6/17/2019-------------
    fun GetExamConfigID(examName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [examConfigId] FROM Exam WHERE ExamName = '$examName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetGroupID(grpName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [GroupId] FROM SGroup WHERE GroupName = '$grpName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetDesignationID(desigName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [DesigId] FROM HRDesignation WHERE DesigName = '$desigName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetCategoryID(categoryName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [CategoryId] FROM STCategory WHERE CategoryName = '$categoryName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun GetRemarkID(remarktitle: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [remarkTempId] FROM RemarksTitle WHERE remarkTitle = '$remarktitle'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    //------------------------5/16/2019---------------
    fun GetSubjectID(subName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [SubjectId] FROM InputMarkSubject WHERE SubjectName = '$subName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    //exam result summary showing .................
    fun GetExamResultID(examName: String, secPos: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [ExamId] FROM ExamResult WHERE ExamName = '$examName' AND SectionPosition ='$secPos'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }

    fun deleteUsers() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(TABLE_LOGIN, null, null)

            Log.d(TAG, "Deleted all user info from sqlite")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun getUserDetails(): UserLogin {

        try {

            val selectQuery = "SELECT *FROM Login"

            val cursor = db?.rawQuery(selectQuery, null)
            val user = UserLogin()
            if (cursor?.moveToFirst()!!) {
                do {
                    user.setuserName(cursor.getString(0))

                } while (cursor.moveToNext())
            }

            return user
        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }

    fun getSliderCheck(): UserLogin {

        try {

            val selectQuery = "SELECT *FROM Slide"

            val cursor = db?.rawQuery(selectQuery, null)
            val user = UserLogin()
            if (cursor?.moveToFirst()!!) {
                do {
                    user.setcheck(cursor.getString(0))

                } while (cursor.moveToNext())
            }

            return user
        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }

    }


    fun getIntituteInfo(): Institute {

        try {

            val selectQuery = "SELECT *FROM InstituteInfo"

            val cursor = db?.rawQuery(selectQuery, null)
            val institute = Institute()
            if (cursor?.moveToFirst()!!) {
                do {
                    institute.setuinstituteId(cursor.getString(0))
                    institute.setinstituteName(cursor.getString(1))
                    institute.setinstituteAddress(cursor.getString(2))
                    institute.setacademic_year(cursor.getString(3))
                    institute.setinstituteLogo(cursor.getString(4))

                } while (cursor.moveToNext())
            }

            return institute

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e
        }
        catch (e: NullPointerException) {
            Log.e(TAG, e.toString(), e);
            throw e
        }
        catch (e: java.lang.NullPointerException) {
            Log.e(TAG, e.toString(), e);
            throw e
        }
        catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }


    fun deleteTakeAttendance() {
        try {
            db?.delete(TABLE_TAKEATTENDANCE, null, null)

            Log.d(TAG, "Deleted all take attendance info from sqlite")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    // TODO Auto-generated catch block
    val allSectionData: ArrayList<Section>
        get() {
            val sectionArrayList = ArrayList<Section>()
            var section: Section?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_SECTION ORDER BY classConfigId ASC",
                    arrayOf(
                        "[classShiftSection]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        section = Section(
                            curs.getString(0)
                        )
                        sectionArrayList.add(section)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return sectionArrayList
        }


    // TODO Auto-generated catch block
    val allSectionResultData: ArrayList<Section>
        get() {
            val sectionArrayList = ArrayList<Section>()
            var section: Section?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_SECTION_RESULT ORDER BY classId ASC",
                    arrayOf(
                        "[className]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        section = Section(
                            curs.getString(0)
                        )
                        sectionArrayList.add(section)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return sectionArrayList
        }


    // TODO Auto-generated catch block
    val allMarkInputSectionData: ArrayList<Section>
        get() {
            val sectionArrayList = ArrayList<Section>()
            var section: Section?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_SECTION_INPUT_MARK ORDER BY classConfigId ASC",
                    arrayOf(
                        "[classShiftSection]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        section = Section(
                            curs.getString(0)
                        )
                        sectionArrayList.add(section)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return sectionArrayList
        }



    // TODO Auto-generated catch block
    val allAcademicData: ArrayList<AcademicYear>
        get() {
            val academicYearArrayList = ArrayList<AcademicYear>()
            var academic: AcademicYear?
            var curs: Cursor? = null

            try {
                curs = db?.query(
                    "$TABLE_ACADEMIC_YEAR ORDER BY academicYear DESC",
                    arrayOf(
                        "[academicYear]"
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )

                if (curs!!.moveToFirst()) {
                    do {
                        academic = AcademicYear(
                            curs.getString(0)
                        )
                        academicYearArrayList.add(academic)
                    } while (curs.moveToNext())
                }

            } catch (e: Exception) {
                Log.e(TAG, e.toString())

            } finally {
                curs?.close()
            }

            return academicYearArrayList
        }

    fun totalAttendanceStudentCount(): Int {
        var nombr = 0
        val cursor = db?.rawQuery("SELECT studentId FROM TakeAttendance", null)
        nombr = cursor?.count!!
        return nombr
    }

    fun deleteInputMark() {
        try {
            db?.delete(TABLE_MARK_INPUT_STUDENT, null, null)

            Log.d(TAG, "Deleted all all input mark from")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteMarkDistribution() {
        try {
            // Delete All Rows
            db?.delete(TABLE_MARK_SCALE_DISTRIBUTION, null, null)

            Log.d(TAG, "Deleted all all input mark distribution from")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteStudentResultList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_STUDENT_RESULT, null, null)

            Log.d(TAG, "deleteStudentResultList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteTakeAttendanceList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_TAKEATTENDANCE, null, null)

            Log.d(TAG, "deleteTakeAttendanceList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteSectionList() {
        val cv = ContentValues()
        try {
            // Delete All Rows
            db?.delete(TABLE_SECTION, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteAcademicList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_ACADEMIC_YEAR, null, null)

            Log.d(TAG, "deleteAcademicYear")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteSubjectList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_INPUT_MARK_SUB, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteGroupList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_GROUP, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteExamList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_EXAM, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteRemarksList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_EXAM, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deletePeriodList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_ATTENDANCESUMMARYPERIOD, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deletePeriod1List() {
        try {
            // Delete All Rows
            db?.delete(TABLE_ATTENDANCESUMMARYPERIOD, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteExamResultList() {
        try {
            // Delete All Rows
            db?.delete(TABLE_EXAM_RESULT, null, null)

            Log.d(TAG, "deleteSectionList")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun clearInputMark() {
        try {
            // Delete All Rows
            db?.delete(TABLE_MARK_INPUT_STUDENT, null, null)

            Log.d(TAG, "clearInputMark")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun clearMarkDistrubution() {
        try {
            // Delete All Rows
            db?.delete(TABLE_MARK_SCALE_DISTRIBUTION, null, null)

            Log.d(TAG, "clearInputMark")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun UpdateSlideCheck() {
        val cv = ContentValues()
        cv.clear()
        cv.put("[checkSlide]", 1)
        db?.update(TABLE_SLIDE, cv, null, null)
    }

    fun deleteSectionData() {
        try {
            // Delete All Rows
            db?.delete(TABLE_SECTION, null, null)

            Log.d(TAG, "Deleted all all section")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    fun deleteSectionResultData() {
        try {
            // Delete All Rows
            db?.delete(TABLE_SECTION_RESULT, null, null)

            Log.d(TAG, "Deleted all all section  result")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }


    fun deleteInputMarkSectionData() {
        try {
            // Delete All Rows
            db?.delete(TABLE_SECTION_INPUT_MARK, null, null)

            Log.d(TAG, "Deleted all all section  result")

        } catch (e: SQLException) {
            Log.e(TAG, e.toString(), e);
            throw e

        } catch (e: Exception) {
            Log.e(TAG, e.toString(), e);
            throw e

        } finally {
            Log.e(TAG, "Deleted");
        }
    }

    //exam result summary showing .................
    fun GetUnAssignedExamID(examName: String): String {
        var selectQuery = ""
        var curs: Cursor? = null

        try {
            selectQuery = "SELECT [ExamId] FROM Exam WHERE ExamName = '$examName'"
            curs = db?.rawQuery(selectQuery, null)
            //curs = db.rawQuery("SELECT [classConfigId] FROM " + DBHelper.TABLE_SECTION + " Where classShiftSection = " + sectionShiftName,null);
            if (curs!!.moveToFirst()) {
                selectQuery = curs.getString(0)
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString())
        } finally {
            curs?.close()
        }
        return selectQuery
    }
}

