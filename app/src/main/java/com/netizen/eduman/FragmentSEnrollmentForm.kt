package com.netizen.eduman

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.android.volley.*
import com.android.volley.Response.ErrorListener
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.shimmer.ShimmerFrameLayout
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_GROUP
import com.netizen.eduman.db.DBHelper.Companion.TABLE_ST_CATEGORY
import com.netizen.eduman.model.*
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class FragmentSEnrollmentForm : Fragment(), AdapterView.OnItemSelectedListener {
    private var btn_reg: Button? = null
    private var mStatusCode: Int = 0

    private var txt_back: TextView? = null
    private var spinner_group: Spinner? = null
    private var spinner_category: Spinner? = null
    private var spinner_session: Spinner? = null
    private var spinner_section: CustomSearchableSpinner? = null
    private var spinner_student_id: Spinner? = null
    private var spinner_gender: Spinner? = null
    private var spinner_religion: Spinner? = null

    private var inputStudentCustomID: EditText? = null
    private var inputStudentRoll: EditText? = null
    private var inputStudentName: EditText? = null
    private var inputFatherName: EditText? = null
    private var inputMotherName: EditText? = null
    private var inputPhoneNo: EditText? = null
    private var custom_id_head: TextView? = null
    private var institute_name: TextView? = null

    private var studentCustom: String? = null
    private var rollNo: String? = null
    private var name: String? = null
    private var fname: String? = null
    private var mname: String? = null
    private var phone: String? = null
    private var selectAcademic: String? = null
    private var selectSection: String? = null
    private var selectGroup: String? = null
    private var selectCategory: String? = null
    private var selectstdIdType: String? = null
    private var selectGender: String? = null
    private var selectReligion: String? = null
    private var pos: String? = null
    private var localSectionID: String? = null
    private var localGroupID: String? = null
    private var localCategoryID: String? = null

    private var std_id_type: String? = null
    private var linearLayout1: LinearLayout? = null
    private var linearLayout2: LinearLayout? = null
    internal var categoriesArrayList = ArrayList<Category>()
    internal var sectionsArrayList = ArrayList<Section>()
    internal var groupsArrayList = ArrayList<Group>()
    private var jObjPost: String? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null
    internal var ok: TextView? = null
    internal var no: TextView? = null
    private var myDialog: Dialog? = null
    private var shimmerLayout: ShimmerFrameLayout? = null
    private var shimmerLayout1: ShimmerFrameLayout? = null

    private var v: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try {

            v = inflater.inflate(R.layout.student_enrollment_laypout, container, false)
            shimmerLayout = v?.findViewById(R.id.shimmer_layout_id)
            shimmerLayout1 = v?.findViewById(R.id.shimmer_layout_id1)

            initializeViews()

            val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("now", "std_reg")
            editor.apply()

            setsectionSpinnerData()
            setAcademicYearSpinnerData()

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }

        return v
    }

    override fun onStart() {

        super.onStart()

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        dao?.deleteGroupList()
        dao?.close()
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Enrollment Form"
    }


    override fun onPause() {
        dismissSpinner()
        super.onPause()
        shimmerLayout?.stopShimmer()
        shimmerLayout1?.stopShimmer()
    }


    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }

        myDialog = Dialog(requireContext())

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        inputStudentCustomID = v?.findViewById<View>(R.id.et_student_custom_id) as EditText
        inputStudentRoll = v?.findViewById<View>(R.id.et_student_roll_no) as EditText
        inputStudentName = v?.findViewById<View>(R.id.et_student_name) as EditText
        inputFatherName = v?.findViewById<View>(R.id.et_student_father_name) as EditText
        inputMotherName = v?.findViewById<View>(R.id.et_student_mother_name) as EditText
        inputPhoneNo = v?.findViewById<View>(R.id.et_student_mobile) as EditText
        custom_id_head = v?.findViewById<View>(R.id.srudent_head) as TextView
        institute_name = v?.findViewById<View>(R.id.inst_name) as TextView

        inputPhoneNo?.addTextChangedListener(MyTextWatcher(inputPhoneNo!!))

        btn_reg = v?.findViewById<View>(R.id.btn_reg_submit) as Button

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()

        linearLayout1 = v?.findViewById(R.id.linear12) as LinearLayout
        linearLayout2 = v?.findViewById(R.id.linear13) as LinearLayout

        spinner_session = v?.findViewById<View>(R.id.spinner_session) as Spinner
        spinner_session?.onItemSelectedListener = this

        spinner_section = v?.findViewById<View>(R.id.spinner_section) as CustomSearchableSpinner
        spinner_section?.setTitle("Search Section")

        spinner_section?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                CustomSearchableSpinner.isCountriesSpinnerOpen = false
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                CustomSearchableSpinner.isCountriesSpinnerOpen = false

                val spinner = parent as CustomSearchableSpinner

                if (spinner.id == R.id.spinner_section) {
                    selectSection = spinner_section?.selectedItem.toString()
                    if (selectSection == resources.getString(R.string.select_section)) {
                        selectSection = ""
                    } else {
                        pos = position.toString()

                        selectSection = parent.getItemAtPosition(position).toString()

                        val da = DAO(activity!!)
                        da.open()
                        localSectionID = da.GetSectionID(selectSection!!)

                        loadServerGroupData()

                        da.close()
                    }
                }
            }

        }

        spinner_group = v?.findViewById<View>(R.id.spinner_group) as Spinner
        spinner_group?.onItemSelectedListener = this

        spinner_category = v?.findViewById<View>(R.id.spinner_category) as Spinner
        spinner_category?.onItemSelectedListener = this

        spinner_student_id = v?.findViewById<View>(R.id.spinner_std_id_type) as Spinner
        spinner_student_id?.onItemSelectedListener = this

        spinner_gender = v?.findViewById<View>(R.id.spinner_gender) as Spinner
        spinner_gender?.onItemSelectedListener = this

        spinner_religion = v?.findViewById<View>(R.id.spinner_religion) as Spinner
        spinner_religion?.onItemSelectedListener = this

        btn_reg?.setOnClickListener {
            try {

                if (!AppController.instance?.isNetworkAvailable()!!) {

                    AppController.instance?.noInternetConnection()
                } else {

                    submitRegistration()
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = FragmentStudentDashboard()
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                fragmentTransaction?.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction?.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun dismissSpinner() {
        val searchableSpinnerDialog = fragmentManager!!.findFragmentByTag("TAG")
        if (searchableSpinnerDialog != null && searchableSpinnerDialog.isAdded) {
            fragmentManager!!.beginTransaction().remove(searchableSpinnerDialog).commit()
        }
    }



    private fun setAcademicYearSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val academicYears = dao?.allAcademicData

            val academicYear = ArrayList<String>()
            academicYear.add(resources.getString(R.string.select_session))

            for (i in academicYears?.indices!!) {
                academicYears[i].getAcademicYear()?.let { academicYear.add(it) }
            }

            val academicAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, academicYear)
            academicAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_session?.adapter = academicAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    private fun setcategorySpinnerData(categoryArrayList: ArrayList<Category>?) {
        try {
            val category = ArrayList<String>()
            category.add(resources.getString(R.string.select_category))
            for (i in categoryArrayList?.indices!!) {
                categoryArrayList[i].getCategoryName()?.let { category.add(it) }
            }

            val categoryAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, category)
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_category?.adapter = categoryAdapter

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    private fun setsectionSpinnerData() {
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val attendances = dao?.allSectionData

            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in attendances?.indices!!) {
                attendances.get(i).getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_section?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    private fun setgroupSpinnerData(groupArrayList: ArrayList<Group>?) {
        try {
            val group = ArrayList<String>()
            group.add(resources.getString(R.string.select_group))
            for (i in groupArrayList?.indices!!) {
                groupArrayList[i].getGroupName()?.let { group.add(it) }
            }

            val groupAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, group)
            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_group?.adapter = groupAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_session) {

                selectAcademic = spinner_session?.selectedItem.toString()
                if (selectAcademic == resources.getString(R.string.select_session)) {
                    selectAcademic = ""
                } else {

                    pos = position.toString()

                    if (selectAcademic.equals("Select Academic Year")) {
                        Toast.makeText(activity, "Select Academic Year", Toast.LENGTH_LONG).show()
                    } else {
                        selectAcademic = parent.getItemAtPosition(position).toString()
                    }
                }
            }

            if (spinner.id == R.id.spinner_group) {
                selectGroup = spinner_group?.selectedItem.toString()
                if (selectGroup == resources.getString(R.string.select_group)) {
                    selectGroup = ""
                } else {
                    pos = position.toString()
                    selectGroup = parent.getItemAtPosition(position).toString()

                    val da = activity?.let { DAO(it) }
                    da?.open()
                    localGroupID = da?.GetGroupID(selectGroup!!)

                    loadServerCategoryData()

                    da?.close()
                }
            }

            if (spinner.id == R.id.spinner_category) {
                selectCategory = spinner_category?.selectedItem.toString()
                if (selectCategory == resources.getString(R.string.select_category)) {
                    selectCategory = ""
                } else {
                    pos = position.toString()
                    selectCategory = parent.getItemAtPosition(position).toString()

                    val da = activity?.let { DAO(it) }
                    da?.open()
                    localCategoryID = da?.GetCategoryID(selectCategory!!)
                    da?.close()
                }
            }

            if (spinner.id == R.id.spinner_gender) {
                selectGender = spinner_gender?.selectedItem.toString()
                if (selectGender == resources.getString(R.string.select_gender)) {
                    selectGender = ""
                } else {
                    pos = position.toString()
                    selectGender = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_religion) {
                selectReligion = spinner_religion?.selectedItem.toString()
                if (selectReligion == resources.getString(R.string.select_religion)) {
                    selectReligion = ""
                } else {
                    pos = position.toString()
                    selectReligion = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_std_id_type) {
                selectstdIdType = spinner_student_id?.selectedItem.toString()
                if (selectstdIdType == resources.getString(R.string.select_student_id_type)) {
                    selectstdIdType = ""
                } else {
                    pos = position.toString()
                    selectstdIdType = parent.getItemAtPosition(position).toString()
                    if (selectstdIdType == "Custom") {
                        std_id_type = 1.toString()
                        linearLayout1?.visibility = View.VISIBLE;
                        linearLayout2?.visibility = View.VISIBLE;
                        inputStudentCustomID?.visibility = View.VISIBLE
                        custom_id_head?.visibility = View.VISIBLE

                    } else {
                        std_id_type = 0.toString()
                        linearLayout1?.visibility = View.GONE
                        linearLayout2?.visibility = View.GONE
                        inputStudentCustomID?.visibility = View.GONE
                        custom_id_head?.visibility = View.GONE
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Student reg  ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }


    private fun submitRegistration() {
        if (!validateStudentAcademicYear()) {
            return
        }

        if (!validateStudentSection()) {
            return
        }

        if (!validateStudentGroup()) {
            return
        }
        if (!validateStudentCategory()) {
            return
        }

        if (!validateAutoID()) {
            return
        }

        if (selectstdIdType == "Custom") {

            if (!validateStudentID()) {
                return
            }
        }

        if (!validateStudentRoll()) {
            return
        }
        if (!validateName()) {
            return
        }

        if (!validateStudentGender()) {
            return
        }

        if (!validateStudentReligion()) {
            return
        }

        if (!validateStudentFatherName()) {
            return
        }
        if (!validateStudentMotherName()) {
            return
        }

        if (!validatePhone()) {
            return
        }


        sentRegistrationInformation()
    }

    private fun sentRegistrationInformation() {
        rollNo = inputStudentRoll?.text.toString()
        name = inputStudentName?.text.toString()
        fname = inputFatherName?.text.toString()
        mname = inputMotherName?.text.toString()
        phone = inputPhoneNo?.text.toString()
        studentCustom = inputStudentCustomID!!.text.toString()

        if (!inputPhoneNo?.text.toString().startsWith("01", 0, true)) {
            inputPhoneNo?.error = getString(R.string.err_msg_phone_invalid)
            requestFocus(inputPhoneNo!!)
        } else {
            dataSendToServer()
        }

    }



    private fun validatePhone(): Boolean {
        if (inputPhoneNo == null || inputPhoneNo?.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputPhoneNo?.error = getString(R.string.err_msg_phone)
            requestFocus(inputPhoneNo!!)
            return false
        } else if (inputPhoneNo?.length() != 11) {
            inputPhoneNo?.error = getString(R.string.err_msg_phone_invalid)
            requestFocus(inputPhoneNo!!)
            return false
        }

        return true
    }

    private fun validateName(): Boolean {
        if (inputStudentName?.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputStudentName?.error = getString(R.string.err_msg_name2)
            requestFocus(inputStudentName!!)
            return false
        }
        return true
    }

    private fun validateStudentID(): Boolean {
        if (inputStudentCustomID?.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputStudentCustomID?.error = getString(R.string.err_mssg_st_id)
            inputStudentCustomID?.let { requestFocus(it) }
            return false
        }
        return true
    }

    private fun validateStudentRoll(): Boolean {
        if (inputStudentRoll?.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputStudentRoll?.error = getString(R.string.err_mssg_st_roll)
            inputStudentRoll?.let { requestFocus(it) }
            return false
        }
        return true
    }

    private fun validateStudentFatherName(): Boolean {
        if (inputFatherName?.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputFatherName?.error = getString(R.string.err_mssg_fname)
            inputFatherName?.let { requestFocus(it) }
            return false
        }
        return true
    }

    private fun validateStudentMotherName(): Boolean {
        if (inputMotherName?.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputMotherName?.error = getString(R.string.err_mssg_mname)
            inputMotherName?.let { requestFocus(it) }
            return false
        }
        return true
    }

    private fun validateStudentAcademicYear(): Boolean {
        if (selectAcademic == null || selectAcademic?.trim { it <= ' ' }!!.isEmpty() || selectAcademic == "Select Academic Year") {
            activity?.let { Toasty.error(it, "Select Academic Year", Toast.LENGTH_SHORT, true).show() };
            return false
        }

        return true
    }

    private fun validateAutoID(): Boolean {
        if (selectstdIdType == null || selectstdIdType?.trim { it <= ' ' }!!.isEmpty() || selectstdIdType == "Select Student ID Type") {
            activity?.let { Toasty.error(it, "Select Student ID Type", Toast.LENGTH_SHORT, true).show() };
            return false
        }

        return true
    }

    private fun validateStudentSection(): Boolean {
        if (selectSection == null || selectSection?.trim { it <= ' ' }!!.isEmpty()) {
            activity?.let { Toasty.error(it, "Select Section", Toast.LENGTH_SHORT, true).show() };

            return false
        } else if (selectSection == "Select Section") {
            activity?.let { Toasty.error(it, "Select Section", Toast.LENGTH_SHORT, true).show() };

            return false
        }
        return true
    }

    private fun validateStudentGroup(): Boolean {
        if (selectGroup == null || selectGroup?.trim { it <= ' ' }!!.isEmpty() || selectGroup.equals("Select Group")) {
            activity?.let { Toasty.error(it, "Select Group", Toast.LENGTH_SHORT, true).show() };

            return false
        }
        return true
    }

    private fun validateStudentCategory(): Boolean {
        if (selectCategory == null || selectCategory?.trim { it <= ' ' }!!.isEmpty() || selectCategory.equals("Select Category")) {
            activity?.let { Toasty.error(it, "Select Category", Toast.LENGTH_SHORT, true).show() };
            return false
        }
        return true
    }

    private fun validateStudentGender(): Boolean {
        if (selectGender == null || selectGender?.trim { it <= ' ' }!!.isEmpty() || selectGender.equals("Select Gender")) {
            activity?.let { Toasty.error(it, "Select Gender", Toast.LENGTH_SHORT, true).show() };

            return false
        }
        return true
    }

    private fun validateStudentReligion(): Boolean {
        if (selectReligion == null || selectReligion?.trim { it <= ' ' }!!.isEmpty() || selectReligion.equals("Select Religion")) {
            activity?.let { Toasty.error(it, "Select Religion", Toast.LENGTH_SHORT, true).show() };
            return false
        }
        return true
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private inner class MyTextWatcher(private val view: View?) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            if (!inputPhoneNo?.text.toString().startsWith("01", 0, true)) {
                inputPhoneNo?.error = getString(R.string.err_msg_phone_invalid)
                requestFocus(inputPhoneNo!!)
            }
        }

        override fun afterTextChanged(editable: Editable) {
            when (view?.id) {
                R.id.et_student_name -> validateName()
            }
        }
    }


    private fun loadServerCategoryData() {

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2106"

        shimmerLayout1?.visibility=View.VISIBLE
        shimmerLayout1?.startShimmer()

        val jsonObjectRequest = object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        categoriesArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val categorylist = Category()
                            categorylist.setCategoryId(c.getString("id"))
                            categorylist.setCategoryName(c.getString("name"))

                            categoriesArrayList.add(categorylist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_ST_CATEGORY + "(CategoryId, CategoryName) " +
                                        "VALUES(?, ?)",
                                arrayOf(c.getString("id"), c.getString("name"))
                            )

                            if (!categoriesArrayList.isEmpty()) {
                                setcategorySpinnerData(categoriesArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerCategoryData", e.toString())
                        shimmerLayout1?.visibility=View.GONE
                        shimmerLayout1?.stopShimmer()
                    }
                    shimmerLayout1?.visibility=View.GONE
                    shimmerLayout1?.stopShimmer()

                }, ErrorListener { error ->
                    shimmerLayout1?.visibility=View.GONE
                    shimmerLayout1?.stopShimmer()
                    //Log.d("loadServerCategoryData", error.toString())
                    //Log.d("loadServerCategoryData", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 200000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }


    private fun loadServerGroupData() {
        val hitURL = AppController.BaseUrl + "core/setting/group-configuration/list/by/class-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID

        shimmerLayout?.visibility=View.VISIBLE
        shimmerLayout?.startShimmer()
        val jsonObjectRequest = object :
            JsonObjectRequest(
                Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        groupsArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val obj = c.getJSONObject("groupObject")

                            val grouplist = Group()
                            grouplist.setGroupId(obj.getString("id"))
                            grouplist.setGroupName(obj.getString("name"))

                            groupsArrayList.add(grouplist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_GROUP + "(GroupId, GroupName) " +
                                        "VALUES(?, ?)",
                                arrayOf(
                                    obj.getString("id"),
                                    obj.getString("name")
                                )
                            )

                            if (!groupsArrayList.isEmpty()) {
                                setgroupSpinnerData(groupsArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Log.d("loadServerGroupData", e.toString())
                        shimmerLayout?.visibility=View.GONE
                        shimmerLayout?.stopShimmer()
                    }
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                }, ErrorListener { error ->
                   // Log.d("loadServerGroupData", error.toString())
                    shimmerLayout?.visibility=View.GONE
                    shimmerLayout?.stopShimmer()

                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }

        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        jsonObjectRequest.retryPolicy = policy
        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    fun dataSendToServer() {
        val hitURL = AppController.BaseUrl + "student/list/save?registrationType=" + std_id_type + "&access_token=" + accessToken

        viewDialog?.showDialog()

        val jsonObj = JSONObject()
        val jsonArray = JSONArray()
        val arrayItem = JSONObject()
        try {
            arrayItem.put("academicSession", "2017-2018")
            arrayItem.put("studentDOB", "1800-01-01")
            arrayItem.put("bloodGroup", "---")
            arrayItem.put("customStudentId", studentCustom)
            arrayItem.put("fatherName", fname)
            arrayItem.put("motherName", mname)
            arrayItem.put("studentGender", selectGender)
            arrayItem.put("studentName", name)
            arrayItem.put("studentReligion", selectReligion)
            arrayItem.put("studentRoll", rollNo)
            arrayItem.put("guardianMobile", phone)

            jsonArray.put(arrayItem)

            jsonObj.put("academicYear", selectAcademic)
            jsonObj.put("classConfigId", localSectionID)//117556
            jsonObj.put("groupId", localGroupID)
            jsonObj.put("studentCategoryId", localCategoryID)
            jsonObj.put("studentRequestHelpers", jsonArray)


        } catch (e: JSONException) {
            e.printStackTrace()
        }

        jObjPost = jsonObj.toString()

        val postRequest = object : CustomRequest(
            Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val messageType = response.getInt("msgType")

                    if (messageType == 1) {
                        viewDialog?.hideDialog()

                        showPopup()

                        if (selectstdIdType == "Custom") {
                            inputStudentCustomID?.text?.clear()
                        }


                    } else {
                        Toast.makeText(requireContext(), "Registration fail!!", Toast.LENGTH_LONG).show()

                        viewDialog?.hideDialog()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                viewDialog?.hideDialog()
            },

            ErrorListener { volleyError ->
               // Log.i("Volley net error:", volleyError.toString())
                viewDialog?.hideDialog()

                if (activity !== null) {

                if (mStatusCode == 409) {
                    activity?.let {
                        Toasty.error(
                            it,
                            "Student ID Already Exists",
                            Toast.LENGTH_SHORT,
                            true
                        ).show()
                    };

                } else {
                    activity?.let {
                        Toasty.error(
                            it,
                            "Student ID Already Exists",
                            Toast.LENGTH_SHORT,
                            true
                        ).show()
                    };
                }
            }

            }
        ) {
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                Log.i("response network .....", response.toString())
                mStatusCode = response?.statusCode!!
                return super.parseNetworkResponse(response)
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }


            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }


    fun showPopup() {

        val back: Button
        val txt: TextView

        myDialog?.setContentView(R.layout.sucessfull_alert_layout)
        back = myDialog?.findViewById(R.id.back) as Button
        txt = myDialog?.findViewById(R.id.tvItemSelected1)!!
        txt.text = "Student Registration Successfully Completed !"

        myDialog?.setCancelable(false)

        back.setOnClickListener {

            myDialog?.dismiss()

            inputStudentName?.text?.clear()
            inputFatherName?.text?.clear()
            inputMotherName?.text?.clear()
            inputPhoneNo?.text?.clear()
            inputStudentRoll?.text?.clear()
            inputPhoneNo?.error = null
            spinner_gender?.setSelection(0)
            spinner_religion?.setSelection(0)
            inputPhoneNo?.clearFocus()
        }

        myDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }
    companion object {
        private val TAG = "FragmentSEnrollmentForm"
    }
}

