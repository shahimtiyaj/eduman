package com.netizen.eduman

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.RemarkUpdateAdapter
import com.netizen.eduman.FragmentRemarkUpdateSelection.Companion.localExamID
import com.netizen.eduman.FragmentRemarkUpdateSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_STUDENT_RESULT
import com.netizen.eduman.model.Exam
import com.netizen.eduman.model.ResultSummary
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class FragmentRemarkUpdate : Fragment() {

    private var btn_remarks_update: Button? = null

    internal var sectionsArrayList = ArrayList<Section>()
    internal var examArrayList = ArrayList<Exam>()

    private var jObjPost: String? = null
    lateinit var v: View

    private var recyclerView: RecyclerView? = null
    private var adapter: RemarkUpdateAdapter? = null
    private var remarksSummaryArrayList: ArrayList<ResultSummary>? = null
    private var mLayoutManager: LinearLayoutManager? = null
    private var total_found: TextView? = null
    private var txt_back: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    internal var ok: TextView? = null
    internal var no: TextView? = null
    private var myDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.remarks_update_recyler_list_view, container, false)

        initializeViews()
        recyclerViewInit()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "remark_update_details")
        editor.apply()

        if (!AppController.instance?.isNetworkAvailable()!!) {
            AppController.instance?.noInternetConnection()
        } else {
            getRemarksUpdateList()
        }

        return v
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
        activity?.title="Remarks Update"
    }


    fun Alert(ctx: Context, icon: Int, title: String, message: String) {
        AlertDialog.Builder(ctx)
            .setIcon(icon)
            .setTitle(title)
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton("OK") { _, _ ->
                activity?.title=null
                val allstdListFragment = TabFragmentRAU()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            }
            .setNegativeButton("Cancel") { _, _ ->
                activity?.title=null
                val allstdListFragment = TabFragmentRAU()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()
            }.show()
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }
        myDialog = Dialog(requireActivity())

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        btn_remarks_update = v.findViewById<View>(R.id.btn_remarks_all_update) as Button
        total_found = v.findViewById<View>(R.id.total_remarks_update_found) as TextView

        btn_remarks_update?.setOnClickListener {
            try {

                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                remarksSummaryArrayList = dao?.getAllRemarksStudentUpdate()

                if (remarksSummaryArrayList?.isNotEmpty()!!) {
                    remarksdataSendToServer()
                } else {
                    activity?.let { Toasty.error(it, "Checked Remarks Student!", Toast.LENGTH_SHORT, true).show() };
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = TabFragmentRAU()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*
   recycler view initialization method
   */
    fun recyclerViewInit() {
        // Initialize item list
        remarksSummaryArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v.findViewById<View>(R.id.all_remarks_update_recyler_view_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = RemarkUpdateAdapter(activity!!, remarksSummaryArrayList!!)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator()
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    private fun loadAllremarksListUpdate() {
        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        remarksSummaryArrayList = dao?.getAllRemarksStudentForUpdate()
        if (!remarksSummaryArrayList?.isEmpty()!!) {
            adapter = RemarkUpdateAdapter(context!!, remarksSummaryArrayList!!)
            recyclerView?.setHasFixedSize(true)
            val mLayoutManager = LinearLayoutManager(context)
            recyclerView?.layoutManager = mLayoutManager
            recyclerView?.adapter = adapter
            adapter?.notifyDataSetChanged()
        } else if (remarksSummaryArrayList?.isEmpty()!!) {
            Alert(activity!!, R.drawable.ic_not_found, "Failure", "Sorry No Data Found !")
        }
        dao?.close()
    }


    fun getRemarksUpdateList() {

        val hitURL =
            AppController.BaseUrl + "exam/section-wise/remarks/find/by/exam-config-id?access_token=" + accessToken + "&classConfigId=" + localSectionID + "&examConfigId=" + localExamID
        viewDialog?.showDialog()

        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    // Getting JSON Array node
                    val getData = response.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        DAO.executeSQL(
                            "INSERT OR REPLACE INTO " + TABLE_STUDENT_RESULT + "(identificationId, customStudentId, studentRoll, " +
                                    "studentName, totalMarks, letterGrade, gradingPoint, remarkId, remarks, remarksTitle) " +
                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            arrayOf(
                                c.getString("identificationId"),
                                c.getString("customStudentId"),
                                c.getString("studentRoll"),
                                c.getString("studentName"),
                                c.getString("totalMarks"),
                                c.getString("letterGrade"),
                                c.getString("gpa"),
                                c.getString("remarkId"),
                                c.getString("remarks"),
                                c.getString("remarksTitle")
                            )
                        )

                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                //Notify adapter if data change
                adapter?.notifyDataSetChanged()

                loadAllremarksListUpdate()

                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                total_found?.text = dao?.totalRemarksUpdateStudent().toString()
                dao?.close()
                viewDialog?.hideDialog()

            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun localRemarkUpdateList() {
        remarksSummaryArrayList = ArrayList()
        try {

            val dao = AppController.instance?.let { DAO(it) }
            dao?.open()
            val resultSummaries = dao?.getAllRemarksStudentUpdate()

            val jsonArray = JSONArray()

            for (resultSummary in resultSummaries!!) {
                val jsonObj = JSONObject()

                jsonObj.put("examConfigId", localExamID)//62
                jsonObj.put("identificationId", resultSummary.result_std_identification)
                jsonObj.put("remarkId", resultSummary.result_std_remarks_id)
                jsonObj.put("remarks", resultSummary.result_std_remarks_des)
                jsonObj.put("remarksTitle", resultSummary.result_std_remarks_title)
                jsonArray.put(jsonObj)
            }

            jObjPost = jsonArray.toString()
            Log.d("Json", jObjPost)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun showPopup() {
        val ok: Button

        val txt: TextView

        myDialog?.setContentView(R.layout.sucessfull_alert_layout)
        ok = myDialog?.findViewById(R.id.back) as Button

        txt = myDialog?.findViewById(R.id.tvItemSelected1)!!

        txt.setText("Result Remarks Successfully Updated !")

        myDialog?.setCancelable(false)

        ok.setOnClickListener {
            myDialog?.dismiss()

            val allstdListFragment = TabFragmentRAU()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
            fragmentTransaction.commit()
        }

        myDialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }


    fun remarksdataSendToServer() {

        viewDialog?.showDialog()

        val hitURL = AppController.BaseUrl + "exam/remarks/all/update?access_token=" + accessToken

        localRemarkUpdateList()

        val postRequest = object : CustomRequest(
            Request.Method.PUT, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Remarks update Response: $response")

                try {
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        viewDialog?.hideDialog()
                        showPopup()

                    } else {
                        activity?.let {
                            AppController.instance?.Alert(
                                it,
                                R.drawable.ic_error_black_24dp,
                                "Remarks ",
                                "Opps!! Fail to Update."
                            )
                        }

                        viewDialog?.hideDialog()

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()

                    viewDialog?.hideDialog()
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("Volley error:", volleyError.toString())
                Log.d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                if (volleyError is NetworkError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (volleyError is ServerError) {
                    Toast.makeText(
                        activity,
                        "The server could not be found. Please try again after some time!!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is AuthFailureError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (volleyError is ParseError) {
                    Toast.makeText(activity, "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT)
                        .show()

                } else if (volleyError is NoConnectionError) {
                    Toast.makeText(
                        activity,
                        "Cannot connect to Internet...Please check your connection!",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (volleyError is TimeoutError) {
                    Toast.makeText(
                        activity,
                        "Connection TimeOut! Please check your internet connection",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }) {


            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jObjPost == null) null else jObjPost!!.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jObjPost,
                        "utf-8"
                    )
                    return null
                }

            }

            /**
             * Passing some request headers
             */
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }

        }


        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
        viewDialog?.hideDialog()
    }

    companion object {
        private val TAG = "FragmentRemarkUpdate"
    }
}
