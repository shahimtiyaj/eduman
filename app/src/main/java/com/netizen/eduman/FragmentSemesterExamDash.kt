package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO

class FragmentSemesterExamDash : Fragment() {

    private var markscardId: CardView? = null
    private var examProcesscardId: CardView? = null
    private var resultcardId: CardView? = null
    private var remarks_cardId: CardView? = null
    private var v: View? = null
    private var txt_back: TextView? = null

    private var txt_scl_name: TextView? = null
    private var txt_inst_id: TextView? = null
    private var txt_academic_year: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.semester_exam_dash_final_layout, container, false)

        markscardId = v?.findViewById<View>(R.id.markscardId) as CardView
        examProcesscardId = v?.findViewById<View>(R.id.examProcesscardId) as CardView
        resultcardId = v?.findViewById<View>(R.id.resultcardId) as CardView
        remarks_cardId = v?.findViewById<View>(R.id.remarks_cardId) as CardView

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        val sharedPreferences = activity?.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putString("now", "semester_exam_dashboard")
        editor?.apply()

        txt_scl_name = v?.findViewById<View>(R.id.school_name) as TextView
        txt_inst_id = v?.findViewById<View>(R.id.school_id) as TextView
        txt_academic_year = v?.findViewById<View>(R.id.academic_year) as TextView


        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val inst_info = dao?.getIntituteInfo()

        txt_scl_name?.setText(inst_info?.getuserinstituteName())
        txt_inst_id?.setText("  Institute ID        :  " + inst_info?.getinstituteId())
        txt_academic_year?.setText("Academic Year  :  " + inst_info?.getacademic_year())

        OnclickView()
        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title="Semester Exam"
    }

    fun OnclickView() {
        markscardId?.setOnClickListener {
            try {
                activity?.title=null
                val da = DAO(activity!!)
                da.open()
                da.deleteInputMarkSectionData()
                da.close()
                val tabFragmentMIU = TabFragmentMIU()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, tabFragmentMIU)
                fragmentManager?.executePendingTransactions()
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        examProcesscardId?.setOnClickListener {
            activity?.title=null
            val tabFragmentExmP = TabFragmentExmP()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, tabFragmentExmP)
            fragmentTransaction.commit()
        }

        resultcardId?.setOnClickListener {
            activity?.title=null
            val fragmentStudentResult = FragmentSemesterExamReportDash()
            //val fragmentStudentResult = FragmentStudentResultSelection()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, fragmentStudentResult)
            fragmentTransaction.commit()
        }

        remarks_cardId?.setOnClickListener {
            activity?.title=null
            val tabFragmentRAU = TabFragmentRAU()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, tabFragmentRAU)
            fragmentTransaction.commit()
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null
                val allstdListFragment = FragmentDashboard()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
}