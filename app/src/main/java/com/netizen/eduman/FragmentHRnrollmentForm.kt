package com.netizen.eduman

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.facebook.shimmer.ShimmerFrameLayout
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.db.DBHelper.Companion.TABLE_HR_DESIG
import com.netizen.eduman.model.Designation
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.util.*

class FragmentHRnrollmentForm : Fragment(), AdapterView.OnItemSelectedListener {
    private var btn_reg_hr: Button? = null
    private var mStatusCode: Int = 0

    private var spinner_category_hr: Spinner? = null
    private var spinner_designation_hr: Spinner? = null
    private var spinner_hr_id: Spinner? = null
    private var spinner_gender_hr: Spinner? = null
    private var spinner_religion_hr: Spinner? = null

    private var inputHRCustomID: EditText? = null
    private var inputHRName: EditText? = null
    private var inputHRPhone: EditText? = null
    private var custom_id_hr_head: TextView? = null

    private var hrCustom: String? = null
    private var hrname: String? = null
    private var phone_hr: String? = null

    private var selectCategory_hr: String? = null
    private var selectIdType_hr: String? = null
    private var selectGender_hr: String? = null
    private var selectReligion_hr: String? = null
    private var selectDesignation_hr: String? = null
    private var id_type_hr: String? = null
    private var localDesignationID: String? = null

    private var linearLayout: LinearLayout? = null
    private var linearLayout0: LinearLayout? = null

    internal var designationArrayList = ArrayList<Designation>()
    internal var sectionsArrayList = ArrayList<Section>()

    private var jsonObjectPost: String? = null

    private var v: View? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences
    private var txt_back: TextView? = null

    var viewDialog: ViewDialog? = null

    internal var ok: TextView? = null
    internal var no: TextView? = null

    private var myDialog: Dialog? = null
    private var shimmerLayout: ShimmerFrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.hr_enlistment_layout, container, false)
        shimmerLayout = v?.findViewById(R.id.shimmer_layout_id)

        initializeViews()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "hr_reg")
        editor.apply()

        return v
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        activity?.title = "Enlistment Form"
    }

    override fun onPause() {
        super.onPause()
        shimmerLayout?.stopShimmer()
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }
        myDialog = Dialog(requireActivity())

        sharedpreferences = activity!!.getSharedPreferences(MyPREFERENCES, 0)
        accessToken = sharedpreferences.getString("accessToken", null)

        inputHRCustomID = v?.findViewById<View>(R.id.et_hr_custom_id) as EditText
        inputHRName = v?.findViewById<View>(R.id.et_hr_name) as EditText
        inputHRPhone = v?.findViewById<View>(R.id.et_hr_mobile) as EditText
        custom_id_hr_head = v?.findViewById<View>(R.id.hr_head) as TextView

        inputHRPhone?.addTextChangedListener(MyTextWatcher(inputHRPhone!!))
        linearLayout = v?.findViewById(R.id.linear5) as LinearLayout
        linearLayout0 = v?.findViewById(R.id.linear4) as LinearLayout

        btn_reg_hr = v?.findViewById<View>(R.id.btn_hr_reg_submit) as Button

        spinner_gender_hr = v?.findViewById<View>(R.id.spinner_gender_hr) as Spinner
        spinner_gender_hr?.onItemSelectedListener = this

        spinner_religion_hr = v?.findViewById<View>(R.id.spinner_religion_hr) as Spinner
        spinner_religion_hr?.onItemSelectedListener = this

        spinner_category_hr = v?.findViewById<View>(R.id.spinner_category_hr) as Spinner
        spinner_category_hr?.onItemSelectedListener = this

        spinner_designation_hr = v?.findViewById<View>(R.id.spinner_designation_hr) as Spinner
        spinner_designation_hr?.onItemSelectedListener = this

        spinner_hr_id = v?.findViewById<View>(R.id.spinner_hr_id_type) as Spinner
        spinner_hr_id?.onItemSelectedListener = this

        btn_reg_hr?.setOnClickListener {
            try {
                if (!AppController.instance?.isNetworkAvailable()!!) {
                    AppController.instance?.noInternetConnection()
                } else {
                    submitRegistration()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title = null

                val allstdListFragment = FragmentHRDashboard()
                val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
                fragmentTransaction?.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction?.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun dataSendToServer() {
        val hitURL: String = if (selectIdType_hr == "Custom") {
            AppController.BaseUrl + "/staff/basic/save/custom-id?registrationType=" + id_type_hr + "&access_token=" + accessToken
        } else {
            AppController.BaseUrl + "/staff/basic/save?access_token=" + accessToken
        }

        viewDialog?.showDialog()

        val jsonArray = JSONArray()
        val arrayItem = JSONObject()
        try {

            arrayItem.put("category", selectCategory_hr)
            arrayItem.put("customId", hrCustom)
            arrayItem.put("designationId", localDesignationID)
            arrayItem.put("gender", selectGender_hr)
            arrayItem.put("staffMobile1", phone_hr)
            arrayItem.put("staffName", hrname)
            arrayItem.put("staffReligion", selectReligion_hr)

            jsonArray.put(arrayItem)

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        jsonObjectPost = jsonArray.toString()

        val postRequest = object : CustomRequest(
            Method.POST, hitURL, null,
            Response.Listener { response ->
                Log.d(TAG, "Register Response: $response")

                try {
                    val messageType = response.getInt("msgType")
                    if (messageType == 1) {
                        viewDialog?.hideDialog()

                        showPopup()

                        if (selectIdType_hr == "Custom") {
                            inputHRCustomID?.text?.clear()
                        }

                    } else {
                        Toast.makeText(activity, "Registration fail!!", Toast.LENGTH_LONG).show()
                        viewDialog?.hideDialog()

                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }
            },
            Response.ErrorListener { volleyError ->
                //Log.i("Volley error:", volleyError.toString())
                // Log.d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                if (activity != null) {

                    if (mStatusCode == 409) {
                        activity?.let {
                            Toasty.error(
                                it,
                                "HR ID Already Exists",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }
                    } else {
                        activity?.let {
                            Toasty.error(
                                it,
                                "HR ID Already Exists",
                                Toast.LENGTH_SHORT,
                                true
                            ).show()
                        }
                    }

                    if (volleyError is NetworkError) {
                        Toast.makeText(
                            activity,
                            "Cannot connect to Internet...Please check your connection!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (volleyError is ServerError) {
                        Toast.makeText(
                            activity,
                            "The server could not be found. Please try again after some time!!",
                            Toast.LENGTH_SHORT
                        ).show()

                    } else if (volleyError is AuthFailureError) {
                        Toast.makeText(
                            activity,
                            "Cannot connect to Internet...Please check your connection!",
                            Toast.LENGTH_SHORT
                        ).show()


                    } else if (volleyError is ParseError) {
                        Toast.makeText(
                            activity,
                            "Parsing error! Please try again after some time!!",
                            Toast.LENGTH_SHORT
                        )
                            .show()

                    } else if (volleyError is NoConnectionError) {
                        Toast.makeText(
                            activity,
                            "Cannot connect to Internet...Please check your connection!",
                            Toast.LENGTH_SHORT
                        ).show()

                    } else if (volleyError is TimeoutError) {
                        Toast.makeText(
                            activity,
                            "Connection TimeOut! Please check your internet connection",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                }

            }) {

            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject>? {
                Log.i("response network .....", response.toString())
                mStatusCode = response?.statusCode!!

                return super.parseNetworkResponse(response)
            }

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                try {
                    return if (jsonObjectPost == null) null else jsonObjectPost!!.toByteArray(
                        charset("utf-8")
                    )
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf(
                        "Unsupported Encoding while trying to get the bytes of %s using %s",
                        jsonObjectPost,
                        "utf-8"
                    )
                    return null
                }
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                //pass application id and rest api key for security
                headers["Content-Type"] = "application/json"

                return headers
            }
        }


        val socketTimeout = 1800000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    private fun loadServerHRDesignationData() {

        val hitURL = AppController.BaseUrl + "core/setting/list/by-type-id?access_token=" + accessToken + "&typeId=2601"
        shimmerLayout?.visibility = View.VISIBLE
        shimmerLayout?.startShimmer()

        val jsonObjectRequest = object :
            JsonObjectRequest(Method.GET, hitURL, null,
                Response.Listener { response ->
                    try {
                        designationArrayList = ArrayList()
                        val getData = response.getJSONArray("item")

                        for (i in 0 until getData.length()) {
                            if (getData.isNull(i))
                                continue
                            val c = getData.getJSONObject(i)
                            val designationlist = Designation()
                            designationlist.setDesignationId(c.getString("id"))
                            designationlist.setDesignationName(c.getString("name"))

                            designationArrayList.add(designationlist)

                            DAO.executeSQL(
                                "INSERT OR REPLACE INTO " + TABLE_HR_DESIG + "(DesigId, DesigName) " +
                                        "VALUES(?, ?)", arrayOf(c.getString("id"), c.getString("name")))

                            if (!designationArrayList.isEmpty()) {
                                setdesignationSpinnerData(designationArrayList)
                            }
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        shimmerLayout?.visibility = View.GONE
                        shimmerLayout?.stopShimmer()
                    }

                    shimmerLayout?.visibility = View.GONE
                    shimmerLayout?.stopShimmer()

                }, Response.ErrorListener {
                    shimmerLayout?.visibility = View.GONE
                    shimmerLayout?.stopShimmer()
                    //Log.d("loadServerHRCategory", "Server error")
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["typeId"] = "2601"
                return headers
            }
        }

        jsonObjectRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(jsonObjectRequest) }
    }

    private fun setdesignationSpinnerData(designationArrayList: ArrayList<Designation>?) {

        try {
            val hrdesignation = ArrayList<String>()
            hrdesignation.add(resources.getString(R.string.select_designation))
            for (i in designationArrayList?.indices!!) {
                designationArrayList[i].getDesignationName()?.let { hrdesignation.add(it) }
            }

            val categoryAdapter = ArrayAdapter(requireContext(), R.layout.spinner_item, hrdesignation)
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_designation_hr?.adapter = categoryAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("Designation :", "Spinner data")

        }

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try {

            val spinner = parent as Spinner

            if (spinner.id == R.id.spinner_category_hr) {
                selectCategory_hr = spinner_category_hr?.selectedItem.toString()
                if (selectCategory_hr == resources.getString(R.string.select_category)) {
                    selectCategory_hr = ""
                } else {
                    selectCategory_hr = parent.getItemAtPosition(position).toString()

                    loadServerHRDesignationData()
                }
            }

            if (spinner.id == R.id.spinner_designation_hr) {
                selectDesignation_hr = spinner_designation_hr!!.selectedItem.toString()
                if (selectDesignation_hr == resources.getString(R.string.select_designation)) {
                    selectDesignation_hr = ""
                } else {
                    selectDesignation_hr = parent.getItemAtPosition(position).toString()
                    val da = activity?.let { DAO(it) }
                    da?.open()
                    localDesignationID = da?.GetDesignationID(selectDesignation_hr!!)
                    da?.close()
                }
            }

            if (spinner.id == R.id.spinner_gender_hr) {
                selectGender_hr = spinner_gender_hr?.selectedItem.toString()
                if (selectGender_hr == resources.getString(R.string.select_gender)) {
                    selectGender_hr = ""
                } else {

                    selectGender_hr = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_religion_hr) {
                selectReligion_hr = spinner_religion_hr?.selectedItem.toString()
                if (selectReligion_hr == resources.getString(R.string.select_religion)) {
                    selectReligion_hr = ""
                } else {
                    selectReligion_hr = parent.getItemAtPosition(position).toString()
                }
            }

            if (spinner.id == R.id.spinner_hr_id_type) {
                selectIdType_hr = spinner_hr_id?.selectedItem.toString()
                if (selectIdType_hr == resources.getString(R.string.select_hr_id_type)) {
                    selectIdType_hr = ""
                } else {
                    selectIdType_hr = parent.getItemAtPosition(position).toString()
                    if (selectIdType_hr == "Custom") {
                        id_type_hr = 1.toString()
                        linearLayout?.visibility = View.VISIBLE;
                        linearLayout0?.visibility = View.VISIBLE;
                        inputHRCustomID?.visibility = View.VISIBLE
                        custom_id_hr_head?.visibility = View.VISIBLE

                    } else {
                        id_type_hr = 0.toString()
                        linearLayout?.visibility = View.GONE;
                        linearLayout0?.visibility = View.GONE;
                        inputHRCustomID?.visibility = View.GONE
                        custom_id_hr_head?.visibility = View.GONE
                    }
                }
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } finally {
            Log.d("HR reg  ", "Spinner data")
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private fun submitRegistration() {

        if (!validateHRIDTYPE()) {
            return
        }

        if (selectIdType_hr == "Custom") {

            if (!validateTypeID()) {
                return
            }
        }

        if (!validateName()) {
            return
        }

        if (!validateHRGender()) {
            return
        }
        if (!validateHRReligion()) {
            return
        }

        if (!validateHRCategory()) {
            return
        }

        if (!validateHRDesignation()) {
            return
        }

        if (!validatePhone()) {
            return
        }

        sentRegistrationInformation()
    }

    private fun sentRegistrationInformation() {
        hrCustom = inputHRCustomID!!.text.toString()
        hrname = inputHRName!!.text.toString()
        phone_hr = inputHRPhone!!.text.toString()

        if (!inputHRPhone?.text.toString().startsWith("01", 0, true)) {
            inputHRPhone?.error = getString(R.string.err_msg_phone_invalid)
            requestFocus(inputHRPhone!!)
        } else {
            dataSendToServer()
        }

    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            if (!inputHRPhone?.text.toString().startsWith("01", 0, true)) {
                inputHRPhone?.error = getString(R.string.err_msg_phone_invalid)
                requestFocus(inputHRPhone!!)
            }
        }

        override fun afterTextChanged(editable: Editable) {
            when (view.id) {
                R.id.et_hr_name -> validateName()
            }
        }
    }

    private fun validatePhone(): Boolean {
        if (inputHRPhone == null || inputHRPhone!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputHRPhone?.error = getString(R.string.err_msg_phone)
            requestFocus(inputHRPhone!!)
            return false
        } else if (inputHRPhone?.length() != 11) {
            inputHRPhone?.error = getString(R.string.err_msg_phone_invalid)
            requestFocus(inputHRPhone!!)
            return false
        }

        return true
    }

    private fun validateTypeID(): Boolean {
        if (inputHRCustomID == null || inputHRCustomID?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputHRCustomID?.setError(getString(R.string.err_mssg_id))
            inputHRCustomID?.let { requestFocus(it) }
            return false
        }
        return true
    }

    private fun validateName(): Boolean {
        if (inputHRName == null || inputHRName?.getText().toString().trim { it <= ' ' }.isEmpty()) {
            inputHRName?.setError(getString(R.string.err_mssg_name))
            inputHRName?.let { requestFocus(it) }
            return false
        }
        return true
    }

    private fun validateHRIDTYPE(): Boolean {
        if (selectIdType_hr == null || selectIdType_hr?.trim { it <= ' ' }!!.isEmpty() || selectIdType_hr.equals(
                "Select HR ID Type"
            )
        ) {
            activity?.let {
                Toasty.error(it, "Select HR ID Type", Toast.LENGTH_SHORT, true).show()
            };

            return false
        }
        return true
    }


    private fun validateHRGender(): Boolean {
        if (selectGender_hr == null || selectGender_hr?.trim { it <= ' ' }!!.isEmpty() || selectGender_hr.equals(
                "Select Gender"
            )
        ) {
            activity?.let { Toasty.error(it, "Select Gender", Toast.LENGTH_SHORT, true).show() };
            return false
        }
        return true
    }

    private fun validateHRReligion(): Boolean {
        if (selectReligion_hr == null || selectReligion_hr?.trim { it <= ' ' }!!.isEmpty() || selectReligion_hr.equals(
                "Select Religion"
            )
        ) {
            activity?.let { Toasty.error(it, "Select Religion", Toast.LENGTH_SHORT, true).show() };
            return false
        }
        return true
    }

    private fun validateHRCategory(): Boolean {
        if (selectCategory_hr == null || selectCategory_hr?.trim { it <= ' ' }!!.isEmpty() || selectCategory_hr.equals(
                "Select Category"
            )
        ) {
            activity?.let { Toasty.error(it, "Select Category", Toast.LENGTH_SHORT, true).show() };
            return false
        }
        return true
    }

    private fun validateHRDesignation(): Boolean {
        if (selectDesignation_hr == null || selectDesignation_hr?.trim { it <= ' ' }!!.isEmpty() || selectDesignation_hr.equals(
                "Select Designation"
            )
        ) {
            activity?.let {
                Toasty.error(it, "Select Designation", Toast.LENGTH_SHORT, true).show()
            }
            return false
        }
        return true
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    fun showPopup() {

        val back: Button

        val txt: TextView

        myDialog?.setContentView(R.layout.sucessfull_alert_layout)

        back = myDialog?.findViewById(R.id.back) as Button

        txt = myDialog?.findViewById(R.id.tvItemSelected1)!!

        txt.text = "HR Registration Successfully Completed !"

        myDialog?.setCancelable(false)

        back.setOnClickListener {

            myDialog?.dismiss()

            val stdtabFragment = FragmentHRnrollmentForm()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)

            fragmentTransaction.detach(stdtabFragment)
            fragmentTransaction.attach(stdtabFragment)
            fragmentTransaction.commit()
        }

        myDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }

    companion object {

        private val TAG = "FragmentHRnrollmentForm"
    }
}
