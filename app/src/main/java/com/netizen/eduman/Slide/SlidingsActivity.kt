package com.netizen.eduman.Slide

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.widget.Button
import com.netizen.eduman.Adapter.SlidingImage_Adapter
import com.netizen.eduman.LoginActivity
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.ImageModel
import com.viewpagerindicator.CirclePageIndicator
import java.util.*
import kotlin.collections.ArrayList

class SlidingsActivity : Activity() {
    private var imageModelArrayList: ArrayList<ImageModel>? = null

    private val myImageList = intArrayOf(R.drawable.slide_1, R.drawable.slide_1, R.drawable.slide_2, R.drawable.slide_2)

    private val TextTitleList = arrayOf<String>("STUDENT", "HR MANAGEMENT", "ATTENDANCE", "SEMESTER EXAM")

    private val TextDesList = arrayOf<String>(
        "Through Student module, you can enroll a new student instantly and also can get section-wise student report.",
        "By using HR Management module, you are able to enlist new HR to the institute and can have HR reports easily.",
        "Effortlessly, you can take Student Attendance through this module. And also can get Student & HR Attendance report.",
        "Here you are able to have exam mark inserting and updating options, numerous result process options, and also exam results information."
    )

    private var btn_go_to_login: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slide)

        imageModelArrayList = ArrayList()

        imageModelArrayList = populateList()
        btn_go_to_login = findViewById<Button>(R.id.btn_got_to_login)

        btn_go_to_login?.setOnClickListener {
            try {

                val intent = Intent(applicationContext, LoginActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.left_in, R.anim.left_out)
                finish()
                val dao = AppController.instance?.let { DAO(it) }
                dao?.open()
                dao?.UpdateSlideCheck()
                dao?.close()


            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }
        }

        init()
    }

    private fun populateList(): ArrayList<ImageModel> {

        val list = ArrayList<ImageModel>()

        for (i in 0..3) {
            val imageModel = ImageModel()
            imageModel.setImage_drawables(myImageList[i])
            imageModel.setTxt1(TextTitleList[i])
            imageModel.setTxt2(TextDesList[i])
            list.add(imageModel)
        }

        return list
    }

    private fun init() {

        mPager = findViewById<ViewPager>(R.id.pager)
        mPager?.adapter = SlidingImage_Adapter(this@SlidingsActivity, this.imageModelArrayList!!)

        val indicator = findViewById<CirclePageIndicator>(R.id.indicator)

        indicator.setViewPager(mPager)

        val density = resources.displayMetrics.density

        //Set circle indicator radius
        indicator.setRadius(5 * density)

        NUM_PAGES = imageModelArrayList!!.size

        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            mPager?.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position
            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })

    }

    companion object {
        private var mPager: ViewPager? = null
        private var currentPage = 0
        private var NUM_PAGES = 0
    }
}
