package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.InputMark
import com.netizen.eduman.model.MarkDistribution
import android.text.InputFilter
import com.netizen.eduman.model.DecimalDigitsInputFilter
import com.netizen.eduman.model.InputFilterMinMax

class InputMarkAdapter(private val mContext: Context, private val inputMarksList: List<InputMark>) :
    RecyclerView.Adapter<InputMarkAdapter.InputMarkViewHolder>() {
    private val markDistributionList: List<MarkDistribution>? = null

    inner class InputMarkViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var input_mark_st_id: TextView
        var input_mark_st_roll: TextView
        var input_mark_st_name: TextView

        var input_mark_exam_name1: TextView
        var input_mark_exam_name2: TextView
        var input_mark_exam_name3: TextView
        var input_mark_exam_name4: TextView
        var input_mark_exam_name5: TextView
        var input_mark_exam_name6: TextView

        var input_exam_mark1: EditText
        var input_exam_mark2: EditText
        var input_exam_mark3: EditText
        var input_exam_mark4: EditText
        var input_exam_mark5: EditText
        var input_exam_mark6: EditText

        var input_view3: View
        var input_view4: View
        var input_view5: View
        var input_view6: View
        var input_view7: View
        var input_view8: View

        init {

            input_mark_st_id = view.findViewById<View>(R.id.input_mark_std_id) as TextView
            input_mark_st_roll = view.findViewById<View>(R.id.input_mark_std_roll) as TextView
            input_mark_st_name = view.findViewById<View>(R.id.input_mark_std_name) as TextView

            input_view3 = view.findViewById<View>(R.id.myview3) as View
            input_view4 = view.findViewById<View>(R.id.myview4) as View
            input_view5 = view.findViewById<View>(R.id.myview5) as View
            input_view6 = view.findViewById<View>(R.id.myview6) as View
            input_view7 = view.findViewById<View>(R.id.myview7) as View
            input_view8 = view.findViewById<View>(R.id.myview8) as View

            input_mark_exam_name1 = view.findViewById<View>(R.id.input_mark_examCodeTitle_1) as TextView //Exam Name
            input_mark_exam_name2 = view.findViewById<View>(R.id.input_mark_examCodeTitle_2) as TextView //Exam Name
            input_mark_exam_name3 = view.findViewById<View>(R.id.input_mark_examCodeTitle_3) as TextView // Exam Name
            input_mark_exam_name4 = view.findViewById<View>(R.id.input_mark_examCodeTitle_4) as TextView // Exam Name
            input_mark_exam_name5 = view.findViewById<View>(R.id.input_mark_examCodeTitle_5) as TextView
            input_mark_exam_name6 = view.findViewById<View>(R.id.input_mark_examCodeTitle_6) as TextView

            input_exam_mark1 = view.findViewById<View>(R.id.input_et_mark_std_examCode1) as EditText // Exam Mark
            input_exam_mark2 = view.findViewById<View>(R.id.input_et_mark_std_examCode2) as EditText // Exam Mark
            input_exam_mark3 = view.findViewById<View>(R.id.input_et_mark_std_examCode3) as EditText // Exam Mark
            input_exam_mark4 = view.findViewById<View>(R.id.input_et_mark_std_examCode4) as EditText
            input_exam_mark5 = view.findViewById<View>(R.id.input_et_mark_std_examCode5) as EditText
            input_exam_mark6 = view.findViewById<View>(R.id.input_et_mark_std_examCode6) as EditText


            var dao = AppController.instance?.let { DAO(it) }
            dao?.open()

            input_exam_mark1.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    try {

                        var defaultId: String?
                        var totlaMark: String?

                        dao = AppController.instance?.let { DAO(it) }

                        val inputMarks = dao?.getALLMarkDistrubutionScale()

                        for (mark in inputMarks!!) {
                            defaultId = mark.defaultId
                            totlaMark = defaultId?.let { dao?.GetTotalMark(it) }.toString()
                            if (defaultId == "1") {
                                input_exam_mark1.filters = arrayOf(InputFilterMinMax(0.0F, totlaMark.toFloat()), DecimalDigitsInputFilter())
                            }
                        }

                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                }

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode1 = input_exam_mark1.text.toString()

                    val shortCode1 = inputMarksList[adapterPosition].shortCode1

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id
                    dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.UpdateExamCode1(shortCode1!!, idNo!!)
                    dao?.close()

                }
            })

            input_exam_mark2.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    try {

                        var defaultId: String?
                        var totlaMark: String?

                        dao = AppController.instance?.let { DAO(it) }

                        val inputMarks = dao?.getALLMarkDistrubutionScale()

                        for (mark in inputMarks!!) {

                            defaultId = mark.defaultId
                            totlaMark = defaultId?.let { dao?.GetTotalMark(it) }.toString()

                            if (defaultId == "2") {
                                input_exam_mark2.filters = arrayOf(InputFilterMinMax(0.0F, totlaMark!!.toFloat()), DecimalDigitsInputFilter())
                            }
                        }

                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }
                }

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode2 = input_exam_mark2.text.toString()

                    val shortCode2 = inputMarksList[adapterPosition].shortCode2

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id

                    dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.UpdateExamCode2(shortCode2!!, idNo!!)
                    dao?.close()

                }
            })


            input_exam_mark3.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    try {

                        var defaultId: String?
                        var totlaMark: String?

                        dao = AppController.instance?.let { DAO(it) }

                        val inputMarks = dao?.getALLMarkDistrubutionScale()

                        for (mark in inputMarks!!) {

                            defaultId = mark.defaultId
                            totlaMark = defaultId?.let { dao?.GetTotalMark(it) }.toString()

                            if (defaultId == "3") {
                                input_exam_mark3.filters = arrayOf(InputFilterMinMax(0.0F, totlaMark!!.toFloat()), DecimalDigitsInputFilter())
                            }
                        }

                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }
                }

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode3 = input_exam_mark3.text.toString()

                    val shortCode3 = inputMarksList[adapterPosition].shortCode3

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id

                    dao = AppController.instance?.let { DAO(it) }
                    dao?.UpdateExamCode3(shortCode3!!, idNo!!)

                }
            })


            input_exam_mark4.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    try {

                        var defaultId: String?
                        var totlaMark: String

                        dao = AppController.instance?.let { DAO(it) }
                        val inputMarks = dao?.getALLMarkDistrubutionScale()

                        for (mark in inputMarks!!) {

                            defaultId = mark.defaultId
                            totlaMark = defaultId?.let { dao?.GetTotalMark(it) }.toString()
                            if (defaultId == "4") {
                                input_exam_mark4.filters = arrayOf(InputFilterMinMax(0.0F, totlaMark.toFloat()), DecimalDigitsInputFilter())
                            }
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }
                }

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode4 = input_exam_mark4.text.toString()

                    val shortCode4 = inputMarksList[adapterPosition].shortCode4

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id

                    dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.UpdateExamCode4(shortCode4!!, idNo!!)

                }
            })

            input_exam_mark5.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    try {

                        var defaultId: String?
                        var totlaMark: String

                        dao = AppController.instance?.let { DAO(it) }
                        val inputMarks = dao?.getALLMarkDistrubutionScale()

                        for (mark in inputMarks!!) {

                            defaultId = mark.defaultId
                            totlaMark = defaultId?.let { dao?.GetTotalMark(it) }.toString()

                            if (defaultId == "5") {
                                input_exam_mark5.filters = arrayOf(InputFilterMinMax(0.0F, totlaMark.toFloat()), DecimalDigitsInputFilter())
                            }

                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                }

                override fun afterTextChanged(s: Editable) {


                    inputMarksList[adapterPosition].shortCode5 = input_exam_mark5.text.toString()

                    val shortCode5 = inputMarksList[adapterPosition].shortCode5

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id

                    dao = AppController.instance?.let { DAO(it) }
                    dao?.open()
                    dao?.UpdateExamCode5(shortCode5!!, idNo!!)
                }
            })

            input_exam_mark6.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                    try {

                        var defaultId: String?
                        var totlaMark: String

                        dao = AppController.instance?.let { DAO(it) }
                        val inputMarks = dao?.getALLMarkDistrubutionScale()

                        for (mark in inputMarks!!) {

                            defaultId = mark.defaultId
                            totlaMark = defaultId?.let { dao?.GetTotalMark(it) }.toString()
                            if (defaultId == "6") {
                                input_exam_mark6.filters = arrayOf(InputFilterMinMax(0.0F, totlaMark.toFloat()), DecimalDigitsInputFilter())
                            }

                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                    }

                }

                override fun afterTextChanged(s: Editable) {

                    inputMarksList[adapterPosition].shortCode6 = input_exam_mark6.text.toString()

                    val shortCode6 = inputMarksList[adapterPosition].shortCode6

                    val idNo = inputMarksList[adapterPosition].in_mark_st_identification_id

                    dao = AppController.instance?.let { DAO(it) }
                    dao?.UpdateExamCode6(shortCode6!!, idNo!!)

                }
            })

            dao?.close()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputMarkAdapter.InputMarkViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.mark_input_item_row, parent, false)
        return InputMarkViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: InputMarkAdapter.InputMarkViewHolder, position: Int) {
        val inputMark = inputMarksList[position]

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        var defaultId: String?
        var examName: String

        val inputMarks = dao?.getALLMarkDistrubutionScale()

        for (mark in inputMarks!!) {

            defaultId = mark.defaultId
            examName = defaultId?.let { dao.GetExamName(it) }.toString()

            if (defaultId == "1") {
                holder.input_mark_exam_name1.visibility = View.VISIBLE
                holder.input_exam_mark1.visibility = View.VISIBLE
                holder.input_view3.visibility = View.VISIBLE

                holder.input_mark_exam_name1.text = examName

                if (inputMark.shortCode1 == "0.0") {
                    holder.input_exam_mark1.setText("")
                } else {
                    holder.input_exam_mark1.setText(inputMark.shortCode1)
                }
            }

            if (defaultId == "2") {
                holder.input_mark_exam_name2.visibility = View.VISIBLE
                holder.input_exam_mark2.visibility = View.VISIBLE
                holder.input_view4.visibility = View.VISIBLE

                holder.input_mark_exam_name2.text = examName
                if (inputMark.shortCode2 == "0.0") {
                    holder.input_exam_mark2.setText("")
                } else {
                    holder.input_exam_mark2.setText(inputMark.shortCode2)
                }

            } else if (defaultId == "3") {
                holder.input_mark_exam_name3.visibility = View.VISIBLE
                holder.input_exam_mark3.visibility = View.VISIBLE
                holder.input_view5.visibility = View.VISIBLE


                holder.input_mark_exam_name3.text = examName
                if (inputMark.shortCode3 == "0.0") {
                    holder.input_exam_mark3.setText("")
                } else {
                    holder.input_exam_mark3.setText(inputMark.shortCode3)
                }

            } else if (defaultId == "4") {
                holder.input_mark_exam_name4.visibility = View.VISIBLE
                holder.input_exam_mark4.visibility = View.VISIBLE
                holder.input_view6.visibility = View.VISIBLE

                holder.input_mark_exam_name4.text = examName
                if (inputMark.shortCode4 == "0.0") {
                    holder.input_exam_mark4.setText("")
                } else {
                    holder.input_exam_mark4.setText(inputMark.shortCode4)
                }
            } else if (defaultId == "5") {
                holder.input_mark_exam_name5.visibility = View.VISIBLE
                holder.input_exam_mark5.visibility = View.VISIBLE
                holder.input_view7.visibility = View.VISIBLE

                holder.input_mark_exam_name5.text = examName
                if (inputMark.shortCode5 == "0.0") {
                    holder.input_exam_mark5.setText("")
                } else {
                    holder.input_exam_mark5.setText(inputMark.shortCode5)
                }
            } else if (defaultId == "6") {
                holder.input_mark_exam_name6.visibility = View.VISIBLE
                holder.input_exam_mark6.visibility = View.VISIBLE
                holder.input_view8.visibility = View.VISIBLE

                holder.input_mark_exam_name6.text = examName

                if (inputMark.shortCode6 == "0.0") {
                    holder.input_exam_mark6.setText("")
                } else {
                    holder.input_exam_mark6.setText(inputMark.shortCode6)
                }
            }
        }

        holder.input_mark_st_id.text = inputMark.in_mark_st_id
        holder.input_mark_st_roll.text = inputMark.in_mark_st_roll
        holder.input_mark_st_name.text = inputMark.in_mark_st_name

        dao.close()
    }

    override fun getItemCount(): Int {
        return inputMarksList.size
    }
}
