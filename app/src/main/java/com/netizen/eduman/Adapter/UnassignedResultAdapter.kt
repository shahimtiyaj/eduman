package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.model.UnAssignedResult
import java.util.*


class UnassignedResultAdapter(private val mContext: Context, private val unassignResultList: List<UnAssignedResult>) :
    RecyclerView.Adapter<UnassignedResultAdapter.UnassignedResultViewHolder>(),
    Filterable {
    private var filteredunAssignedResultList: List<UnAssignedResult>? = null

    inner class UnassignedResultViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var section_val: TextView
        var total_subject_val: TextView
        var inputted_subject_val: TextView
        var remaining_subject_val: TextView
        var remaining_sub_name_val: TextView

        init {
            section_val = view.findViewById<View>(R.id.section_val) as TextView
            total_subject_val = view.findViewById<View>(R.id.total_subject_val) as TextView
            inputted_subject_val = view.findViewById<View>(R.id.inputted_subject_val) as TextView
            remaining_subject_val = view.findViewById<View>(R.id.remaining_subject_val) as TextView
            remaining_sub_name_val = view.findViewById<View>(R.id.remaining_sub_name_val) as TextView
        }
    }

    init {
        this.filteredunAssignedResultList = unassignResultList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UnassignedResultViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.unassigned_marks_item_row, parent, false)
        return UnassignedResultViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UnassignedResultViewHolder, position: Int) {

        val unAssignedResult = filteredunAssignedResultList!![position]

        holder.section_val.text = unAssignedResult.sectionName
        holder.total_subject_val.text = unAssignedResult.numOfTotalSubjects
        holder.inputted_subject_val.text = unAssignedResult.numOfMarkInputtedSubjects
        holder.remaining_subject_val.text = unAssignedResult.numOfMarkUnInputtedSubjects
        holder.remaining_sub_name_val.text = unAssignedResult.markUnInputtedSubjects
    }

    override fun getItemCount(): Int {
        return filteredunAssignedResultList!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredunAssignedResultList = unassignResultList
                } else {
                    val filteredList = ArrayList<UnAssignedResult>()
                    for (row in unassignResultList) {
                        // here we are looking for name or phone number match
                        if (row.sectionName!!.toLowerCase().contains(charString.toLowerCase()) || row.sectionName!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredunAssignedResultList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredunAssignedResultList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredunAssignedResultList = filterResults.values as ArrayList<UnAssignedResult>
                notifyDataSetChanged()
            }
        }
    }

}

