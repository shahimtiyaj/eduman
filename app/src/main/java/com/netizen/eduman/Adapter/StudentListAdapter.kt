package com.netizen.eduman.Adapter

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.bumptech.glide.Glide
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController.Companion.instance
import com.netizen.eduman.model.Student
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*


class StudentListAdapter(val studentList: ArrayList<Student>?, val mContext: Context) :
    RecyclerView.Adapter<StudentListAdapter.StudentViewHolder>(), Filterable {

    private var filteredresultstudentList: List<Student>? = null

    inner class StudentViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var st_id: TextView
        var st_roll: TextView
        var st_name: TextView
        var group: TextView
        var category: TextView
        var gender: TextView
        var religion: TextView
        var fname: TextView
        var mname: TextView
        var blood: TextView
        var dob: TextView
        var mobile: TextView

       var mProfileImageView: CircleImageView

        init {
            st_id = view.findViewById<View>(R.id.student_id) as TextView
            st_roll = view.findViewById<View>(R.id.student_roll) as TextView
            st_name = view.findViewById<View>(R.id.student_name) as TextView
            group = view.findViewById<View>(R.id.student_group) as TextView
            category = view.findViewById<View>(R.id.student_category) as TextView
            gender = view.findViewById<View>(R.id.student_gender) as TextView
            religion = view.findViewById<View>(R.id.student_religion) as TextView
            fname = view.findViewById<View>(R.id.student_father_name) as TextView
            mname = view.findViewById<View>(R.id.student_mother_name) as TextView
            blood = view.findViewById<View>(R.id.student_blood_group) as TextView
            dob = view.findViewById<View>(R.id.student_blood_dob) as TextView
            mobile = view.findViewById<View>(R.id.student_mobile) as TextView

            mProfileImageView = view.findViewById<View>(R.id.student_image_id) as CircleImageView
        }
    }

    init {
        this.filteredresultstudentList = studentList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.student_list_item, parent, false)
        // Return a new holder instance
        return StudentViewHolder(itemView)
    }


    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {

        try {

            // Get the item model based on position
            val student = filteredresultstudentList?.get(position)

            // Set item views based on our views and data model
            holder.st_id.text = student?.getstudent_id()
            holder.st_roll.text = student?.getstudent_roll()
            holder.st_name.text = student?.getstudent_name()
            holder.group.text = student?.getgroup()
            holder.category.text = student?.getcategory()
            holder.gender.text = student?.getgender()
            holder.religion.text = student?.getreligion()
            holder.fname.text = student?.getfather_name()
            holder.mname.text = student?.getmother_name()
            if (student?.getblood_group() == "null") {
                holder.blood.text = "---"
            } else {
                holder.blood.text = student?.getblood_group()
            }

            when {
                student?.getdate_of_birth().equals("01/01/1800") -> holder.dob.text = "---"
                student?.getdate_of_birth().equals("null") -> holder.dob.text = "---"
                else -> holder.dob.text = student?.getdate_of_birth()
            }

            holder.mobile.text = student?.getmobile_no()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val theByteArray = Base64.getDecoder().decode(student?.getstudent_image())
                Glide.with(instance).load(theByteArray)
                    .asBitmap()
                    .into(holder.mProfileImageView)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }


    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredresultstudentList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredresultstudentList = studentList
                } else {
                    val filteredList = ArrayList<Student>()
                    for (row in studentList!!) {
                        // here we are looking for name or phone number match
                        if (row.student_id!!.toLowerCase().contains(charString.toLowerCase()) || row.getstudent_name()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredresultstudentList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredresultstudentList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredresultstudentList = filterResults.values as ArrayList<Student>
                notifyDataSetChanged()
            }
        }
    }
}
