package com.netizen.eduman.Adapter

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.netizen.eduman.FragmentTotalStudent
import com.netizen.eduman.R
import com.netizen.eduman.model.AttendanceSummary

class AttendanceListAdapter(
    private val mContext: Context,
    private val attendancesSummaryList: List<AttendanceSummary>
) : RecyclerView.Adapter<AttendanceListAdapter.AttendanceSummaryViewHolder>() {

    inner class AttendanceSummaryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var attendance_sumrry_sec: TextView
        var attendance_summary_total_std: TextView
        var attendance_summary_present: TextView
        var attendance_summary_absent: TextView
        var attendance_summary_leave: TextView
        var attendance_summary_present_per: TextView
        var attendance_summary_absent_per: TextView
        var attendance_summary_leave_per: TextView
        var attendance_total_next: TextView
        var attendance_present_next: TextView
        var attendance_absent_next: TextView
        var attendance_leave_next: TextView


        var checkBox: CheckBox? = null

        init {
            attendance_sumrry_sec = view.findViewById(R.id.attendance_summary_sec) as TextView
            attendance_summary_total_std = view.findViewById(R.id.attendance_summary_total_std) as TextView
            attendance_summary_present = view.findViewById(R.id.attendance_summary_present_std) as TextView
            attendance_summary_absent = view.findViewById(R.id.attendance_summary_absent_std) as TextView
            attendance_summary_leave = view.findViewById(R.id.attendance_summary_leave_std) as TextView

            attendance_summary_present_per = view.findViewById(R.id.attendance_summary_present_std_per) as TextView
            attendance_summary_absent_per = view.findViewById(R.id.attendance_summary_absent_std_per) as TextView
            attendance_summary_leave_per = view.findViewById(R.id.attendance_summary_leave_std_per) as TextView

            attendance_total_next = view.findViewById(R.id.next1) as TextView
            attendance_present_next = view.findViewById(R.id.next2) as TextView
            attendance_absent_next = view.findViewById(R.id.next3) as TextView
            attendance_leave_next = view.findViewById(R.id.next4) as TextView

        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AttendanceSummaryViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.attendance_list_summary_item_row, parent, false)
        return AttendanceSummaryViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: AttendanceSummaryViewHolder, position: Int) {

        val attendanceSummary = attendancesSummaryList[position]

        holder.attendance_sumrry_sec.text = attendanceSummary.atd_smry_section
        holder.attendance_summary_total_std.text = attendanceSummary.atd_smry_total_student
        holder.attendance_summary_present.text = attendanceSummary.atd_smry_present
        holder.attendance_summary_absent.text = attendanceSummary.atd_smry_absent
        holder.attendance_summary_leave.text = attendanceSummary.atd_smry_leave

        holder.attendance_summary_present_per.text = "(" + attendanceSummary.atd_smry_total_present_per + "%)"
        holder.attendance_summary_absent_per.text = "(" + attendanceSummary.atd_smry_total_absent_per + "%)"
        holder.attendance_summary_leave_per.text = "(" + attendanceSummary.atd_smry_total_leave_per + "%)"

        holder.attendance_total_next.setOnClickListener { v ->
            val activity = v.context as AppCompatActivity
            val myFragment = FragmentTotalStudent()

            val bundle = Bundle()
            bundle.putString("status", 5.toString())
            bundle.putString("header", "Student Attendance List")
            bundle.putString("section", attendancesSummaryList[position].atd_smry_section)
            bundle.putString("section_id", attendancesSummaryList[position].atd_smry_section_id)
            bundle.putString("section_date", attendancesSummaryList[position].atd_smry_date)

            myFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, myFragment)
                .addToBackStack(null).commit()
        }

        holder.attendance_present_next.setOnClickListener { v ->
            val activity = v.context as AppCompatActivity
            val myFragment = FragmentTotalStudent()

            val bundle = Bundle()
            bundle.putString("status", 1.toString())
            bundle.putString("header", "Present Student List")
            bundle.putString("section", attendancesSummaryList[position].atd_smry_section)
            bundle.putString("section_id", attendancesSummaryList[position].atd_smry_section_id)
            bundle.putString("section_date", attendancesSummaryList[position].atd_smry_date)

            myFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, myFragment)
                .addToBackStack(null).commit()
        }

        holder.attendance_absent_next.setOnClickListener { v ->
            val activity = v.context as AppCompatActivity
            val myFragment = FragmentTotalStudent()

            val bundle = Bundle()
            bundle.putString("status", 2.toString())
            bundle.putString("header", "Absent Student List")
            bundle.putString("section", attendancesSummaryList[position].atd_smry_section)
            bundle.putString("section_id", attendancesSummaryList[position].atd_smry_section_id)
            bundle.putString("section_date", attendancesSummaryList[position].atd_smry_date)

            myFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, myFragment)
                .addToBackStack(null).commit()
        }

        holder.attendance_leave_next.setOnClickListener { v ->
            val activity = v.context as AppCompatActivity
            val myFragment = FragmentTotalStudent()

            val bundle = Bundle()
            bundle.putString("status", 3.toString())
            bundle.putString("header", "Leave Student List")
            bundle.putString("section", attendancesSummaryList[position].atd_smry_section)
            bundle.putString("section_id", attendancesSummaryList[position].atd_smry_section_id)
            bundle.putString("section_date", attendancesSummaryList[position].atd_smry_date)

            myFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, myFragment)
                .addToBackStack(null).commit()
        }
    }

    override fun getItemCount(): Int {
        return attendancesSummaryList.size
    }
}
