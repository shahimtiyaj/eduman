package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.netizen.eduman.R
import com.netizen.eduman.model.HRPresent
import java.util.*

class HRPresentAdapter(private val mContext: Context, private val hrPresentList: List<HRPresent>) :
    RecyclerView.Adapter<HRPresentAdapter.HRPresentViewHolder>(), Filterable {
    private var filteredhrPresentList: List<HRPresent>? = null


    inner class HRPresentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var hr_p_id: TextView
        var hr_p_name: TextView
        var hr_p_category: TextView
        var hr_p_designation: TextView
        var hr_p_mobile_no: TextView
        var hr_p_in_time: TextView
        var hr__present_in_time_h: TextView

        var intime_view: View
        var linear: LinearLayout

        init {
            hr_p_id = view.findViewById<View>(R.id.hr__present_id) as TextView
            hr_p_name = view.findViewById<View>(R.id.hr__present_name) as TextView
            hr_p_category = view.findViewById<View>(R.id.hr__present_category) as TextView
            hr_p_designation = view.findViewById<View>(R.id.hr__present_designation) as TextView
            hr_p_mobile_no = view.findViewById<View>(R.id.hr__present_mobile_no) as TextView
            hr_p_in_time = view.findViewById<View>(R.id.hr__present_in_time) as TextView
            hr__present_in_time_h = view.findViewById<View>(R.id.hr__present_in_time_head) as TextView

            intime_view = view.findViewById<View>(R.id.view_intime) as View
            linear = view.findViewById<LinearLayout>(R.id.linear_intime) as LinearLayout

        }
    }

    init {
        this.filteredhrPresentList = hrPresentList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HRPresentAdapter.HRPresentViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.hr_attendance_present_list_row, parent, false)
        return HRPresentViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: HRPresentAdapter.HRPresentViewHolder, position: Int) {
        val hr = filteredhrPresentList?.get(position)

        holder.hr_p_id.text = hr?.getHr_p_id()
        holder.hr_p_name.text = hr?.getHr_p_name()
        if(hr?.getHr_p_category()=="null"){
            holder.hr_p_category.text="---"
        }
        else{
            holder.hr_p_category.text = hr?.getHr_p_category()
        }

        holder.hr_p_designation.text = hr?.getHr_p_designation()
        holder.hr_p_mobile_no.text = hr?.getHr_p_mobile()

        if(hr?.getHr_p_in_time()=="null"){
            holder.intime_view.visibility = View.GONE
            holder.linear.visibility = View.GONE
        }
        else{
            holder.hr_p_in_time.text = hr?.getHr_p_in_time()
        }

    }

    override fun getItemCount(): Int {
        return filteredhrPresentList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredhrPresentList = hrPresentList
                } else {
                    val filteredList = ArrayList<HRPresent>()
                    for (row in hrPresentList) {
                        // here we are looking for name or phone number match
                        if (row.getHr_p_id()!!.toLowerCase().contains(charString.toLowerCase()) || row.getHr_p_name()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredhrPresentList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredhrPresentList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredhrPresentList = filterResults.values as ArrayList<HRPresent>
                notifyDataSetChanged()
            }
        }
    }

}