package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO
import com.netizen.eduman.model.ResultSummary

class RemarksAssignAdapter(private val mContext: Context, private val remarksSummaryList: List<ResultSummary>) :
    RecyclerView.Adapter<RemarksAssignAdapter.RemarksViewHolder>() {

    inner class RemarksViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var remarks_std_id: TextView
        var remarks_std_roll: TextView
        var remarks_std_name: TextView
        var remarks_std_total_marks: TextView
        var remarks_std_grade: TextView
        var remarks_std_gpa: TextView
        var remarks_check: CheckBox


        init {
            remarks_std_id = view.findViewById<View>(R.id.remarks_assign_std_id) as TextView
            remarks_std_roll = view.findViewById<View>(R.id.remarks_assign_std_roll) as TextView
            remarks_std_name = view.findViewById<View>(R.id.remarks_assign_std_name) as TextView
            remarks_std_total_marks = view.findViewById<View>(R.id.remarks_assign_std_total_marks) as TextView
            remarks_std_grade = view.findViewById<View>(R.id.remarks_assign_std_grade) as TextView
            remarks_std_gpa = view.findViewById<View>(R.id.remarks_assign_std_gpa) as TextView
            remarks_check = view.findViewById<View>(R.id.remarks_assign_check) as CheckBox

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RemarksAssignAdapter.RemarksViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.remarks_assign_item_row, parent, false)
        return RemarksViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: RemarksAssignAdapter.RemarksViewHolder, position: Int) {

        val remarks = remarksSummaryList[position]

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val student_identification = remarks.result_std_identification

        holder.remarks_std_id.text = remarks.result_std_id
        holder.remarks_std_roll.text = remarks.result_std_roll
        holder.remarks_std_name.text = remarks.result_std_name
        holder.remarks_std_total_marks.text = remarks.result_std_total_mark
        holder.remarks_std_grade.text = remarks.result_std_grade
        holder.remarks_std_gpa.text = remarks.result_std_gpa

        remarksSummaryList.get(position).getSelected().let { holder.remarks_check.isChecked = it }
        holder.remarks_check.tag = position

        holder.remarks_check.setOnClickListener {

            if (remarksSummaryList.get(position).getSelected()) {
                remarksSummaryList.get(position).setSelected(false)
                student_identification?.let { it1 -> dao?.UpdateRemarkStudentFlag(0, it1) }

            } else {
                remarksSummaryList.get(position).setSelected(true)
                student_identification?.let { it1 -> dao?.UpdateRemarkStudentFlag(1, it1) }

            }
        }
    }

    override fun getItemCount(): Int {
        return remarksSummaryList.size
    }
}

