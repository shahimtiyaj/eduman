package com.netizen.eduman.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.TextView
import com.netizen.eduman.R
import com.netizen.eduman.model.TotalStudent
import java.util.*

class TotalStudentAdapter(private val mContext: Context, private val totalStudentList: List<TotalStudent>) :
    RecyclerView.Adapter<TotalStudentAdapter.TotalStudentViewHolder>(), Filterable{
    private var filteredtotalStudentList: List<TotalStudent>? = null

    inner class TotalStudentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var std_id: TextView
        var std_roll: TextView
        var std_name: TextView
        var std_gender: TextView
        var std_mobile: TextView
        var std_status: TextView
        var linearLayout: LinearLayout? = null
        var atd_view: View

        init {
            std_id = view.findViewById<View>(R.id.total_st_poup_std_id) as TextView
            std_roll = view.findViewById<View>(R.id.total_st_poup_std_roll_no) as TextView
            std_name = view.findViewById<View>(R.id.total_st_poup_std_name) as TextView
            std_gender = view.findViewById<View>(R.id.total_st_poup_std_gender) as TextView
            std_mobile = view.findViewById<View>(R.id.total_st_poup_std_phone_no) as TextView
            std_status = view.findViewById<View>(R.id.total_st_poup_std_status) as TextView

            linearLayout = view.findViewById(R.id.atd_status) as LinearLayout
            atd_view = view.findViewById<View>(R.id.vv) as View


        }
    }
    init {
        this.filteredtotalStudentList = totalStudentList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TotalStudentViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.total_student_item_row_st_summry, parent, false)
        return TotalStudentViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TotalStudentViewHolder, position: Int) {
        // Get the item model based on position
        val student = filteredtotalStudentList?.get(position)

        // Set item views based on our views and data model
        holder.std_id.text = student?.getStd_id()
        holder.std_roll.text = student?.getStd_roll()
        holder.std_name.text = student?.getStd_name()
        holder.std_gender.text = student?.getStd_gender()
        holder.std_mobile.text = student?.getStd_mobile_no()
        if(student?.getStd_status()=="null"){
            holder.atd_view.visibility = View.GONE
            holder.linearLayout?.visibility = View.GONE
        }
        else{
            holder.atd_view.visibility = View.VISIBLE
            holder.linearLayout?.visibility = View.VISIBLE

            holder.std_status.text = student?.getStd_status()
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredtotalStudentList?.size!!
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filteredtotalStudentList = totalStudentList
                } else {
                    val filteredList = ArrayList<TotalStudent>()
                    for (row in totalStudentList) {
                        // here we are looking for name or phone number match
                        if (row.getStd_id()!!.toLowerCase().contains(charString.toLowerCase()) || row.getStd_name()!!.contains(
                                charSequence
                            )
                        ) {
                            filteredList.add(row)
                        }
                    }

                    filteredtotalStudentList = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredtotalStudentList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredtotalStudentList = filterResults.values as ArrayList<TotalStudent>
                notifyDataSetChanged()
            }
        }
    }
}
