package com.netizen.eduman

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.*
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.HRPresentAdapter
import com.netizen.eduman.FragmentHRPresentAtdSelection.Companion.hr_p_attndance_smry_date
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.HRPresent
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.text.SimpleDateFormat
import java.util.*

class FragmentHRPresent : Fragment(), View.OnClickListener {

    internal var hrPresentsArrayList = ArrayList<HRPresent>()
    private var recyclerView: RecyclerView? = null
    private var adapter: HRPresentAdapter? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var hr_attendance_p_date: String? = null
    lateinit var v: View
    private var hr_attndance_smry_date: EditText? = null
    private var fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null

    private var total_found_present_hr: TextView? = null
    private var txt_back: TextView? = null
    private var searchView: SearchView? = null
    var dataload = false

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    private var swipeRefresh: SwipeRefreshLayout? = null
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        v = inflater.inflate(R.layout.hr_attendance_recyler_list_view, container, false)

        initializeViews()
        setHasOptionsMenu(true)
        recyclerViewInit()

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "hr_present")
        editor.apply()

        if (!AppController.instance?.isNetworkAvailable()!!) {
            AppController.instance?.noInternetConnection()
        } else {
            GetTotalStaffAttendancePresentSummary()
        }

        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title = "Report"
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser && !dataload) {

            if (!AppController.instance?.isNetworkAvailable()!!) {
                AppController.instance?.noInternetConnection()
            } else {
                GetTotalStaffAttendancePresentSummary()
            }

            dataload = true;
        }
    }

    private fun initializeViews() {

        viewDialog = activity?.let { ViewDialog(it) }
        mHandler = Handler()
        swipeRefresh = v.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout

        swipeRefresh?.setOnRefreshListener {
            mRunnable = Runnable {
                adapter?.notifyDataSetChanged()

                hrPresentsArrayList

                swipeRefresh?.isRefreshing = false
            }

            mHandler.postDelayed(
                mRunnable, 50
            )
        }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)

        total_found_present_hr = v.findViewById<View>(R.id.total_found_present_hr) as TextView

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title = null

                val allstdListFragment = FragmentHRPresentAtdSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        return if (id == R.id.action_search) {

            true
        } else super.onOptionsItemSelected(item)

    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

        // Associate searchable configuration with the SearchView
        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu.findItem(R.id.action_search).actionView as SearchView

        searchView?.queryHint =
            Html.fromHtml("<font color = #5459EC>" + resources.getString(R.string.search_hr) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.colorEduman)

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })

    }


    override fun onClick(v: View) {
        if (v === hr_attndance_smry_date) {
            fromDatePickerDialog?.show()
        }
    }

    fun recyclerViewInit() {
        // Initialize item list
        hrPresentsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v.findViewById<View>(R.id.hr_attdnce_smry_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = HRPresentAdapter(activity!!, hrPresentsArrayList)
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    fun GetTotalStaffAttendancePresentSummary() {

        hr_attendance_p_date = hr_attndance_smry_date?.text.toString() //send to server

        val hitURL =
            AppController.BaseUrl + "staff/attendance/date-based/by/attendance-status?attendanceDate=" + hr_p_attndance_smry_date + "&attendanceStatus=1&access_token=" + accessToken

        viewDialog?.showDialog()

        //JSON Post Request --------------------------------
        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")
                try {
                    val getData = response.getJSONArray("item")
                    for (i in 0 until getData.length()) {
                        if (getData.isNull(i))
                            continue
                        val c = getData.getJSONObject(i)
                        val hrPresent = HRPresent()
                        hrPresent.setHr_p_id(c.getString("customStaffId"))
                        hrPresent.setHr_p_name(c.getString("staffName"))
                        hrPresent.setHr_p_category(c.getString("staffCategory"))
                        hrPresent.setHr_p_designation(c.getString("staffDesignation"))
                        hrPresent.setHr_p_mobile(c.getString("staffMobileNo"))
                        hrPresent.setHr_p_in_time(c.getString("stringInTime"))
                        hrPresentsArrayList.add(hrPresent)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()

                    viewDialog?.hideDialog()
                }

                if (hrPresentsArrayList.isEmpty()) {

                    activity?.let {
                        AppController.instance?.Alert(
                            it,
                            R.drawable.ic_not_found,
                            "Failure",
                            "Sorry! No Data Found !"
                        )
                    }

                    viewDialog?.hideDialog()
                }

                adapter?.notifyDataSetChanged()
                viewDialog?.hideDialog()
                total_found_present_hr?.text = hrPresentsArrayList.size.toString()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
                d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                var message: String? = null
                if (volleyError is NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ServerError) {
                    message = "The server could not be found. Please try again after some time!!"
                } else if (volleyError is AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!"
                } else if (volleyError is ParseError) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (volleyError is TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else {
                    message = volleyError.toString()
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

        }
        val socketTimeout = 500000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        postRequest.setShouldCache(true)
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {

        private val TAG = "FragmentHRPresent"
    }
}
