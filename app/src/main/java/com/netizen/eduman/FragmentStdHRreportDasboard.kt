package com.netizen.eduman

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatDelegate
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.netizen.eduman.app.AppController
import com.netizen.eduman.db.DAO

class FragmentStdHRreportDasboard : Fragment() {

    private var studentReportCard: CardView? = null
    private var hr_report_Card: CardView? = null

    lateinit var v: View
    private var txt_back: TextView? = null
    private var txt_scl_name: TextView? = null
    private var txt_inst_id: TextView? = null
    private var txt_academic_year: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.std_hr_report_dash_board_final, container, false)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        studentReportCard = v.findViewById<View>(R.id.std_atd_report_card) as CardView
        hr_report_Card = v.findViewById<View>(R.id.hr_atd_report_card) as CardView

        val sharedPreferences = activity!!.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("now", "student_hr_report_dash")
        editor.apply()

        txt_scl_name = v.findViewById<View>(R.id.school_name) as TextView
        txt_inst_id = v.findViewById<View>(R.id.school_id) as TextView
        txt_academic_year = v.findViewById<View>(R.id.academic_year) as TextView

        val dao = AppController.instance?.let { DAO(it) }
        dao?.open()
        val inst_info = dao?.getIntituteInfo()

        txt_scl_name?.setText(inst_info?.getuserinstituteName())
        txt_inst_id?.setText("  Institute ID        :  " + inst_info?.getinstituteId())
        txt_academic_year?.setText("Academic Year  :  " + inst_info?.getacademic_year())

        OnclickView()

        return v
    }
    override fun onResume() {
        super.onResume()
        activity?.title="Report"
    }


    fun OnclickView() {
        studentReportCard?.setOnClickListener {
            try {
                activity?.title=null

                val stdtabFragment = FragmentSAttendanceSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)
                fragmentTransaction.commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        hr_report_Card?.setOnClickListener {
            activity?.title=null

            val hrmtabFragment = FragmentHRPresentAtdSelection()
            val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, hrmtabFragment)
            fragmentTransaction.commit()
        }

        txt_back = v.findViewById<View>(R.id.back_text) as TextView

        txt_back?.setOnClickListener {
            try {
                activity?.title=null

                val allstdListFragment = FragmentAttdanceDash()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
