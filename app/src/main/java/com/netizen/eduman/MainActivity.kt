package com.netizen.eduman

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.netizen.eduman.db.DAO
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    internal var close: ImageView? = null
    internal var ok: TextView? = null
    internal var no: TextView? = null

    private var myDialog: Dialog? = null
    var view: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        myDialog = Dialog(this, android.R.style.Theme_DeviceDefault_NoActionBar_Fullscreen)

        val dashboardFragment = FragmentDashboard()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
        fragmentTransaction.commit()

        val navFooter2 = findViewById<View>(R.id.btn_logout)
        navFooter2.setOnClickListener {
            LogOut()
        }

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)

        toggle.drawerArrowDrawable.color = resources.getColor(R.color.colorEduman)

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

    }

    fun LogOut() {
        val logout: Button
        val cencel: Button
        val txtclose: TextView

        myDialog?.setContentView(R.layout.logout_pop_up_final)
        logout = myDialog?.findViewById(R.id.logout) as Button
        cencel = myDialog?.findViewById(R.id.cencel) as Button
        myDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        myDialog?.setCancelable(false)

        logout.setOnClickListener {

            try {

                val da = DAO(applicationContext)
                da.open()
                da.deleteGroupList()
                da.deleteExamList()
                da.deleteSubjectList()
                da.clearInputMark()
                da.clearMarkDistrubution()
                da.deleteSectionData()
                da.deleteSectionResultData()
                da.deleteInputMarkSectionData()
                da.close()

                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        cencel.setOnClickListener { myDialog?.dismiss() }

        txtclose = myDialog?.findViewById(R.id.txtclose)!!
        txtclose.setOnClickListener { myDialog?.dismiss() }
        myDialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog?.show()
    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment.isAdded) return
        super.onAttachFragment(fragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        myDialog?.dismiss()
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else if (!drawer.isDrawerOpen(GravityCompat.START)) {

            val sharedPreferences = getSharedPreferences("back", Context.MODE_PRIVATE)

            when (sharedPreferences.getString("now", "")) {
                "std_reg" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStudentDashboard()
                ).commit()
                "std_all_list" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragGetStudentListSelection()
                ).commit()
                "std_take_atd" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentTakeAtdSelection()
                ).commit()
                "std_take_atd_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentAttdanceDash()
                ).commit()
                "hr_reg" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentHRDashboard()
                ).commit()
                "hr_list" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentHRDashboard()
                ).commit()
                "std_atd_list_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStdHRreportDasboard()
                ).commit()
                "input_mark_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()
                "input_mark_std_details" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    TabFragmentMIU()
                ).commit()
                "update_mark_std_details" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    TabFragmentMIU()
                ).commit()
                "semester_exam_dashboard" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentDashboard()
                ).commit()
                "general_exam_process" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()
                "grand_final_exam_process" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()
                "merit_position_process" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()
                "student_result_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamReportDash()
                ).commit()
                "student_result_details" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStudentResultSelection()
                ).commit()
                "remark_assign_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()
                "remark_assign_details" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    TabFragmentRAU()
                ).commit()
                "remark_update_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()
                "remark_update_details" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    TabFragmentRAU()
                ).commit()
                "hr_present" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentHRPresentAtdSelection()
                ).commit()
                "hr_absent" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentHRPresentAtdSelection()
                ).commit()
                "hr_leave" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentHRPresentAtdSelection()
                ).commit()
                "hr_present_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStdHRreportDasboard()
                ).commit()
                "hr_leave_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStdHRreportDasboard()
                ).commit()
                "hr_absent_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStdHRreportDasboard()
                ).commit()
                "attendance_count" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()

                "std_take_atd_summary" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSAttendanceSelection()
                ).commit()

                "student_dash" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentDashboard()
                ).commit()

                "student_get_list" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentStudentDashboard()
                ).commit()
                "hr_dash" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentDashboard()
                ).commit()
                "std_atd_dash" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentDashboard()
                ).commit()

                "student_hr_report_dash" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentAttdanceDash()
                ).commit()

                "std_atd_present_absent_leave" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSAttendanceList()
                ).commit()

                "semester_exam_report_dashboard" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamDash()
                ).commit()

                "unassigned_mark_exam_selection" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentSemesterExamReportDash()
                ).commit()

                "student_unassigned_result_details" -> supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    FragmentUnassignedExamSelection()
                ).commit()

                "home" -> LogOut()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        try {

            if (id == R.id.nav_home) {
                val dashboardFragment = FragmentDashboard()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, dashboardFragment)
                fragmentTransaction.commit()
            } else if (id == R.id.nav_student) {
                val tabstdFragment = FragmentStudentDashboard()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, tabstdFragment)
                fragmentTransaction.commit()
            } else if (id == R.id.nav_hr_management) {
                val tabhrmFragment = FragmentHRDashboard()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, tabhrmFragment)
                fragmentTransaction.commit()
            } else if (id == R.id.nav_all_attendance) {
                val stdtabFragment = FragmentAttdanceDash()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, stdtabFragment)
                fragmentTransaction.commit()
            } else if (id == R.id.nav_all_semester_exam) {
                val fragmentSemesterExam = FragmentSemesterExamDash()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, fragmentSemesterExam)
                fragmentTransaction.commit()
            }

        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

}
