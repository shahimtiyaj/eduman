package com.netizen.eduman

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.VolleyLog.d
import com.netizen.eduman.Adapter.StudentListAdapter
import com.netizen.eduman.FragGetStudentListSelection.Companion.localSectionID
import com.netizen.eduman.app.AppController
import com.netizen.eduman.model.Section
import com.netizen.eduman.model.Student
import com.netizen.eduman.model.ViewDialog
import com.netizen.eduman.network.CloudRequest
import com.netizen.eduman.network.CustomRequest
import org.json.JSONException
import java.lang.reflect.InvocationTargetException
import java.util.*


class FragmentStudentList : Fragment() {

    private var spinner_get_all_std_list: Spinner? = null
    internal var sectionsArrayList_all_std = ArrayList<Section>()
    private var txt_back: TextView? = null
    private var total_found: TextView? = null

    private var recyclerView: RecyclerView? = null
    private var adapter: StudentListAdapter? = null
    private var studentsArrayList: ArrayList<Student>? = null
    private var mLayoutManager: LinearLayoutManager? = null

    private var v: View? = null
    private var searchView: SearchView? = null
    private var section: String? = null
    private var section_txt: TextView? = null

    val MyPREFERENCES = "MyPrefs"
    var accessToken: String? = null
    lateinit var sharedpreferences: SharedPreferences

    var viewDialog: ViewDialog? = null

    private var swipeRefresh: SwipeRefreshLayout? = null
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.student_recyler_list, container, false)

        initializeViews()
        recyclerViewInit()

        view?.setOnClickListener {
            it.hideKeyboard()
        }

        val sharedPreferences = activity?.getSharedPreferences("back", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        editor?.putString("now", "std_all_list")
        editor?.apply()

        setHasOptionsMenu(true)

        section_txt = v?.findViewById<View>(R.id.school_section) as TextView

        try {
            if (savedInstanceState == null) {
                if (arguments != null) {
                    section = arguments?.getString("section")
                    section_txt?.text = section
                }
            }

            if (!AppController.instance?.isNetworkAvailable()!!) {
                AppController.instance?.noInternetConnection()
            } else {
                GetAllStudentDataWebService()
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: java.lang.NullPointerException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        return v
    }

    override fun onResume() {
        super.onResume()
        activity?.title = "Report"
    }

    private fun View.hideKeyboard() {
        val inputMethodManager = context?.getSystemService(android.content.Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.hideSoftInputFromWindow(this.windowToken, 0)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        return if (id == R.id.action_search) {

            true
        } else super.onOptionsItemSelected(item)

    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)

        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        searchView?.queryHint = Html.fromHtml("<font color = #5459EC>" + resources.getString(R.string.search_student) + "</font>")

        //Applies white color on searchview text
        val id = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_src_text",
            null,
            null
        )
        val textView = searchView?.findViewById(id!!) as TextView
        textView.setTextColor(R.color.colorEduman)

        val searchiconId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_button",
            null,
            null
        )
        val image = searchView?.findViewById(searchiconId!!) as ImageView
        image.setImageResource(R.drawable.ic_search_black_24dp)


        val closeId = searchView?.getContext()?.resources?.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val close = searchView?.findViewById(closeId!!) as ImageView
        close.setImageResource(R.drawable.ic_close_black_24dp)

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView?.maxWidth = Integer.MAX_VALUE

        // listening to search query text change
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                adapter?.filter?.filter(query)
                return false
            }
        })
    }

    private fun initializeViews() {
        viewDialog = activity?.let { ViewDialog(it) }
        mHandler = Handler()
        swipeRefresh = v?.findViewById<View>(R.id.swipe_refresh_layout) as SwipeRefreshLayout
        swipeRefresh?.setOnRefreshListener {
            // Initialize a new Runnable
            mRunnable = Runnable {
                adapter?.notifyDataSetChanged()
                studentsArrayList
                swipeRefresh?.isRefreshing = false
            }

            mHandler.postDelayed(
                mRunnable, 50
            )
        }
        total_found = v?.findViewById<View>(R.id.total_found) as TextView

        txt_back = v?.findViewById<View>(R.id.back_text) as TextView
        txt_back?.setOnClickListener {
            try {
                activity?.title = null

                val allstdListFragment = FragGetStudentListSelection()
                val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment_container, allstdListFragment)
                fragmentTransaction.commit()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        sharedpreferences = activity?.getSharedPreferences(MyPREFERENCES, 0)!!
        accessToken = sharedpreferences.getString("accessToken", null)
    }

    fun recyclerViewInit() {
        // Initialize item list
        studentsArrayList = ArrayList()
        // Lookup the recyclerview in activity layout
        recyclerView = v?.findViewById<View>(R.id.all_student_recyler_list_id) as RecyclerView
        // Create adapter passing in the sample item data
        adapter = activity?.let { StudentListAdapter(studentsArrayList, it) }
        //GridLayoutManager shows items in a grid.
        mLayoutManager = LinearLayoutManager(activity)
        // Set layout manager to position the items
        recyclerView?.layoutManager = mLayoutManager
        // Set the default animator
        recyclerView?.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        // Attach the adapter to the recyclerview to populate items
        recyclerView?.adapter = adapter
    }

    private fun setsectionSpinnerData(sectionArrayList: ArrayList<Section>?) {
        try {
            val section = ArrayList<String>()
            section.add(resources.getString(R.string.select_section))
            for (i in sectionArrayList?.indices!!) {
                sectionArrayList[i].getSectionName()?.let { section.add(it) }
            }

            val sectionAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, section)
            sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_get_all_std_list?.adapter = sectionAdapter
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            Log.d("Student List ", "Section spinner data")
        }
    }

    fun GetAllStudentDataWebService() {

        val hitURL = AppController.BaseUrl + "student/list/by/class-config-id?classConfigId=" + localSectionID + "&access_token=" + accessToken

        viewDialog?.showDialog()

        val postRequest = object : CustomRequest(
            Method.GET, hitURL, null,
            Response.Listener { response ->
                d(TAG, "Server Response:$response")

                try {
                    // Getting JSON Array node
                    val getData = response?.getJSONArray("item")
                    // looping through All nodes
                    for (i in 0 until getData?.length()!!) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)
                        // Get the item model
                        val studentlist = Student()

                        studentlist.setstudent_id(c.getString("customStudentId"))
                        studentlist.setstudent_roll(c.getString("studentRoll"))
                        studentlist.setstudent_name(c.getString("studentName"))
                        studentlist.setgroup(c.getString("groupName"))
                        studentlist.setcategory(c.getString("studentCategoryName"))
                        studentlist.setgender(c.getString("studentGender"))
                        studentlist.setreligion(c.getString("studentReligion"))
                        studentlist.setfather_name(c.getString("fatherName"))
                        studentlist.setmother_name(c.getString("motherName"))
                        studentlist.setblood_group(c.getString("bloodGroup"))
                        studentlist.setdate_of_birth(c.getString("studentDOB"))
                        studentlist.setmobile_no(c.getString("guardianMobile"))
                        if (!c.isNull("pureByteImage")) {
                            studentlist.setstudent_image(c.getString("pureByteImage"))
                        }
                        studentsArrayList?.add(studentlist)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                } catch (e: java.lang.NullPointerException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                    viewDialog?.hideDialog()
                }

                if (studentsArrayList?.isEmpty()!!) {
                    activity?.let {
                        AppController.instance?.Alert(
                            it,
                            R.drawable.ic_not_found,
                            "Failure",
                            "Sorry No Data Found !"
                        )
                    }
                }

                adapter?.notifyDataSetChanged()
                total_found?.text = studentsArrayList?.size.toString()
                viewDialog?.hideDialog()
            },
            Response.ErrorListener { volleyError ->
                volleyError.printStackTrace()
               // d(TAG, "Error: " + volleyError.message)
                viewDialog?.hideDialog()

                if (context !== null) {

                    when (volleyError) {
                        is NetworkError -> {
                            Toast.makeText(
                                context,
                                "No Internet connection !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is ServerError -> {
                            Toast.makeText(
                                context,
                                "The server could not be found.!",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        is AuthFailureError -> {
                            Toast.makeText(
                                context,
                                "No Internet connection !",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                        is ParseError -> {
                            Toast.makeText(
                                context,
                                "Parse Error !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        is TimeoutError -> {
                            Toast.makeText(
                                context,
                                "Connection TimeOut !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Toast.makeText(
                                context,
                                "Sorry No Data Found !",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }
        }
        // Volley socket time its use for loading huge amount data
        val socketTimeout = 1800000//18 Minutes-change to what you want//1800000 milliseconds = 18 minutes
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postRequest.retryPolicy = policy
        //Volley cache true
        postRequest.setShouldCache(true)
        //// Add the request to the RequestQueue. from singleton class
        activity?.let { CloudRequest.getInstance(it).addToRequestQueue(postRequest) }
    }

    companion object {
        private val TAG = "FragmentStudentList"
    }
}

