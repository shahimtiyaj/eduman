package com.netizen.eduman

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatDelegate
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class TabFragmentHPAL : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val x = inflater.inflate(R.layout.fragment_tab, null)
        tabLayout = x.findViewById<View>(R.id.tabs) as TabLayout
        viewPager = x.findViewById<View>(R.id.viewpager) as ViewPager
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        viewPager.adapter = MyAdapter(childFragmentManager)
        viewPager.offscreenPageLimit = 3

        tabLayout.post {

            tabLayout.setupWithViewPager(viewPager)
        }

        return x
    }

    internal inner class MyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return FragmentHRPresent()
                1 -> return FragmentHRAbsent()
                2 -> return FragmentHRLeave()
            }
            return null
        }

        override fun getCount(): Int {

            return int_items
        }

        override fun getPageTitle(position: Int): CharSequence? {

            when (position) {
                0 -> return "Present"
                1 -> return "Absent"
                2 -> return "Leave"
            }
            return null
        }
    }

    companion object {

        lateinit var tabLayout: TabLayout
        lateinit var viewPager: ViewPager
        var int_items = 3
    }

}

