create TABLE [Token] (
    [access_token] nvarchar(250),
    [refresh_token] nvarchar(250),
	[token_type] nvarchar(10),
	PRIMARY KEY ([access_token], [token_type])
);

create TABLE [InstituteInfo] (
    [instituteId] nvarchar(250),
    [instituteName] nvarchar(250),
	[instituteAddress] nvarchar(250),
	[academicYear] nvarchar(250),
	[instituteLogo] nvarchar,
	PRIMARY KEY ([instituteId])
);

create TABLE [UserRole] (
    [RoleAccount] nvarchar(250),
    [RoleAdmin] nvarchar(250),
	[RoleOperator] nvarchar(10),
	[RoleTeacher] nvarchar(10),
	PRIMARY KEY ([RoleAccount], [RoleAdmin], [RoleOperator], [RoleTeacher])
);

create TABLE [Slide] (
    [checkSlide] Integer DEFAULT 0,
    PRIMARY KEY ([checkSlide])
);

create TABLE [Login] (
    [userName] nvarchar(20),
    PRIMARY KEY ([userName])
);

create TABLE [AcademicYear] (
    [academicYearID] nvarchar(20),
	[academicYear] nvarchar(10),
	PRIMARY KEY ([academicYearID], [academicYear])
);

create TABLE [Section] (
    [classConfigId] nvarchar(20),
	[classShiftSection] nvarchar(50),
	[instituteId] nvarchar(20),
	PRIMARY KEY ([classConfigId], [classShiftSection])
);

create TABLE [SectionResult] (
    [classId] nvarchar(20),
	[className] nvarchar(50),
	PRIMARY KEY ([classId], [className])
);

create TABLE [Exam] (
    [examConfigId] nvarchar(20),
    [ExamId] nvarchar(20),
	[ExamName] nvarchar(50),
	PRIMARY KEY ([ExamId])
);

create TABLE [ExamResult] (
    [SectionPosition] nvarchar(20),
    [ExamId] nvarchar(20),
	[ExamName] nvarchar(50),
	PRIMARY KEY ([ExamId], [ExamId])
);

create TABLE [SGroup] (
    [GroupId] nvarchar(20),
	[GroupName] nvarchar(50),
	PRIMARY KEY ([GroupId])
);

create TABLE [InputMarkSubject] (
    [SubjectId] nvarchar(20),
	[SubjectName] nvarchar(50),
	PRIMARY KEY ([SubjectId])
);

create TABLE [HRDesignation] (
    [DesigId] nvarchar(20),
	[DesigName] nvarchar(50),
	PRIMARY KEY ([DesigId])
);

create TABLE [STCategory] (
    [CategoryId] nvarchar(20),
	[CategoryName] nvarchar(50),
	PRIMARY KEY ([CategoryId])
);

create TABLE [Attendance] (
    [periodId] nvarchar(20),
	[periodName] nvarchar(50),
	PRIMARY KEY ([periodId], [periodName])
);

create TABLE [AttendanceSummaryPeriod] (
    [typeId] nvarchar(20),
	[periodName] nvarchar(50),
	PRIMARY KEY ([typeId], [periodName])
);

create TABLE [TakeAttendance] (
    [studentId] nvarchar(20),
    [studentRoll] nvarchar(20),
    [studentName] nvarchar(30),
    [studentGender] nvarchar(10),
    [identificationId] nvarchar(20),
	[checked] default 0,
    PRIMARY KEY ([identificationId])

);

create TABLE [MarkScaleDistribution] (
    [shortCodeName] nvarchar(20),
    [passMark] nvarchar(20),
    [totalMark] nvarchar(30),
    [acceptance] nvarchar(10),
    [defaultId] nvarchar(20),
     PRIMARY KEY ([shortCodeName])

);

create TABLE [MarkInputStudent] (
    [markinputId] nvarchar(20),
    [identificationId] nvarchar(20),
    [customStudentId] nvarchar(20),
    [studentName] nvarchar(20),
    [totalMarks] nvarchar(20),
    [gpa] nvarchar(20),
    [studentRoll] nvarchar(30),
    [shortCode1] nvarchar(30),
    [shortCode2] nvarchar(30),
    [shortCode3] nvarchar(30),
    [shortCode4] nvarchar(30),
    [shortCode5] nvarchar(30),
    [shortCode6] nvarchar(30),
    PRIMARY KEY ([identificationId])
);

create TABLE [StudentResult] (
    [identificationId] nvarchar(20),
    [customStudentId] nvarchar(20),
    [studentRoll] nvarchar(20),
    [studentName] nvarchar(20),
    [totalMarks] nvarchar(20),
    [letterGrade] nvarchar(20),
    [gradingPoint] nvarchar(20),
    [remarkId] nvarchar(20),
    [remarks] nvarchar(20),
    [remarksTitle] nvarchar(20),
    [checked] default 0,
    PRIMARY KEY ([identificationId])
);

create TABLE [RemarksTitle] (
    [remarkTempId] nvarchar(50),
    [remarkTitle] nvarchar(250),
    PRIMARY KEY ([remarkTempId])
);

create TABLE [SectionMarkInput] (
    [classConfigId] nvarchar(20),
	[classShiftSection] nvarchar(50),
	PRIMARY KEY ([classConfigId], [classShiftSection])
);






