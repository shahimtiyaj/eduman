create TABLE [SectionMarkInput] (
    [classConfigId] nvarchar(20),
	[classShiftSection] nvarchar(50),
	PRIMARY KEY ([classConfigId], [classShiftSection])
);